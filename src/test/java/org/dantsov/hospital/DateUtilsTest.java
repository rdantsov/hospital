package org.dantsov.hospital;

import org.dantsov.hospital.utils.DateUtils;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 6/13/13
 * Time: 10:07 AM
**/
public class DateUtilsTest {

    @Test
    public void testIsValidDateFormat_whenDateIsNull_shouldReturnFalse() {
        assertFalse(DateUtils.isValidDateFormat(null, ' '));
    }

    @Test
    public void testIsValidDateFormat_whenDateIsEmpty_shouldReturnFalse() {
        assertFalse(DateUtils.isValidDateFormat(new String(""), '/'));
    }

    @Test
    public void testIsValidDateFormat_whenDateIsValidDateFormat_shouldReturnTrue() {
        assertTrue(DateUtils.isValidDateFormat(new String("20/10/2012"), '/'));
    }

    @Test
    public void testIsValidDateFormat_whenDateIsValidDateFormatAndHasOneNumberInDayAndMonth_shouldReturnTrue() {
        assertTrue(DateUtils.isValidDateFormat(new String("2.1.2012"), '.'));
    }

    @Test
    public void testIsValidDateFormat_whenDateDelimitersIncorrect_shouldReturnFalse() {
        assertFalse(DateUtils.isValidDateFormat(new String("20.10.2012"), '/'));
    }

    @Test
    public void testIsValidDateFormat_whenMonthInDateHasSymbols_shouldReturnFalse() {
        assertFalse(DateUtils.isValidDateFormat(new String("20/xx/2012"), '/'));
    }

    @Test
    public void testIsValidDateFormat_whenMonthInDateIncorrect_shouldReturnFalse() {
        assertFalse(DateUtils.isValidDateFormat(new String("20/13/2012"), '/'));
    }

    @Test
    public void testIsValidDateFormat_whenDayInDateIncorrect_shouldReturnFalse() {
        assertFalse(DateUtils.isValidDateFormat(new String("35/12/2012"), '/'));
    }

    @Test
    public void testIsValidDateFormat_whenDayInDateIncorrectInLeapYear_shouldReturnFalse() {
        assertFalse(DateUtils.isValidDateFormat(new String("29/02/2013"), '/'));
    }

    @Test
    public void testGetEndPeriodByStartPeriod_whenStartDateIsFirstDayInMonth_shouldReturnLastDayInCurrentMonth() {
        Calendar startDate = Calendar.getInstance();
        //01.01.2013
        startDate.set(2013, 0, 1);
        Calendar endDate = Calendar.getInstance();
        //31.01.2013
        endDate.set(2013, 0, 31);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");
        Calendar temp = Calendar.getInstance();
        temp = DateUtils.getEndPeriodByStartPeriod(startDate);
        String d = shortFormat.format(temp.getTime());
        assertTrue(shortFormat.format(DateUtils.getEndPeriodByStartPeriod(startDate).getTime())
                .equals(shortFormat.format(endDate.getTime())));
    }

    @Test
    public void testGetEndPeriodByStartPeriod_whenStartDateHaveMiddleDayInMonth_shouldReturnMiddleDayInNextMonth() {
        Calendar startDate = Calendar.getInstance();
        //15.01.2013
        startDate.set(2013, 0, 15);
        Calendar endDate = Calendar.getInstance();
        //14.01.2013
        endDate.set(2013, 1, 14);
        assertTrue(DateUtils.getEndPeriodByStartPeriod(startDate).equals(endDate));
    }

    @Test
    //если день начальной даты превышает последний день следующего месяца, должен вернуть последний день след месяца
    public void testGetEndPeriodByStartPeriod_whenStartDateHaveLastDayInMonth_shouldReturnLastDayInNextMonth() {
        Calendar startDate = Calendar.getInstance();
        //31.03.2013
        startDate.set(2013, 2, 31);
        Calendar endDate = Calendar.getInstance();
        //30.04.2013
        endDate.set(2013, 3, 30);
        assertTrue(DateUtils.getEndPeriodByStartPeriod(startDate).equals(endDate));

        //30.01.2013
        startDate.set(2013, 0, 30);
        //28.02.2013
        endDate.set(2013, 1, 28);
        assertTrue(DateUtils.getEndPeriodByStartPeriod(startDate).equals(endDate));
    }

    @Test
    // договор с 15.01.2013 по 14.04.2013, дата расчета 01.06.2013
    //метод должен вернуть дату окончания расчетов 14.04.2013
    public void testGetDateOfEndCalculation_test1() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        startDogovor.set(2013, 0, 15);
        endDogovor.set(2013, 3, 14);
        dateCalculation.set(2013, 5, 1);
        dateOfEndCalculation.set(2013, 3, 14);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");
        Calendar temp = Calendar.getInstance();
        temp = DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation);
        String d = shortFormat.format(temp.getTime());
        String d1 = shortFormat.format(dateOfEndCalculation.getTime());
        assertTrue(
                shortFormat.format(DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation).getTime())
                        .equals(shortFormat.format(dateOfEndCalculation.getTime()))
        );
    }

    @Test
      // договор с 01.01.2013 по 31.01.2013, дата расчета 01.01.2013
      //метод должен вернуть дату окончания расчетов 31.01.2013
    public void testGetDateOfEndCalculation_test2() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        startDogovor.set(2013, 0, 1);
        endDogovor.set(2013, 0, 31);
        dateCalculation.set(2013, 0, 1);
        dateOfEndCalculation.set(2013, 0, 31);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");

        Calendar temp = Calendar.getInstance();
        temp = DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation);
        String d = shortFormat.format(temp.getTime());
        String d1 = shortFormat.format(dateOfEndCalculation.getTime());

        assertTrue(
                shortFormat.format(DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation).getTime())
                        .equals(shortFormat.format(dateOfEndCalculation.getTime()))
        );
    }

    @Test
    // договор с 31.01.2013 по 31.08.2013, дата расчета 01.06.2013
    //метод должен вернуть дату окончания расчетов 30.06.2013
    public void testGetDateOfEndCalculation_test3() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        startDogovor.set(2013, 0, 31);
        endDogovor.set(2013, 7, 31);
        dateCalculation.set(2013, 5, 1);
        dateOfEndCalculation.set(2013, 5, 30);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");
        /*
        для дебага
        Calendar temp = Calendar.getInstance();
        temp = DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation);
        String d = shortFormat.format(temp.getTime());
        String d1 = shortFormat.format(dateOfEndCalculation.getTime());
        */
        assertTrue(
                shortFormat.format(DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation).getTime())
                .equals(shortFormat.format(dateOfEndCalculation.getTime()))
                );
    }

    @Test
    // договор с 28.01.2013 по 15.03.2013, дата расчета 01.02.2013
    //метод должен вернуть дату окончания расчетов 27.02.2013
    public void testGetDateOfEndCalculation_test4() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        startDogovor.set(2013, 0, 29);
        endDogovor.set(2013, 2, 15);
        dateCalculation.set(2013, 1, 1);
        dateOfEndCalculation.set(2013, 1, 28);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");
        assertTrue(
                shortFormat.format(DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation).getTime())
                        .equals(shortFormat.format(dateOfEndCalculation.getTime()))
        );
    }


    @Test
    // договор с 15.01.2013 по 31.03.2014, дата расчета 01.01.2014
    //метод должен вернуть дату окончания расчетов 14.01.2014
    public void testGetDateOfEndCalculation_test5() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        startDogovor.set(2013, 0, 15);
        endDogovor.set(2014, 2, 31);
        dateCalculation.set(2014, 0, 1);
        dateOfEndCalculation.set(2014, 0, 14);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");

        Calendar temp = Calendar.getInstance();
        assertTrue(
                shortFormat.format(DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation).getTime())
                        .equals(shortFormat.format(dateOfEndCalculation.getTime()))
        );
    }

    @Test
    // договор с 30.01.2013 по 31.03.2016, дата расчета 01.02.2016
    //метод должен вернуть дату окончания расчетов 29.02.2014
    public void testGetDateOfEndCalculation_test6() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        startDogovor.set(2013, 0, 30);
        endDogovor.set(2016, 2, 31);
        dateCalculation.set(2016, 1, 1);
        dateOfEndCalculation.set(2016, 1, 29);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");
        assertTrue(
                shortFormat.format(DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation).getTime())
                        .equals(shortFormat.format(dateOfEndCalculation.getTime()))
        );
    }

    @Test
    // договор с 15.01.2013 по 31.03.2014, дата расчета 01.08.2014
    //метод должен вернуть дату окончания расчетов 31.03.2014
    public void testGetDateOfEndCalculation_test7() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        startDogovor.set(2013, 0, 15);
        endDogovor.set(2014, 2, 31);
        dateCalculation.set(2014, 7, 1);
        dateOfEndCalculation.set(2014, 2, 31);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");
        assertTrue(
                shortFormat.format(DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation).getTime())
                        .equals(shortFormat.format(dateOfEndCalculation.getTime()))
        );
    }

    @Test
    // договор с 09.11.2012 по 25.11.2012, дата расчета 01.08.2014
    //метод должен вернуть дату окончания расчетов 25.11.2012
    public void testGetDateOfEndCalculation_test8() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        startDogovor.set(2012, 10, 9);
        endDogovor.set(2012, 10, 25);
        dateCalculation.set(2013, 7, 1);
        dateOfEndCalculation.set(2012, 10, 25);
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyy");


        assertTrue(
                shortFormat.format(DateUtils.getDateOfEndCalculation(startDogovor, endDogovor, dateCalculation).getTime())
                        .equals(shortFormat.format(dateOfEndCalculation.getTime()))
        );
    }


    @Test
    // начальная дата 15.01.2013, конечная дата 01.01.2013
    //метод должен вернуть -15
    public void testGetDaysBetween_test1() {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(2013, 0, 15);
        end.set(2013, 0, 1);
        assertTrue(DateUtils.daysBetween(start, end) == -15);
    }

    @Test
    // начальная дата 15.01.2013, конечная дата 31.01.2013
    //метод должен вернуть 17
    public void testGetDaysBetween_test2() {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(2013, 0, 15);
        end.set(2013, 0, 31);
        assertTrue(DateUtils.daysBetween(start, end) == 17);
    }

    @Test
    // начальная дата 15.02.2013, конечная дата 31.03.2013
    //метод должен вернуть 45
    public void testGetDaysBetween_test3() {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(2013, 1, 15);
        end.set(2013, 2, 31);
        long d = DateUtils.daysBetween(start, end);
        assertTrue(DateUtils.daysBetween(start, end) == 45);
    }

    @Test
    // начальная дата 15.02.2013, конечная дата 01.03.2014
    //метод должен вернуть 380
    public void testGetDaysBetween_test4() {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(2013, 1, 15);
        end.set(2014, 2, 1);
        assertTrue(DateUtils.daysBetween(start, end) == 380);
    }

    @Test
    // начальная дата 15.02.2013, конечная дата 15.02.2013
    //метод должен вернуть 1
    public void testGetDaysBetween_test5() {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(2013, 1, 15);
        end.set(2013, 1, 15);
        assertTrue(DateUtils.daysBetween(start, end) == 1);
    }


    @Test
    public void probuiu() {

        Calendar calendar = Calendar.getInstance();
        calendar.set(2013, 30, 1);

        calendar.add(Calendar.MONTH, 1);
        Date date = calendar.getTime();
        calendar.add(Calendar.MONTH, 1);
        date = calendar.getTime();
    }

    @Test
    // договор с 25.12.2012 по 31.01.2014, период 01.01.13 по 31.12.2013
    //метод должен вернуть истину
    public void testIsCrossingPeriodsOfDates_test1() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar startPeriod = Calendar.getInstance();
        Calendar endPeriod = Calendar.getInstance();
        startDogovor.set(2012, 11, 25);
        endDogovor.set(2014, 0, 31);
        startPeriod.set(2013, 0, 1);
        endPeriod.set(2013, 11, 31);
        assertTrue(DateUtils.isCrossingPeriodsOfDates(startDogovor.getTime(), endDogovor.getTime(), startPeriod.getTime(), endPeriod.getTime()));
    }

    @Test
    // договор с 25.12.2012 по 31.01.2013, период 01.01.13 по 31.12.2013
    //метод должен вернуть истину
    public void testIsCrossingPeriodsOfDates_test2() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar startPeriod = Calendar.getInstance();
        Calendar endPeriod = Calendar.getInstance();
        startDogovor.set(2012, 11, 25);
        endDogovor.set(2013, 0, 31);
        startPeriod.set(2013, 0, 1);
        endPeriod.set(2013, 11, 31);
        assertTrue(DateUtils.isCrossingPeriodsOfDates(startDogovor.getTime(), endDogovor.getTime(), startPeriod.getTime(), endPeriod.getTime()));
    }

    @Test
    // договор с 25.12.2012 по 01.01.2013, период 01.01.13 по 31.12.2013
    //метод должен вернуть истину
    public void testIsCrossingPeriodsOfDates_test3() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar startPeriod = Calendar.getInstance();
        Calendar endPeriod = Calendar.getInstance();
        startDogovor.set(2012, 11, 25);
        endDogovor.set(2013, 0, 1);
        startPeriod.set(2013, 0, 1);
        endPeriod.set(2013, 11, 31);
        assertTrue(DateUtils.isCrossingPeriodsOfDates(startDogovor.getTime(), endDogovor.getTime(), startPeriod.getTime(), endPeriod.getTime()));
    }

    @Test
    // договор с 25.12.2012 по 26.12.2012, период 01.01.13 по 31.12.2013
    //метод должен вернуть ложь
    public void testIsCrossingPeriodsOfDates_test4() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar startPeriod = Calendar.getInstance();
        Calendar endPeriod = Calendar.getInstance();
        startDogovor.set(2012, 11, 25);
        endDogovor.set(2012, 11, 26);
        startPeriod.set(2013, 0, 1);
        endPeriod.set(2013, 11, 31);
        assertFalse(DateUtils.isCrossingPeriodsOfDates(startDogovor.getTime(), endDogovor.getTime(), startPeriod.getTime(), endPeriod.getTime()));
    }

    @Test
    // договор с 25.12.2013 по 31.01.2014, период 01.01.13 по 31.12.2013
    //метод должен вернуть истину
    public void testIsCrossingPeriodsOfDates_test5() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar startPeriod = Calendar.getInstance();
        Calendar endPeriod = Calendar.getInstance();
        startDogovor.set(2013, 11, 25);
        endDogovor.set(2014, 0, 31);
        startPeriod.set(2013, 0, 1);
        endPeriod.set(2013, 11, 31);
        assertTrue(DateUtils.isCrossingPeriodsOfDates(startDogovor.getTime(), endDogovor.getTime(), startPeriod.getTime(), endPeriod.getTime()));
    }

    @Test
    // договор с 25.01.2014 по 31.01.2014, период 01.01.13 по 31.12.2013
    //метод должен вернуть ложь
    public void testIsCrossingPeriodsOfDates_test6() {
        Calendar startDogovor = Calendar.getInstance();
        Calendar endDogovor = Calendar.getInstance();
        Calendar startPeriod = Calendar.getInstance();
        Calendar endPeriod = Calendar.getInstance();
        startDogovor.set(2014, 0, 25);
        endDogovor.set(2014, 0, 31);
        startPeriod.set(2013, 0, 1);
        endPeriod.set(2013, 11, 31);
        assertFalse(DateUtils.isCrossingPeriodsOfDates(startDogovor.getTime(), endDogovor.getTime(), startPeriod.getTime(), endPeriod.getTime()));
    }





}
