package org.dantsov.hospital;

import org.dantsov.hospital.service.ServiceUtils;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 07.05.13
 * Time: 12:36
 * To change this template use File | Settings | File Templates.
 */
public class ServiceUtilsTest {

    @Test
    public void testRound_test1() {
        BigDecimal numberAfterRounfing = ServiceUtils.round(new BigDecimal(349), new BigDecimal(50));
        assertEquals(numberAfterRounfing.compareTo(new BigDecimal(350)), 0);
    }

    @Test
    public void testRound_test2() {
        BigDecimal numberAfterRounfing = ServiceUtils.round(new BigDecimal(350), new BigDecimal(50));
        assertEquals(numberAfterRounfing.compareTo(new BigDecimal(350)), 0);
    }

    @Test
    public void testRound_test3() {
        BigDecimal numberAfterRounfing = ServiceUtils.round(new BigDecimal(351), new BigDecimal(50));
        assertEquals(numberAfterRounfing.compareTo(new BigDecimal(350)), 0);
    }

    @Test
    public void testRound_test4() {
        BigDecimal numberAfterRounfing = ServiceUtils.round(new BigDecimal(325), new BigDecimal(50));
        assertEquals(numberAfterRounfing.compareTo(new BigDecimal(350)), 0);
    }

    @Test
    public void testRound_whenAmountAreNull_shouldReturnNull() {
        BigDecimal numberAfterRounfing = ServiceUtils.round(null, new BigDecimal(50));
        assertNull(numberAfterRounfing);
    }

    @Test
    public void testRound_whenRoundToAreNull_shouldReturnAmount() {
        BigDecimal numberAfterRounfing = ServiceUtils.round(new BigDecimal(125), null);
        assertEquals(numberAfterRounfing.compareTo(new BigDecimal(125)), 0);
    }

    @Test
    public void testRound_whenRoundToLessZero_shouldReturnAmount() {
        BigDecimal numberAfterRounfing = ServiceUtils.round(new BigDecimal(125), new BigDecimal(-5));
        assertEquals(numberAfterRounfing.compareTo(new BigDecimal(125)), 0);
    }

    @Test
    public void testRound_whenAmountLessZero_shouldReturnRoundingAmount() {
        BigDecimal numberAfterRounfing = ServiceUtils.round(new BigDecimal(-125), new BigDecimal(50));
        assertEquals(numberAfterRounfing.compareTo(new BigDecimal(-150)), 0);
    }
}
