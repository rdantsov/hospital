package org.dantsov.hospital.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 09.04.13
 * Time: 11:04
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "BankStatment")
public class BankStatment extends Model {
    private Integer number;
    private Date date;
    private BankStatmentType type;

    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@org.hibernate.annotations.Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "number", nullable = false)
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Column(name = "date", nullable = false)
    @Temporal(value= TemporalType.DATE)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    public BankStatmentType getType() {
        return type;
    }

    public void setType(BankStatmentType type) {
        this.type = type;
    }
}
