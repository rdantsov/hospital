package org.dantsov.hospital.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 30.04.13
 * Time: 12:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Config")
public class Config extends Model {
    private Date currentDate;
    private String fullNameChief;
    private String shortNameChief;
    private String vicariousAuthorty;
    private Integer countPacientOnPage;
    private Date dateCreated;
    private Integer roundingAmount;
    private int actual;
    private String login;
    private String password;

    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@org.hibernate.annotations.Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "currentDate", nullable = false)
    @Temporal(value= TemporalType.DATE)
    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    @Column(name = "fullNameChief", nullable = false)
    public String getFullNameChief() {
        return fullNameChief;
    }

    public void setFullNameChief(String fullNameChief) {
        this.fullNameChief = fullNameChief;
    }

    @Column(name = "shortNameChief", nullable = false)
    public String getShortNameChief() {
        return shortNameChief;
    }

    public void setShortNameChief(String shortNameChief) {
        this.shortNameChief = shortNameChief;
    }

    @Column(name = "vicariousAuthorty", nullable = false)
    public String getVicariousAuthorty() {
        return vicariousAuthorty;
    }

    public void setVicariousAuthorty(String vicariousAuthorty) {
        this.vicariousAuthorty = vicariousAuthorty;
    }

    @Column(name = "countPacientOnPage", nullable = false)
    public Integer getCountPacientOnPage() {
        return countPacientOnPage;
    }

    public void setCountPacientOnPage(Integer countPacientOnPage) {
        this.countPacientOnPage = countPacientOnPage;
    }

    @Column(name = "dateCreated", nullable = false)
    @Temporal(value= TemporalType.TIMESTAMP)
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Column(name = "actual", nullable = false)
    public int getActual() {
        return actual;
    }

    public void setActual(int actual) {
        this.actual = actual;
    }

    @Column(name = "roundingAmount", nullable = false)
    public Integer getRoundingAmount() {
        return roundingAmount;
    }

    public void setRoundingAmount(Integer roundingAmount) {
        this.roundingAmount = roundingAmount;
    }

    @Column(name = "login", nullable = false)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Column(name = "password", nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
