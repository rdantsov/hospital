package org.dantsov.hospital.models;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 08.05.13
 * Time: 15:11
 * To change this template use File | Settings | File Templates.
 */
public enum BankStatmentType {
    PACIENT_PAID("Реестр оплаты"),
    HOSPITAL_RETURNED("Реестр возврата излишней оплаты");

    private final String russianName;
    BankStatmentType(String russianName) {
        this.russianName = russianName;
    }

    public String getRussianName() {
        return russianName;
    }
}
