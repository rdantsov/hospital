package org.dantsov.hospital.models;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rus
 * Date: 07.02.13
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Pacient")
public class Pacient extends Model {
    private String name;
    private  String surname;
    private String midleName;
    private String passport;
    private String authorityIssuingPassport;
    private String address;
    private Date birthday;
    private String customerName;
    private String customerSurname;
    private String customerMidleName;
    private String customerAddress;
    private String customerPassport;
    private String customerAuthorityIssuingPassport;

    private Contract contract;

    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "surname", nullable = false)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "midleName")
    public String getMidleName() {
        return midleName;
    }

    public void setMidleName(String midleName) {
        this.midleName = midleName;
    }

    @Column(name = "passport", nullable = false, length = 9)
    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    @Column(name = "authorityIssuingPassport", nullable = false)
    public String getAuthorityIssuingPassport() {
        return authorityIssuingPassport;
    }

    public void setAuthorityIssuingPassport(String authorityIssuingPassport) {
        this.authorityIssuingPassport = authorityIssuingPassport;
    }

    @Column(name = "customerName", nullable = false)
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(name = "customerSurname", nullable = false)
    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    @Column(name = "customerMidleName", nullable = false)
    public String getCustomerMidleName() {
        return customerMidleName;
    }

    public void setCustomerMidleName(String customerMidleName) {
        this.customerMidleName = customerMidleName;
    }

    @Column(name = "customerPassport", nullable = false, length = 9)
    public String getCustomerPassport() {
        return customerPassport;
    }

    public void setCustomerPassport(String customerPassport) {
        this.customerPassport = customerPassport;
    }

    @Column(name = "customerAuthorityIssuingPassport", nullable = false)
    public String getCustomerAuthorityIssuingPassport() {
        return customerAuthorityIssuingPassport;
    }

    public void setCustomerAuthorityIssuingPassport(String customerAuthorityIssuingPassport) {
        this.customerAuthorityIssuingPassport = customerAuthorityIssuingPassport;
    }

    @OneToOne
    @JoinColumn(name="contract")
    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    @Column(name = "address", nullable = false)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "customerAddress", nullable = false)
    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    @Column(name = "birthday", nullable = false)
    @Temporal(value= TemporalType.DATE)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
