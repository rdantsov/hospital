package org.dantsov.hospital.models;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 18.03.13
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "PaidSum")
public class PaidSum extends Model {
    private BigDecimal amount;
    private Pacient pacient;
    private BankStatment bankStatment;
    private Integer paymentCreditCard;

    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@org.hibernate.annotations.Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "amount", nullable = false)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "paymentCreditCard", nullable = false)
    public Integer getPaymentCreditCard() {
        return paymentCreditCard;
    }

    public void setPaymentCreditCard(Integer paymentCreditCard) {
        this.paymentCreditCard = paymentCreditCard;
    }

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "pacient", nullable = false)
    public Pacient getPacient() {
        return pacient;
    }

    public void setPacient(Pacient pacient) {
        this.pacient = pacient;
    }

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "bankStatment", nullable = false)
    public BankStatment getBankStatment() {
        return bankStatment;
    }

    public void setBankStatment(BankStatment bankStatment) {
        this.bankStatment = bankStatment;
    }
}
