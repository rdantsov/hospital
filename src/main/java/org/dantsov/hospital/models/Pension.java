package org.dantsov.hospital.models;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "Pension")
public class Pension extends Model {
    private Date month;
    private BigDecimal amount;
    private Pacient pacient;

    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@org.hibernate.annotations.Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "amount", nullable = false)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "pacient", nullable = false)
    public Pacient getPacient() {
        return pacient;
    }

    public void setPacient(Pacient pacient) {
        this.pacient = pacient;
    }

    @Column(name = "month", nullable = false)
    @Temporal(value= TemporalType.DATE)
    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }
}
