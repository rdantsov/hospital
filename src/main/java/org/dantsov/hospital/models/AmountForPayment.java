package org.dantsov.hospital.models;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 18.03.13
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "AmountForPayment")
public class AmountForPayment extends Model {
    private Date month;
    private BigDecimal amount;
    private Pacient pacient;
    private String detailCalculation;


    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@org.hibernate.annotations.Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "month", nullable = false)
    @Temporal(value= TemporalType.DATE)
    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    @Column(name = "amount", nullable = false)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "detailCalculation", nullable = true)
    public String getDetailCalculation() {
        return detailCalculation;
    }

    public void setDetailCalculation(String detailCalculation) {
        this.detailCalculation = detailCalculation;
    }

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "pacient", nullable = false)
    public Pacient getPacient() {
        return pacient;
    }

    public void setPacient(Pacient pacient) {
        this.pacient = pacient;
    }

}
