package org.dantsov.hospital.models;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.proxy.HibernateProxyHelper;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: rus
 * Date: 21.01.13
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */
@MappedSuperclass
public class Model {
    public Long id;

    @Transient
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        Class<?> objClass = HibernateProxyHelper.getClassWithoutInitializingProxy(obj);
        if (this.getClass() != objClass) {
            return false;
        }
        if (id == null) {
            return false;
        }
        return id.equals(((Model) obj).getId());
    }

    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        } else {
            return super.hashCode();
        }
    }

}
