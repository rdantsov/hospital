package org.dantsov.hospital.models;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * autor Dantsov Ruslan
 */
@Entity
@Table(name = "Users")
public class Users extends Model {
    private String username;
    private String password;
    private Boolean enabled;

    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@org.hibernate.annotations.Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "username", nullable = false)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "enabled", nullable = false)
    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
