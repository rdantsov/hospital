package org.dantsov.hospital.models;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 15.03.13
 * Time: 16:41
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Period")
public class Period extends Model {
    private Date startPeriod;
    private Date endPeriod;
    private Contract contract;

    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@org.hibernate.annotations.Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "startPeriod", nullable = false)
    @Temporal(value= TemporalType.DATE)
    public Date getStartPeriod() {
        return startPeriod;
    }

    public void setStartPeriod(Date startPeriod) {
        this.startPeriod = startPeriod;
    }

    @Column(name = "endPeriod", nullable = false)
    @Temporal(value= TemporalType.DATE)
    public Date getEndPeriod() {
        return endPeriod;
    }

    public void setEndPeriod(Date endPeriod) {
        this.endPeriod = endPeriod;
    }

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "contract", nullable = false)
    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }
}
