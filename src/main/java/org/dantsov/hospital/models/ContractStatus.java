package org.dantsov.hospital.models;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 07.05.13
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */
public enum ContractStatus {
    ACTIVE,
    COMPLETED,
    CLOSE
}
