package org.dantsov.hospital.models;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 15.03.13
 * Time: 16:38
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Contract")
public class Contract extends Model {
    private Integer nomer;
    private Date dateCreated;
    private Integer completed;

    @Id
    @Column(name = "Id", nullable = false)
    @GenericGenerator(name="table-hilo-generator", strategy="org.hibernate.id.TableHiLoGenerator",
            parameters={@org.hibernate.annotations.Parameter(value="hibernate_id_generation", name="table")})
    @GeneratedValue(generator="table-hilo-generator")
    @Override
    public Long getId() {
        return id;
    }

    @Column(name = "nomer", nullable = false)
    public Integer getNomer() {
        return nomer;
    }

    public void setNomer(Integer nomer) {
        this.nomer = nomer;
    }

    @Column(name = "completed", nullable = false)
    public Integer getCompleted() {
        return completed;
    }

    public void setCompleted(Integer completed) {
        this.completed = completed;
    }

    @Column(name = "dateCreated", nullable = false)
    @Temporal(value= TemporalType.DATE)
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
