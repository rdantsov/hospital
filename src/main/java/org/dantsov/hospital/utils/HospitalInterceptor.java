package org.dantsov.hospital.utils;

import org.dantsov.hospital.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: rus
 * Date: 29.01.13
 * Time: 17:02
 * To change this template use File | Settings | File Templates.
 */
public class HospitalInterceptor implements HandlerInterceptor {
    @Autowired
    private ConfigService configService;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler)
            throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object handler, ModelAndView modelAndView)
            throws Exception {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //получаем имя залогиневшегося пользователя
        String userName = null;
        if (principal instanceof UserDetails) {
           userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        if (modelAndView != null) {
            SimpleDateFormat dateformat = new SimpleDateFormat("MM.yyyy");
            String configCurrentDate = dateformat.format(configService.getCurrentDate());

            modelAndView.getModelMap().addAttribute("userName", userName);
            modelAndView.getModelMap().addAttribute("configCurrentDate", configCurrentDate);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler, Exception ex)
            throws Exception {

    }
}
