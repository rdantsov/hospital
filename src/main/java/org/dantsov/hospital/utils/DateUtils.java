package org.dantsov.hospital.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 6/12/13
 * Time: 6:34 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class DateUtils {
    //название месяца на русском в в
    public static String getMonthNameOnRussianInGenitive(String monthNumber) {
        Map<String, String> monthFullName = new HashMap<String, String>();
        monthFullName.put("01", "января");
        monthFullName.put("02", "февраля");
        monthFullName.put("03", "марта");
        monthFullName.put("04", "апреля");
        monthFullName.put("05", "мая");
        monthFullName.put("06", "июня");
        monthFullName.put("07", "июля");
        monthFullName.put("08", "августа");
        monthFullName.put("09", "сентября");
        monthFullName.put("10", "октября");
        monthFullName.put("11", "ноября");
        monthFullName.put("12", "декабря");
        if (monthFullName.get(monthNumber) != null) {
            return monthFullName.get(monthNumber);
        } else {
            return "";
        }
    }

    //функция для определения кол-ва дней между двумя датами
    public static long daysBetween(Calendar startDate, Calendar endDate) {
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yyyy");
        long daysBetween = 0;
        if (startDate.before(endDate) ||
                shortFormat.format(startDate.getTime()).equals(shortFormat.format(endDate.getTime()))) {
            //проверка пока стартовая дата меньше либо равна конечной
            Calendar date = (Calendar) startDate.clone();
            while (date.before(endDate) ||
                    shortFormat.format(date.getTime()).equals(shortFormat.format(endDate.getTime()))) {
                date.add(Calendar.DATE, 1);
                daysBetween++;
            }
        } else {
            //проверка пока кончная дата меньше либо равна начальной, но результат будет отрицательный
            Calendar date = (Calendar) endDate.clone();
            while (date.before(startDate) ||
                    shortFormat.format(date.getTime()).equals(shortFormat.format(startDate.getTime()))) {
                date.add(Calendar.DATE, 1);
                daysBetween++;
            }
            daysBetween = (-daysBetween);
        }
        return daysBetween;
    }


    //функция для определения даты окончания периода, величиной в 1 месяц.
    public static Calendar getEndPeriodByStartPeriod(Calendar startPeriod) {
        /*
           если день даты начала  > последнего дня след месяц,
                то конец периода устанавливаем последним днем след месяца
                иначе прибавляем к нач. дате один месяц и отнимаем один день
        */

        Calendar endPeriod = Calendar.getInstance();
        endPeriod.setTime(startPeriod.getTime());
        endPeriod.set(Calendar.DATE, 1);
        endPeriod.add(Calendar.MONTH, 1);
        int dateStratPeriod = startPeriod.get(Calendar.DATE);
        int lastDayInNextMonth = endPeriod.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (dateStratPeriod > lastDayInNextMonth) {
            endPeriod.set(Calendar.DATE, lastDayInNextMonth);
        } else {
            endPeriod.setTime(startPeriod.getTime());
            endPeriod.add(Calendar.MONTH, 1);
            endPeriod.add(Calendar.DATE, -1);
        }

        return endPeriod;
    }

    //функция для определения даты начала нового периода,
    // в качестве параметра получает дату окончания предыдущего периода
    public static Calendar getStartPeriodByPreviousEndPeriod(Calendar endPeriod) {
        Calendar startPeriod = Calendar.getInstance();
        startPeriod.setTime(endPeriod.getTime());
        startPeriod.add(Calendar.DATE, 1);
        return startPeriod;
    }

    //функция для определения даты окончания расчета
    //параметры: Дата начала договора, дата окончания последнего из периодов договора,
    //дата за которую будет происходить расчет (месяц расчета).
    public static Calendar getDateOfEndCalculation(
            final Calendar startDogovor,
            final Calendar endDogovor,
            final Calendar dateCalculation) {
        Calendar dateOfEndCalculation = Calendar.getInstance();
        Calendar newDateCalculation = Calendar.getInstance();
        newDateCalculation.setTime(dateCalculation.getTime());
        int lastDay = dateCalculation.getActualMaximum(Calendar.DAY_OF_MONTH);
        newDateCalculation.set(Calendar.DATE, lastDay);
        newDateCalculation.set(Calendar.HOUR, 1);
        SimpleDateFormat shortFormat = new SimpleDateFormat("MM.yyy");
        if (dateCalculation.after(endDogovor)
                || shortFormat.format(dateCalculation.getTime()).equals(shortFormat.format(endDogovor.getTime())) ) {
            dateOfEndCalculation.setTime(endDogovor.getTime());
        } else {
            int dayOfStartDogovor = startDogovor.get(Calendar.DATE);
            //если день даты начала договора превышает максимальный день месяца расчета,
            //то устанавливаем последний день месяца расчета
            //иначе день начала договора
            if (dayOfStartDogovor > lastDay) {
                dateOfEndCalculation.setTime(dateCalculation.getTime());
                dateOfEndCalculation.set(Calendar.DATE, lastDay);
            } else {
                dateOfEndCalculation.setTime(dateCalculation.getTime());
                dateOfEndCalculation.set(Calendar.DATE, dayOfStartDogovor);
                dateOfEndCalculation.add(Calendar.DATE, -1);
            }
        }
        return dateOfEndCalculation;
    }

    public static boolean isValidDateFormat(String date, char dateDelimiters) {
        if (date == null) {
            return false;
        }
        //date format may be: dd MM yyyy; d MM yyyy; dd M yyyy; d M yyyy.
        if (date.matches("\\d{2}" + dateDelimiters + "\\d{2}" + dateDelimiters + "\\d{4}") ||
                date.matches("\\d" + dateDelimiters + "\\d{2}" + dateDelimiters + "\\d{4}") ||
                date.matches("\\d{2}" + dateDelimiters + "\\d" + dateDelimiters + "\\d{4}") ||
                date.matches("\\d" + dateDelimiters + "\\d" + dateDelimiters + "\\d{4}")) {
            String[] tmp = date.split("\\" + dateDelimiters );
            Integer day = Integer.valueOf(tmp[0]);
            Integer month = Integer.valueOf(tmp[1]);
            Integer year = Integer.valueOf(tmp[2]);
            if (month < 1 || month > 12) {
                return false;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month-1, 1);
            if (day < 0 || day > calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                return false;
            }
            return true;
        } else return false;

    }
    // функция для проверки попадает или пересекает  ли один период в другой
    public static boolean isCrossingPeriodsOfDates(Date startFirstPeriod, Date endFirstPeriod, Date startSecondPeriod,
                                                   Date endSecondPeriod) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Calendar startFirstPeriodCal = Calendar.getInstance();
        Calendar endFirstPeriodCal = Calendar.getInstance();
        Calendar startSecondPeriodCal = Calendar.getInstance();
        Calendar endSecondPeriodCal = Calendar.getInstance();
        startFirstPeriodCal.setTime(startFirstPeriod);
        endFirstPeriodCal.setTime(endFirstPeriod);
        startSecondPeriodCal.setTime(startSecondPeriod);
        endSecondPeriodCal.setTime(endSecondPeriod);
        //s2 > s1 &&  s2 < e1 ||
        //e2 > s1 && e2 < e1 ||
        //s1 > s2 && s1 < e2 ||
        //e1 > s2 && e1 < e2

        if ( ( (startSecondPeriodCal.after(startFirstPeriodCal)
                    || dateFormat.format(startSecondPeriod).equals(dateFormat.format(startFirstPeriod)))
               && (startSecondPeriodCal.before(startFirstPeriodCal)
                    || dateFormat.format(startSecondPeriod).equals(dateFormat.format(startFirstPeriod)))
             ) ||
             ( (endSecondPeriodCal.after(startFirstPeriodCal)
                    || dateFormat.format(endSecondPeriod).equals(dateFormat.format(startFirstPeriod)))
               && (endSecondPeriodCal.before(endFirstPeriodCal)
                    || dateFormat.format(endSecondPeriod).equals(dateFormat.format(endFirstPeriod)))
             ) ||
             ( (startFirstPeriodCal.after(startSecondPeriodCal)
                    || dateFormat.format(startFirstPeriod).equals(dateFormat.format(startSecondPeriod)))
                        && (startFirstPeriodCal.before(endSecondPeriodCal)
                    || dateFormat.format(startFirstPeriod).equals(dateFormat.format(endSecondPeriod)))
             ) ||
             ( (endFirstPeriodCal.after(startSecondPeriodCal)
                    || dateFormat.format(endFirstPeriod).equals(dateFormat.format(startSecondPeriod)))
                        && (endFirstPeriodCal.before(endSecondPeriodCal)
                    || dateFormat.format(endFirstPeriod).equals(dateFormat.format(endSecondPeriod)))
             )
           ) {
            return true;
        }
        return false;
    }
}