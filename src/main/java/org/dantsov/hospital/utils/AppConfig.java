package org.dantsov.hospital.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.datetime.DateFormatterRegistrar;
import org.springframework.format.number.NumberFormatAnnotationFormatterFactory;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 28.05.13
 * Time: 16:14
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class AppConfig {
    //пока не используется
    //создание конфига для добавления нового формата дат и т.д.
    @Bean
    public FormattingConversionService conversionService() {

        // Use the DefaultFormattingConversionService but do not register defaults
        DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService(false);

        // Ensure @NumberFormat is still supported
        conversionService.addFormatterForFieldAnnotation(new NumberFormatAnnotationFormatterFactory());

        // Register date conversion with a specific global format
        DateFormatterRegistrar registrar = new DateFormatterRegistrar();
        registrar.setFormatter(new DateFormatter("dd.MM.yyyy"));
        registrar.registerFormatters(conversionService);

        return conversionService;
    }
}