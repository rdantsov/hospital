package org.dantsov.hospital.controllers;

import org.dantsov.hospital.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 21.05.13
 * Time: 12:22
 * To change this template use File | Settings | File Templates.
 */
@ControllerAdvice
class ExceptionHandler {
    @Autowired
    private ConfigService configService;

    /**
     * Handle exceptions thrown by handlers.
     */
@org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
    public ModelAndView exception(Exception exception, WebRequest request) {
        ModelAndView modelAndView = new ModelAndView("error/allError");
        modelAndView.addObject("userName", "Ошибка");
        SimpleDateFormat dateformat = new SimpleDateFormat("MM.yyyy");
        String configCurrentDate = dateformat.format(configService.getCurrentDate());
        modelAndView.addObject("configCurrentDate", configCurrentDate);
        StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();
        modelAndView.addObject("exceptionStackTrace", exceptionAsString);
        return modelAndView;
    }
}