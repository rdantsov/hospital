package org.dantsov.hospital.controllers;

import org.dantsov.hospital.controllers.forms.pacient.PacientForm;
import org.dantsov.hospital.controllers.forms.pacient.PacientListParametrsForm;
import org.dantsov.hospital.controllers.validators.PacientValidator;
import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Period;
import org.dantsov.hospital.service.ConfigService;
import org.dantsov.hospital.service.ContractService;
import org.dantsov.hospital.service.PacientService;
import org.dantsov.hospital.service.PeriodService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class PacientController {
    @Autowired
    private PacientService pacientService;
    @Autowired
    private ContractService contractService;
    @Autowired
    private PeriodService periodService;
    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/pacients")
    String list(@RequestParam(value = "sort", required = false) String sort,
                @RequestParam(value = "page", required = false) Integer page,
                @ModelAttribute("pacientListParametrsForm")PacientListParametrsForm pacientListParametrsForm,
                Model model) {
        //RedirectAttributes redirectAttributes,
        final Integer countPacientOnPage = configService.getActualConfig().getCountPacientOnPage();
        if (pacientListParametrsForm == null) {
            pacientListParametrsForm = new PacientListParametrsForm();
            pacientListParametrsForm.setPacientContractClose(false);
            pacientListParametrsForm.setPage(page);
        }
        boolean isPacientContractClose = false;
        if (sort == null || (!sort.equals("contract") && !sort.equals("fullName"))) {
            sort = "contract";
        }
        if (pacientListParametrsForm.getPage() == null) {
            pacientListParametrsForm.setPage(1);
        }
        if (pacientListParametrsForm.getSearchText() == null) {
            pacientListParametrsForm.setSearchText(new String());
        }

        int prevPage = pacientListParametrsForm.getPage() - 1;
        int nextPage = pacientListParametrsForm.getPage() + 1;
        List<Pacient> pacients = null;
        if (isPacientContractClose || !pacientListParametrsForm.getSearchText().equals(new String(""))) {
             pacients = pacientService.getAllByPageAndFilter(pacientListParametrsForm.getPage(),
                     countPacientOnPage, pacientListParametrsForm.getSearchText(),
                     isPacientContractClose, sort);
        } else {
            pacients = pacientService.getAllByPage(pacientListParametrsForm.getPage(), countPacientOnPage, sort);
        }

        Map<String, String> pacientWithContractStatus = pacientService.getMapPacientsWithContracrtStatus(pacients);
        boolean availableNextPage = true;
        if (pacients.size() <= countPacientOnPage) {
            availableNextPage = false;
            model.addAttribute("pacients", pacients);
        } else {
            model.addAttribute("pacients", pacients.subList(0, countPacientOnPage));
        }

        model.addAttribute("prevPage", prevPage);
        model.addAttribute("nextPage", nextPage);
        model.addAttribute("pacientListParametrsForm", pacientListParametrsForm);
        model.addAttribute("availableNextPage", availableNextPage);
        model.addAttribute("pacientWithContractStatus", pacientWithContractStatus);
        return "/pacient/list";
    }

    @RequestMapping(value = "/pacient/new", method = RequestMethod.GET)
    String add(Model model) {
        PacientForm pacientForm = new PacientForm();
        Date currentDate = configService.getCurrentDate();
        pacientForm.setContractNomer(contractService.getMaxNumberOfContractByYear(currentDate) + 1);
        pacientForm.setStartPeriod(currentDate);
        pacientForm.setBirthday(currentDate);
        pacientForm.setDateCreated(currentDate);
        Calendar calEndPeriod = Calendar.getInstance();
        calEndPeriod.setTime(currentDate);
        calEndPeriod.add(Calendar.MONTH, 1);
        calEndPeriod.add(Calendar.DATE, -1);
        pacientForm.setEndPeriod(calEndPeriod.getTime());
        model.addAttribute("new", "new");
        model.addAttribute("endDate", calEndPeriod.getTime());
        model.addAttribute("currentDate", currentDate);
        model.addAttribute("pacientForm", pacientForm);
        return "pacient/edit";
    }

    @RequestMapping(value = "/pacient/new", method = RequestMethod.POST)
    String create(@Valid @ModelAttribute("pacientForm")
                  PacientForm pacientForm, final BindingResult result,
                  final Model model) {
        PacientValidator pacientValidator = new PacientValidator();
        pacientValidator.validate(pacientForm, result);
        if (result.hasErrors()) {
            Date currentDate = configService.getCurrentDate();
            model.addAttribute("new", "new");
            model.addAttribute("currentDate", currentDate);
            model.addAttribute("endDate", pacientForm.getEndPeriod());
            model.addAttribute("pacientForm", pacientForm);
            return "pacient/edit";
        } else {
            Calendar calendar = Calendar.getInstance();
            ModelMapper modelMapper = new ModelMapper();
            Contract contract = modelMapper.map(pacientForm, Contract.class);
            Pacient pacient = modelMapper.map(pacientForm, Pacient.class);
            calendar.setTime(pacientForm.getBirthday());
            pacient.setBirthday(calendar.getTime());

            contract.setCompleted(Integer.valueOf(0));
            calendar.setTime(pacientForm.getDateCreated());
            contract.setDateCreated(calendar.getTime());

            pacient.setContract(contract);
            Period period = modelMapper.map(pacientForm, Period.class);
            period.setContract(contract);

            calendar.setTime(pacientForm.getStartPeriod());
            period.setStartPeriod(calendar.getTime());
            calendar.setTime(pacientForm.getEndPeriod());
            period.setEndPeriod(calendar.getTime());
            pacientService.save(pacient, contract, period);
            return "redirect:/pacients";
        }

    }

    @RequestMapping(value = "/pacient/{id}/edit", method = RequestMethod.GET)
    String edit(@PathVariable Long id, Model model) {
        Pacient pacient = pacientService.getById(id);
        if (pacient == null) {
            return "/error/allError";
        }
        PacientForm pacientForm = pacientService.getPacientFormByPacient(pacient);
        Date currentDate = configService.getCurrentDate();
        model.addAttribute("endDate", pacientForm.getEndPeriod());
        model.addAttribute("currentDate", currentDate);
        model.addAttribute("pacientForm", pacientForm);
        return "pacient/edit";
    }

    @RequestMapping(value = "/pacient/{id}/edit", method = RequestMethod.POST)
    String create(@Valid @ModelAttribute("pacientForm")
                  PacientForm pacientForm, BindingResult result,
                  @PathVariable Long id, Model model) {
        Pacient pacient = pacientService.getById(id);
        if (pacient == null) {
            return "/error/allError";
        }

        PacientValidator pacientValidator = new PacientValidator();
        pacientValidator.validate(pacientForm, result);
        if (result.hasErrors()) {
            Date currentDate = configService.getCurrentDate();
            model.addAttribute("currentDate", currentDate);
            model.addAttribute("endDate", pacientForm.getEndPeriod());
            model.addAttribute("pacientForm", pacientForm);
            return "pacient/edit";
        } else {
            Calendar calendar = Calendar.getInstance();
            ModelMapper modelMapper = new ModelMapper();

            modelMapper.map(pacientForm, pacient);
            calendar.setTime(pacientForm.getBirthday());
            pacient.setBirthday(calendar.getTime());

            Contract contract = contractService.getByPacient(pacient);
            modelMapper.map(pacientForm, contract);
            calendar.setTime(pacientForm.getDateCreated());
            contract.setDateCreated(calendar.getTime());

           // pacient.setContract(contract);
            Period period = periodService.getFirstPeriodByContract(contract);
            modelMapper.map(pacientForm, period);
            //period.setContract(contract);

            calendar.setTime(pacientForm.getStartPeriod());
            period.setStartPeriod(calendar.getTime());
            calendar.setTime(pacientForm.getEndPeriod());
            period.setEndPeriod(calendar.getTime());

            pacientService.save(pacient, contract, period);
            return "redirect:/pacients";
        }

    }

}
