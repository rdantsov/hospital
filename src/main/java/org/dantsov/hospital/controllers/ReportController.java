package org.dantsov.hospital.controllers;

import org.dantsov.hospital.controllers.forms.report.DebtRegistrPeriodForm;
import org.dantsov.hospital.controllers.forms.report.Statistics6ynPeriodForm;
import org.dantsov.hospital.controllers.validators.DebtRegistrValidator;
import org.dantsov.hospital.controllers.validators.Statistics6ynValidator;
import org.dantsov.hospital.models.AmountForPayment;
import org.dantsov.hospital.models.Config;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Period;
import org.dantsov.hospital.service.*;
import org.dantsov.hospital.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 15.04.13
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ReportController {
    //redirectAttrs.addFlashAttribute("AttributeName", value); //так нужно добавлять атрибуты в урл
    @Autowired
    private CalculatorPaymentService calculatorPaymentService;
    @Autowired
    private PacientService pacientService;
    @Autowired
    private PeriodService periodService;
    @Autowired
    private ConfigService configService;
    @Autowired
    private PaidSumService paidSumService;
    @Autowired
    private ReportService reportService;

    @RequestMapping(value = "reports/receiptForPayment/pdf", method = RequestMethod.GET)
    public ModelAndView generateReceiptForPayment(
            @RequestParam(value = "pacient", required = true) Long pacientId,
            @RequestParam(value = "date", required = true)
            @DateTimeFormat(pattern="MM.yyyy")Date date,
            Model model) {
        AmountForPayment amountForPayment = calculatorPaymentService.getAmountForPaymentByPacientAndDate(pacientId, date);
        BigDecimal amount;
        //проверка что бы для указаннго пациента была сумма оплаты и она была не отрицательной
        if (amountForPayment != null) {
            amount = amountForPayment.getAmount();
        } else {
            amount = BigDecimal.ZERO;
        }
        if (amount.compareTo(BigDecimal.ZERO) == -1) {
            amount = BigDecimal.ZERO;
        }
        //

        Pacient pacient = pacientService.getById(pacientId);
        StringBuilder pacientReport = new StringBuilder();
        pacientReport.append(pacient.getSurname()).append(" ").append(pacient.getName())
                .append(" ").append(pacient.getMidleName()).append(", договор № ")
                .append(pacient.getContract().getNomer());

        StringBuilder amountReport = new StringBuilder();
        amountReport.append(amount.toString()).append(" руб.");

        SimpleDateFormat dateformat = new SimpleDateFormat("dd.MM.yyyy");
        StringBuilder dateReport = new StringBuilder(dateformat.format(new Date()));

        Map<String,String> parametrMap = new HashMap<String,String>();
        parametrMap.put("pacient", pacientReport.toString());
        parametrMap.put("amount", amountReport.toString());
        parametrMap.put("currentDate", dateReport.toString());

        return new ModelAndView("receiptForPaymentPDF", parametrMap);
    }

    @RequestMapping(value = "reports/pacient/{id}/contract/pdf", method = RequestMethod.GET)
    public ModelAndView generateContract(@PathVariable Long id, Model model) {
        Pacient pacient = pacientService.getById(id);
        if (pacient == null) {
            return new ModelAndView("/error/allError");
        }
        Period period = periodService.getFirstPeriodByContract(pacient.getContract());
        Config config = configService.getActualConfig();

        SimpleDateFormat dateFormatFullMonth = new SimpleDateFormat("dd.MM.yyyy");
        StringBuilder contractDateCreateReport = new StringBuilder();
        contractDateCreateReport.append("от ").
                append(dateFormatFullMonth.format(pacient.getContract().getDateCreated()));

        StringBuilder customerSurnameReport = new StringBuilder();
        customerSurnameReport.append(pacient.getCustomerSurname());
                //.append("____________________________________");

        StringBuilder customerNameMidleNameReport = new StringBuilder();
        customerNameMidleNameReport.append(pacient.getCustomerName())
                .append(" ")
                .append(pacient.getCustomerMidleName())
                .append(",");
                //.append(", _____________________________________");

        StringBuilder customerAddressReport = new StringBuilder();
        customerAddressReport.append(pacient.getCustomerAddress())
                .append(",");
                //.append(", ____________________________________________________________________");

        StringBuilder customerPassportReport = new StringBuilder();
        customerPassportReport.append(pacient.getCustomerPassport())
                .append(",");
                //.append(", __________");

        StringBuilder customerAuthorityIssuingPassportReport = new StringBuilder();
        customerAuthorityIssuingPassportReport.append(pacient.getCustomerAuthorityIssuingPassport())
                .append(",");
                //.append(", _________________________________________");

        StringBuilder pacientFullNameReport = new StringBuilder();
        pacientFullNameReport.append(pacient.getSurname())
                .append(" ")
                .append(pacient.getName())
                .append(" ")
                .append(pacient.getMidleName())
                .append(",");
                //.append(", ______________________");

        SimpleDateFormat dateFormatShort = new SimpleDateFormat("dd.MM.yyyy");
        StringBuilder pacientBirthdayReport = new StringBuilder();
        pacientBirthdayReport.append(dateFormatShort.format(pacient.getBirthday()))
                .append(",");
                //.append(", ____________________");

        StringBuilder pacientAddressReport = new StringBuilder();
        pacientAddressReport.append(pacient.getAddress())
                .append(",");
                //.append(", ________________________________________________________________");

        StringBuilder pacientPassportReport = new StringBuilder();
        pacientPassportReport.append(pacient.getPassport())
                .append(",");
                //.append(", ______________");

        StringBuilder pacientAuthorityIssuingPassportReport = new StringBuilder();
        pacientAuthorityIssuingPassportReport.append(pacient.getAuthorityIssuingPassport())
                .append(",");
                //.append(", ________________________________________________");

        StringBuilder periodStartReport = new StringBuilder();
        periodStartReport.append(dateFormatShort.format(period.getStartPeriod()));
                //.append("________");

        StringBuilder periodEndReport = new StringBuilder();
        periodEndReport.append(dateFormatShort.format(period.getEndPeriod()))
                .append(".");
                //.append("____ .");

        StringBuilder customerSurnameForEndReport = new StringBuilder();
        customerSurnameForEndReport.append(pacient.getCustomerSurname())
                .append("____________________________");

        StringBuilder customerNameReport = new StringBuilder();
        customerNameReport.append(pacient.getCustomerName())
                .append("____________________________");

        StringBuilder customerMidleNameForEndReport = new StringBuilder();
        customerMidleNameForEndReport.append(pacient.getCustomerMidleName())
                .append("_____________________________");

        Map<String,String> parametrMap = new HashMap<String,String>();
        parametrMap.put("contractNomer", pacient.getContract().getNomer().toString());
        parametrMap.put("contractDateCreate", contractDateCreateReport.toString());
        parametrMap.put("fullNameChief", config.getFullNameChief());
        parametrMap.put("vicariousAuthortyReport", config.getVicariousAuthorty());
        parametrMap.put("shortNameChiefReport", config.getShortNameChief());
        parametrMap.put("customerSurnameReport", customerSurnameReport.toString());
        parametrMap.put("customerNameMidleNameReport", customerNameMidleNameReport.toString());
        parametrMap.put("customerAddressReport", customerAddressReport.toString());
        parametrMap.put("customerPassportReport", customerPassportReport.toString());
        parametrMap.put("customerAuthorityIssuingPassportReport", customerAuthorityIssuingPassportReport.toString());
        parametrMap.put("pacientFullNameReport", pacientFullNameReport.toString());
        parametrMap.put("pacientBirthdayReport", pacientBirthdayReport.toString());
        parametrMap.put("pacientAddressReport", pacientAddressReport.toString());
        parametrMap.put("pacientPassportReport", pacientPassportReport.toString());
        parametrMap.put("pacientAuthorityIssuingPassportReport", pacientAuthorityIssuingPassportReport.toString());
        parametrMap.put("periodStartReport", periodStartReport.toString());
        parametrMap.put("periodEndReport", periodEndReport.toString());
        parametrMap.put("customerSurnameForEndReport", customerSurnameForEndReport.toString());
        parametrMap.put("customerNameReport", customerNameReport.toString());
        parametrMap.put("customerMidleNameForEndReport", customerMidleNameForEndReport.toString());

        return new ModelAndView("contractPDF", parametrMap);
    }

    @RequestMapping(value = "reports/pacientList/pdf", method = RequestMethod.GET)
    public ModelAndView generateListOfPacients(
            @RequestParam(value = "dateForReport", required = true)
            @DateTimeFormat(pattern="dd.MM.yyyy")Date dateForReport) {
        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        StringBuilder monthAndYear = new StringBuilder();
        monthAndYear.append(DateUtils.getMonthNameOnRussianInGenitive(monthFormat.format(dateForReport)))
                .append(" ")
                .append(yearFormat.format(dateForReport))
                .append(" г.");
        Map<String, Object> parametrMap = new HashMap<String, Object>();
        parametrMap.put("dayReport", dayFormat.format(dateForReport));
        parametrMap.put("monthAndYearReport", monthAndYear.toString());
        parametrMap.put("pacientFormList", pacientService.getPacientListForReport(dateForReport));
        return new ModelAndView("listOfPacientsPDF", parametrMap);
    }

    @RequestMapping(value = "reports/pacientList", method = RequestMethod.GET)
    public String dataForReportPacientList(Model model) {
        Date configDate = configService.getCurrentDate();
        model.addAttribute("configDate", configDate);
        model.addAttribute("dateForReport", configDate);
        return "/report/pacientList";
    }

    @RequestMapping(value = "reports/pacientList", method = RequestMethod.POST)
    public String createReportPacientList(Model model,
                                          @ModelAttribute("dateForReport") String dateForReport) {
        return "redirect:/reports/pacientList/pdf";
    }

    @RequestMapping(value = "reports/statistics6yn", method = RequestMethod.GET)
    public String dataForReport6YN(Model model) {
        Date configDate = configService.getCurrentDate();
        model.addAttribute("configDate", configDate);
        //Определяем нужный квартал для дата С и дата По
        Calendar configDateCalendar = Calendar.getInstance();
        Calendar dateFromCalendar = Calendar.getInstance();
        Calendar dateToCalendar = Calendar.getInstance();
        configDateCalendar.setTime(configDate);
        if (configDateCalendar.get(Calendar.MONTH)>=0 && configDateCalendar.get(Calendar.MONTH)<=1) {
            dateFromCalendar.set(configDateCalendar.get(Calendar.YEAR), 0, 1);
            dateToCalendar.set(configDateCalendar.get(Calendar.YEAR), 0, 31);
        } else {
            dateFromCalendar.set(configDateCalendar.get(Calendar.YEAR), 0, 1);
            dateToCalendar.set(configDateCalendar.get(Calendar.YEAR), configDateCalendar.get(Calendar.MONTH)-1, 1);
            dateToCalendar.set(Calendar.DATE, dateToCalendar.getActualMaximum(Calendar.DATE));
        }
        Statistics6ynPeriodForm statistics6ynPeriodForm = new Statistics6ynPeriodForm();
        statistics6ynPeriodForm.setStartPeriod(dateFromCalendar.getTime());
        statistics6ynPeriodForm.setEndPeriod(dateToCalendar.getTime());
        model.addAttribute("periodForm", statistics6ynPeriodForm);
        return "/report/statistics6yn";                                               }

    @RequestMapping(value = "reports/statistics6yn", method = RequestMethod.POST)
    public String createReport6YN(@Valid @ModelAttribute("periodForm")Statistics6ynPeriodForm statistics6ynPeriodForm,
                                  BindingResult periodResult, RedirectAttributes redirectAttributes,
                                  Model model) {
        Statistics6ynValidator statistics6ynValidator = new Statistics6ynValidator();
        statistics6ynValidator.validate(statistics6ynPeriodForm, periodResult);
        if (periodResult.hasErrors()) {
            Date configDate = configService.getCurrentDate();
            model.addAttribute("configDate", configDate);
            model.addAttribute("periodForm", statistics6ynPeriodForm);
            return "/report/statistics6yn";
        }
        redirectAttributes.addFlashAttribute("periodForm", statistics6ynPeriodForm);
        return "redirect:/reports/statistics6yn/pdf";
    }

    @RequestMapping(value = "reports/statistics6yn/pdf", method = RequestMethod.GET)
    public ModelAndView generate6YN(@ModelAttribute("periodForm")Statistics6ynPeriodForm statistics6ynPeriodForm) {
        Config config = configService.getActualConfig();
        Locale locale = new Locale("ru");
        SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM", locale);
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        StringBuilder monthAndYear = new StringBuilder();
        monthAndYear.append(monthFormat.format(statistics6ynPeriodForm.getEndPeriod()).toLowerCase())
                .append(" ")
                .append(yearFormat.format(statistics6ynPeriodForm.getEndPeriod()))
                .append(" года");

        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        startDate.setTime(statistics6ynPeriodForm.getStartPeriod());
        startDate.set(Calendar.DATE, endDate.getActualMinimum(Calendar.DATE));

        endDate.setTime(statistics6ynPeriodForm.getEndPeriod());
        endDate.set(Calendar.DATE, endDate.getActualMaximum(Calendar.DATE));


        BigDecimal amountForPeriod = paidSumService.getPaidSumForPeriod(startDate.getTime(), endDate.getTime());
        amountForPeriod = amountForPeriod.divide(new BigDecimal(1000000), 10, BigDecimal.ROUND_HALF_UP);
        amountForPeriod = ServiceUtils.round(amountForPeriod, new BigDecimal("0.1"));

        Map<String, Object> parametrMap = new HashMap<String, Object>();
        parametrMap.put("dateTo", monthAndYear.toString());
        parametrMap.put("amountForPeriod001", amountForPeriod.toString());
        parametrMap.put("amountForPeriod037", amountForPeriod.toString());
        parametrMap.put("shortNameChiefReport", config.getShortNameChief());
        return new ModelAndView("statistics6ynPDF", parametrMap);
    }

    @RequestMapping(value = "reports/statistics1yn", method = RequestMethod.GET)
    public String dataForReport1YN(Model model) {
        Date configDate = configService.getCurrentDate();
        model.addAttribute("configDate", configDate);
        model.addAttribute("yearForReport", configDate);
        return "/report/statistics1yn";
    }

    @RequestMapping(value = "reports/statistics1yn", method = RequestMethod.POST)
    public String createReport1YN(@ModelAttribute("yearForReport")String yearForReport) {
        return "redirect:/reports/statistics1yn/pdf";
    }

    @RequestMapping(value = "reports/statistics1yn/pdf", method = RequestMethod.GET)
    public ModelAndView generate1YN(
            @RequestParam(value = "yearForReport", required = true)
            @DateTimeFormat(pattern="yyyy")Date yearForReport) {
        Config config = configService.getActualConfig();
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        StringBuilder year = new StringBuilder();
        year.append(yearFormat.format(yearForReport))
                .append(" год");

        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        startDate.setTime(yearForReport);
        startDate.set(Calendar.MONTH, 0);
        startDate.set(Calendar.DATE, 1);

        endDate.setTime(yearForReport);
        endDate.set(Calendar.MONTH, 11);
        endDate.set(Calendar.DATE, 31);


        BigDecimal creditCardAmountForPeriod = paidSumService.getPaidSumByCreditCardForPeriod(startDate.getTime(), endDate.getTime());
        //переводим в млн. руб.
        creditCardAmountForPeriod = creditCardAmountForPeriod.divide(new BigDecimal(1000000), 10, BigDecimal.ROUND_HALF_UP);
        creditCardAmountForPeriod = ServiceUtils.round(creditCardAmountForPeriod, new BigDecimal("0.1"));
        BigDecimal amountForPeriod = paidSumService.getPaidSumForPeriod(startDate.getTime(), endDate.getTime());
        amountForPeriod = amountForPeriod.divide(new BigDecimal(1000000), 10, BigDecimal.ROUND_HALF_UP);
        amountForPeriod = ServiceUtils.round(amountForPeriod, new BigDecimal("0.1"));

        Map<String, Object> parametrMap = new HashMap<String, Object>();
        parametrMap.put("year", year.toString());
        parametrMap.put("amountForPeriod001", amountForPeriod.toString());
        parametrMap.put("amountForPeriod037", amountForPeriod.toString());
        parametrMap.put("creditCardAmountForPeriod001", creditCardAmountForPeriod.toString());
        parametrMap.put("creditCardAmountForPeriod037", creditCardAmountForPeriod.toString());
        parametrMap.put("shortNameChiefReport", config.getShortNameChief());
        return new ModelAndView("statistics1ynPDF", parametrMap);
    }

    @RequestMapping(value = "reports/debtRegistr", method = RequestMethod.GET)
    public String dataForReportDebtRegistr(Model model,
                      @RequestParam(value = "searchText", required = false)String searchText) {
        Date configDate = configService.getCurrentDate();
        DebtRegistrPeriodForm debtRegistrPeriodForm = new DebtRegistrPeriodForm();
        debtRegistrPeriodForm.setEndPeriod(configDate);
        debtRegistrPeriodForm.setStartPeriod(new Date());
        model.addAttribute("configDate", configDate);
        model.addAttribute("periodForm", debtRegistrPeriodForm);
        List<Pacient> pacients = new LinkedList<Pacient>();
        if (searchText != null && !searchText.isEmpty()) {
            pacients = pacientService.getAllBySearchText(searchText);
            model.addAttribute("searchText", searchText);
            model.addAttribute("pacients", pacients);

            return "/report/debtRegistr";
        } else {
            searchText = "";
            model.addAttribute("searchText", searchText);
            model.addAttribute("pacients", pacients);
            return "/report/debtRegistr";
        }
    }

    @RequestMapping(value = "reports/debtRegistr", method = RequestMethod.POST)
    public String createDebtRegistr(@Valid @ModelAttribute("periodForm")DebtRegistrPeriodForm debtRegistrPeriodForm, BindingResult periodResult,
                                    RedirectAttributes redirectAttributes, Model model) {
        DebtRegistrValidator debtRegistrValidator = new DebtRegistrValidator();
        debtRegistrValidator.validate(debtRegistrPeriodForm, periodResult);
        if (periodResult.hasErrors()) {
            Date configDate = configService.getCurrentDate();
            model.addAttribute("configDate", configDate);
            model.addAttribute("startPeriod", debtRegistrPeriodForm.getStartPeriod());
            model.addAttribute("endPeriod", debtRegistrPeriodForm.getEndPeriod());
            model.addAttribute("searchText", "");
            model.addAttribute("pacients", new LinkedList<Pacient>());
            return "/report/debtRegistr";
        }
        model.addAttribute("periodForm", debtRegistrPeriodForm);
        redirectAttributes.addFlashAttribute("periodForm", debtRegistrPeriodForm);   //сохранение модели между редиректами
        return "redirect:/reports/debtRegistr/pdf";
    }

    @RequestMapping(value = "reports/debtRegistr/pdf", method = RequestMethod.GET)
    public ModelAndView generateDebtRegistrForAllPacient(@ModelAttribute("periodForm")DebtRegistrPeriodForm debtRegistrPeriodForm) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        Map<String, Object> parametrMap = new HashMap<String, Object>();
        parametrMap.put("debtRegisterForm", reportService.getDebtRegisterFormsByDate(debtRegistrPeriodForm.getStartPeriod(),
                debtRegistrPeriodForm.getEndPeriod(), null));
        parametrMap.put("startPeriod", dateFormat.format(debtRegistrPeriodForm.getStartPeriod()));
        parametrMap.put("endPeriod", dateFormat.format(debtRegistrPeriodForm.getEndPeriod()));
        return new ModelAndView("debtRegistrPDF", parametrMap);
    }

    @RequestMapping(value = "reports/debtRegistr/{pacientId}/pdf", method = RequestMethod.GET)
    public ModelAndView generateDebtRegistrForPacient(@PathVariable Long pacientId) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        Map<String, Object> parametrMap = new HashMap<String, Object>();
        Pacient pacient = pacientService.getById(pacientId);
        Date startPeriod = pacient.getContract().getDateCreated();
        Date endPeriod = periodService.getLastEndPeriod(pacient.getContract());
        parametrMap.put("debtRegisterForm", reportService.getDebtRegisterFormsByDate(startPeriod,
                endPeriod, pacient));
        parametrMap.put("startPeriod", dateFormat.format(startPeriod));
        parametrMap.put("endPeriod", dateFormat.format(endPeriod));
        return new ModelAndView("debtRegistrPDF", parametrMap);
    }

}
