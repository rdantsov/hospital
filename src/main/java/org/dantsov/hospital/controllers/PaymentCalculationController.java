package org.dantsov.hospital.controllers;

import org.dantsov.hospital.controllers.forms.calculation.PaymentCalculationSearchForm;
import org.dantsov.hospital.models.AmountForPayment;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.service.CalculatorPaymentService;
import org.dantsov.hospital.service.ConfigService;
import org.dantsov.hospital.service.PacientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 14.03.13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class PaymentCalculationController {
    @Autowired
    private CalculatorPaymentService calculatorPaymentService;
    @Autowired
    private PacientService pacientService;
    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/payment/calculation", method = RequestMethod.GET)
    public String show(@RequestParam(value = "date", required = false) @DateTimeFormat(pattern="MM.yyyy")Date date,
                             Model model) {
        Date currentDate = configService.getCurrentDate();
        PaymentCalculationSearchForm paymentCalculationSearchForm = new PaymentCalculationSearchForm();
        if (date != null) {
            paymentCalculationSearchForm.setDate(date);
        } else {
            paymentCalculationSearchForm.setDate(currentDate);
        }
        Map<String, BigDecimal> amountsForPayment =
               calculatorPaymentService.getPacientsAndAmountByDate(paymentCalculationSearchForm.getDate());
        List<Pacient> pacients = pacientService.getAll();
        model.addAttribute("pacients", pacients);

        model.addAttribute("amountsForPayment", amountsForPayment);
        model.addAttribute("paymentCalculationSearchForm", paymentCalculationSearchForm);
        model.addAttribute("currentDate", currentDate);
        return "/payment/calculation";
    }

    @RequestMapping(value = "/payment/calculation", method = RequestMethod.POST)
    public String calc(@ModelAttribute("paymentCalculationSearchForm")
                       PaymentCalculationSearchForm paymentCalculationSearchForm, BindingResult searchResult,
                       Model model) {
        Calendar calYear = Calendar.getInstance();
        calYear.setTime(paymentCalculationSearchForm.getDate());
        calculatorPaymentService.calculateAmountByMonth(paymentCalculationSearchForm.getDate(), null);
        return "redirect:/payment/calculation?date=" + (calYear.get(Calendar.MONTH) + 1) + "." + calYear.get(Calendar.YEAR);
    }

    @RequestMapping(value = "/payment/detailcalculation", method = RequestMethod.GET)
    public String showDetailCalculation(@RequestParam(value = "pacient", required = true) Long pacientId,
                                        @RequestParam(value = "date", required = true)
                                        @DateTimeFormat(pattern="MM.yyyy")Date date,
                                        Model model) {
        AmountForPayment amountForPayment = calculatorPaymentService.getAmountForPaymentByPacientAndDate(pacientId, date);
        if (amountForPayment != null && amountForPayment.getDetailCalculation() != null )  {
            model.addAttribute("detailCalculation", amountForPayment.getDetailCalculation());
        } else {
            SimpleDateFormat dateFormatShort = new SimpleDateFormat("MMMM yyyy");
            StringBuilder detailCalculation = new StringBuilder("Необходимо произвести расчет для ")
                    .append(dateFormatShort.format(date))
                    .append(" года.");
            model.addAttribute("detailCalculation",  detailCalculation.toString());
        }

        return "payment/detailCalculation";
    }

}
