package org.dantsov.hospital.controllers.validators;

import org.dantsov.hospital.controllers.forms.report.DebtRegistrPeriodForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 13/08/13
 * Time: 12:25
 * To change this template use File | Settings | File Templates.
 */
public class DebtRegistrValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == DebtRegistrPeriodForm.class;
    }

    @Override
    public void validate(Object o, Errors errors) {
        DebtRegistrPeriodForm debtRegistrPeriodForm = (DebtRegistrPeriodForm) o;
        //проверка: дата начала должна быть меньше либо равна даты окончания периода
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        if ( (debtRegistrPeriodForm.getEndPeriod().before(debtRegistrPeriodForm.getStartPeriod()))) {
            errors.rejectValue("startPeriod", "startPeriod.wrong", "Дата начала должна быть меньше либо равна даты окончания");
        }
    }
}
