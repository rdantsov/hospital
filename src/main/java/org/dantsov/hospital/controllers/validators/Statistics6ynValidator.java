package org.dantsov.hospital.controllers.validators;

import org.dantsov.hospital.controllers.forms.report.DebtRegistrPeriodForm;
import org.dantsov.hospital.controllers.forms.report.Statistics6ynPeriodForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 21/08/13
 * Time: 12:43
 * To change this template use File | Settings | File Templates.
 */
public class Statistics6ynValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == Statistics6ynPeriodForm.class;
    }

    @Override
    public void validate(Object o, Errors errors) {
        Statistics6ynPeriodForm statistics6ynPeriodForm = (Statistics6ynPeriodForm) o;
        //проверка: дата начала должна быть меньше либо равна даты окончания периода

        if ( (statistics6ynPeriodForm.getEndPeriod().before(statistics6ynPeriodForm.getStartPeriod()))) {
            errors.rejectValue("startPeriod", "startPeriod.wrong", "Дата начала должна быть меньше либо равна даты окончания.");
        }
        //Проверка: период должен быть за один год
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.setTime(statistics6ynPeriodForm.getStartPeriod());
        end.setTime(statistics6ynPeriodForm.getEndPeriod());
        int yearStart = start.get(Calendar.YEAR);
        int yearEnd = end.get(Calendar.YEAR);

        if (Math.abs(yearStart - yearEnd) != 0) {
            errors.rejectValue("startPeriod", "startPeriod.wrong", "Период должен быть за один год.");
        }




    }
}