package org.dantsov.hospital.controllers.validators;


import org.dantsov.hospital.controllers.forms.pacient.PacientForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 18.04.13
 * Time: 10:45
 * To change this template use File | Settings | File Templates.
 */
public class PacientValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == PacientForm.class;
    }

    @Override
    public void validate(Object o, Errors errors) {
        PacientForm pacientForm = (PacientForm) o;
        //проверка: дата начала должна быть меньше даты окончания договора
        if (pacientForm.getStartPeriod().after(pacientForm.getEndPeriod())) {
            errors.rejectValue("startPeriod", "startPeriod.wrong", "Дата начала должна быть меньше даты окончания");
        }

        if (pacientForm.getDateCreated().after(pacientForm.getStartPeriod())) {
            errors.rejectValue("dateCreated", "dateCreated.wrong",
                    "Дата создания договора должна быть раньше даты начало периода");
        }

    }
}
