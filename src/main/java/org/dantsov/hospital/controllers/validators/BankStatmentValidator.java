package org.dantsov.hospital.controllers.validators;

import org.dantsov.hospital.controllers.forms.paidSum.BankStatmentForm;
import org.dantsov.hospital.controllers.forms.paidSum.PaidSumForm;
import org.dantsov.hospital.service.BankStatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 29.04.13
 * Time: 10:44
 * To change this template use File | Settings | File Templates.
 */
public class BankStatmentValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return  aClass == BankStatmentForm.class;
    }

    @Override
    public void validate(Object o, Errors errors) {
        BankStatmentForm bankStatmentForm = (BankStatmentForm) o;

    }
}
