package org.dantsov.hospital.controllers.validators;

import org.dantsov.hospital.controllers.forms.pension.PensionForm;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 22.04.13
 * Time: 9:30
 * To change this template use File | Settings | File Templates.
 */
public class PensionValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == PensionForm.class;
    }

    @Override
    public void validate(Object o, Errors errors) {
        PensionForm pensionForm = (PensionForm) o;
        if (pensionForm.getAmount() == null || (pensionForm.getAmount().compareTo(BigDecimal.valueOf(0)) < 0 )) {
            errors.rejectValue("pensionForms", "pensionForms.wrong", "Размер пенсии должен быть указан и не может быть отрицательным");
        }
}
}
