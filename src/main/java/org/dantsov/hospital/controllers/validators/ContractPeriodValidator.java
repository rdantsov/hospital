package org.dantsov.hospital.controllers.validators;

import org.dantsov.hospital.controllers.forms.period.PeriodForm;
import org.dantsov.hospital.controllers.forms.report.Statistics6ynPeriodForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 22/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */
public class ContractPeriodValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == PeriodForm.class;
    }

    @Override
    public void validate(Object o, Errors errors) {
        PeriodForm periodForm = (PeriodForm) o;
        //проверка: дата начала должна быть меньше либо равна даты окончания периода

        if ( (periodForm.getEndPeriod().before(periodForm.getStartPeriod()))) {
            errors.rejectValue("startPeriod", "startPeriod.wrong", "Дата начала должна быть меньше либо равна даты окончания.");
        }
    }
}

