package org.dantsov.hospital.controllers.forms.report;

/**
 * autor Dantsov Ruslan
 */

public class DebtRegisterForm {
    private String fullName;
    private String contract;
    private String periodOfStayAndAmount;
    private String dateAndPaidSum;

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPeriodOfStayAndAmount() {
        return periodOfStayAndAmount;
    }

    public void setPeriodOfStayAndAmount(String periodOfStayAndAmount) {
        this.periodOfStayAndAmount = periodOfStayAndAmount;
    }

    public String getDateAndPaidSum() {
        return dateAndPaidSum;
    }

    public void setDateAndPaidSum(String dateAndPaidSum) {
        this.dateAndPaidSum = dateAndPaidSum;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    private String totalAmount;


}
