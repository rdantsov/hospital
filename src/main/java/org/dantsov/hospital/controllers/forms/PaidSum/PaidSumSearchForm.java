package org.dantsov.hospital.controllers.forms.paidSum;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 28.03.13
 * Time: 10:41
 * To change this template use File | Settings | File Templates.
 */
public class PaidSumSearchForm {
    private String surname;
    private String nomerContract;
    @DateTimeFormat(pattern = "MM.yyyy")
    private Date date;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNomerContract() {
        return nomerContract;
    }

    public void setNomerContract(String nomerContract) {
        this.nomerContract = nomerContract;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
