package org.dantsov.hospital.controllers.forms.paidSum;

import org.springframework.util.AutoPopulatingList;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 04.04.13
 * Time: 12:02
 * To change this template use File | Settings | File Templates.
 */
public class PaidSumListForm {
    private List<PaidSumForm> paidSumForms = new AutoPopulatingList<PaidSumForm>(PaidSumForm.class);

    public List<PaidSumForm> getPaidSumForms() {
        return paidSumForms;
    }

    public void setPaidSumForms(List<PaidSumForm> paidSumForms) {
        this.paidSumForms = paidSumForms;
    }
}
