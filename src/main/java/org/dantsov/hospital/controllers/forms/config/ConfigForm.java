package org.dantsov.hospital.controllers.forms.config;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 30.04.13
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
public class ConfigForm {
    @NotEmpty(message = "Необходимо указать полное имя заведующей")
    private String fullNameChief;

    @NotEmpty(message = "Необходимо указать ФИО заведующей в краткой форме")
    private String shortNameChief;

    @NotEmpty(message = "Необходимо указать данные о доверенности")
    private String vicariousAuthorty;

    @NotNull(message = "Необходимо указать кол-во пациентов в списке на странице")
    @Digits(integer = 2, fraction = 0, message = "Кол-во пациентов в списке на странице должно содержать цифры")
    @Min(value = 1, message = "Количество пациентов в списке на странице должно быть больше 0")
    @Max(value = 30, message = "Количество пациентов в списке на странице должно быть не больше 30")
    private Integer countPacientOnPage;
    @NotNull(message = "Необходимо указать правило округления")
    @Digits(integer = 3, fraction = 0, message = "Значение в окргулениях должно быть от 0 до 1000")
    private Integer roundingAmount;

    @NotEmpty(message = "Необходимо указать данные об имени пользователя")
    private String login;

    @Length(min = 3, max = 10, message = "Пароль должен быть более 3-ех и менее 10-ти символов")
    private String password;

    private Long actual;

    public String getFullNameChief() {
        return fullNameChief;
    }

    public void setFullNameChief(String fullNameChief) {
        this.fullNameChief = fullNameChief;
    }

    public String getShortNameChief() {
        return shortNameChief;
    }

    public void setShortNameChief(String shortNameChief) {
        this.shortNameChief = shortNameChief;
    }

    public String getVicariousAuthorty() {
        return vicariousAuthorty;
    }

    public void setVicariousAuthorty(String vicariousAuthorty) {
        this.vicariousAuthorty = vicariousAuthorty;
    }

    public Integer getCountPacientOnPage() {
        return countPacientOnPage;
    }

    public void setCountPacientOnPage(Integer countPacientOnPage) {
        this.countPacientOnPage = countPacientOnPage;
    }

    public Long getActual() {
        return actual;
    }

    public void setActual(Long actual) {
        this.actual = actual;
    }

    public Integer getRoundingAmount() {
        return roundingAmount;
    }

    public void setRoundingAmount(Integer roundingAmount) {
        this.roundingAmount = roundingAmount;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
