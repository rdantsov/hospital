package org.dantsov.hospital.controllers.forms.calculation;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 27.03.13
 * Time: 18:10
 * To change this template use File | Settings | File Templates.
 */
public class PaymentCalculationSearchForm {
    @DateTimeFormat(pattern = "MM.yyyy")
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
