package org.dantsov.hospital.controllers.forms.paidSum;

import org.dantsov.hospital.models.BankStatmentType;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 05.04.13
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 */
public class BankStatmentForm {
    private Long id;
    private Date date;
    private BankStatmentType type;

    @NotNull(message = "Необходимо указать № банковской выписки.")
    private Integer numberBankStatment;
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getNumberBankStatment() {
        return numberBankStatment;
    }

    public void setNumberBankStatment(Integer numberBankStatment) {
        this.numberBankStatment = numberBankStatment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BankStatmentType getType() {
        return type;
    }

    public void setType(BankStatmentType type) {
        this.type = type;
    }
}
