package org.dantsov.hospital.controllers.forms.pension;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

public class PensionForm {
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date month;

    @NotNull(message = "Размер пенсии должен быть указан")
    @Min(value = 0, message = "Размер пенсии должен быть больше 0")
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }
}
