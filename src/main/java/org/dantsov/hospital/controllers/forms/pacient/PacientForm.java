package org.dantsov.hospital.controllers.forms.pacient;

import org.hibernate.validator.constraints.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class PacientForm {
    @NotEmpty (message = "Фамилия должна быть указана")
    private String surname;

    @NotEmpty (message = "Имя должно быть указано")
    private String name;

    @NotEmpty (message = "Отчество должно быть указано")
    private String midleName;

    @NotEmpty (message = "Серия паспорта должна быть указана")
    @Length(min = 9, max = 9, message = "Серия паспорта должна содержать 9 символов")
    private String passport;

    @NotEmpty (message = "Орган выдавший паспорт должен быть указан")
    private String authorityIssuingPassport;

    @NotEmpty (message = "Адрес должен быть указан")
    private String address;

    @NotNull (message = "Дата дня рождения не указана")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date birthday;

    @NotEmpty (message = "Имя заказчика должно быть указано")
    private String customerName;

    @NotEmpty (message = "Фамилия заказчика должна быть указана")
    private String customerSurname;

    @NotEmpty (message = "Отчество заказчика должно быть указано")
    private String customerMidleName;

    @NotEmpty (message = "Адрес заказчика должен быть указан")
    private String customerAddress;

    @NotEmpty (message = "Серия паспорта заказчика должна быть указана.")
    @Length(min = 9, max = 9, message = "Серия паспорта заказчика должна содержать 9 символов")
    private String customerPassport;

    @NotEmpty (message = "Орган выдавший паспорт заказчика должен быть указан")
    private String customerAuthorityIssuingPassport;

    @NotNull (message = "Номер договора должен быть указан")
    private Integer contractNomer;

    @NotNull (message = "Дата начала договора должна быть указана")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date startPeriod;

    @NotNull (message = "Дата окончания договора должна быть указана")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date endPeriod;

    @NotNull (message = "Дата заключения договора должна быть указана")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateCreated;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getMidleName() {
        return midleName;
    }

    public void setMidleName(String midleName) {
        this.midleName = midleName;
    }

    public Integer getContractNomer() {
        return contractNomer;
    }

    public void setContractNomer(Integer contractNomer) {
        this.contractNomer = contractNomer;
    }

    public Date getStartPeriod() {
        return startPeriod;
    }

    public void setStartPeriod(Date startPeriod) {
        this.startPeriod = startPeriod;
    }

    public Date getEndPeriod() {
        return endPeriod;
    }

    public void setEndPeriod(Date endPeriod) {
        this.endPeriod = endPeriod;
    }

    public String getAuthorityIssuingPassport() {
        return authorityIssuingPassport;
    }

    public void setAuthorityIssuingPassport(String authorityIssuingPassport) {
        this.authorityIssuingPassport = authorityIssuingPassport;
    }



    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public String getCustomerMidleName() {
        return customerMidleName;
    }

    public void setCustomerMidleName(String customerMidleName) {
        this.customerMidleName = customerMidleName;
    }

    public String getCustomerPassport() {
        return customerPassport;
    }

    public void setCustomerPassport(String customerPassport) {
        this.customerPassport = customerPassport;
    }

    public String getCustomerAuthorityIssuingPassport() {
        return customerAuthorityIssuingPassport;
    }

    public void setCustomerAuthorityIssuingPassport(String customerAuthorityIssuingPassport) {
        this.customerAuthorityIssuingPassport = customerAuthorityIssuingPassport;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
