package org.dantsov.hospital.controllers.forms.period;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 26.03.13
 * Time: 16:02
 * To change this template use File | Settings | File Templates.
 */
public class PeriodForm {
    @NotNull(message = "Поле не может быть пустым")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date startPeriod;

    @NotNull(message = "Поле не может быть пустым")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date endPeriod;

    public Date getStartPeriod() {
        return startPeriod;
    }

    public void setStartPeriod(Date startPeriod) {
        this.startPeriod = startPeriod;
    }

    public void setEndPeriod(Date endPeriod) {
        this.endPeriod = endPeriod;
    }

    public Date getEndPeriod() {
        return endPeriod;
    }

}
