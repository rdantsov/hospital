package org.dantsov.hospital.controllers.forms.report;

import org.dantsov.hospital.models.Pacient;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 16/07/13
 * Time: 12:44
 * To change this template use File | Settings | File Templates.
 */
public class PacientForm {
    private String fullName;
    private String contract;
    private String periodOfStay;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getPeriodOfStay() {
        return periodOfStay;
    }

    public void setPeriodOfStay(String periodOfStay) {
        this.periodOfStay = periodOfStay;
    }
}
