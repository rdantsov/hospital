package org.dantsov.hospital.controllers.forms.paidSum;

import org.dantsov.hospital.models.Pacient;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 04.04.13
 * Time: 12:02
 * To change this template use File | Settings | File Templates.
 */
public class PaidSumForm {
    private Long pacientId;
    private String shortName;
    private Integer contractNumber;
    private BigDecimal amount;
    private boolean paymentCreditCard;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Long getPacientId() {
        return pacientId;
    }

    public void setPacientId(Long pacientId) {
        this.pacientId = pacientId;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }

    public boolean getPaymentCreditCard() {
        return paymentCreditCard;
    }

    public void setPaymentCreditCard(boolean paymentCreditCard) {
        this.paymentCreditCard = paymentCreditCard;
    }
}
