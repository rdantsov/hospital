package org.dantsov.hospital.controllers.forms.report;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 13/08/13
 * Time: 12:27
 * To change this template use File | Settings | File Templates.
 */
public class DebtRegistrPeriodForm {
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date startPeriod;
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date endPeriod;

    public Date getStartPeriod() {
        return startPeriod;
    }

    public void setStartPeriod(Date startPeriod) {
        this.startPeriod = startPeriod;
    }

    public Date getEndPeriod() {
        return endPeriod;
    }

    public void setEndPeriod(Date endPeriod) {
        this.endPeriod = endPeriod;
    }
}
