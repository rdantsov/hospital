package org.dantsov.hospital.controllers.forms.pension;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * autor Dantsov Ruslan
 */

public class PensionSearchForm {
    @DateTimeFormat(pattern = "yyyy")
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
