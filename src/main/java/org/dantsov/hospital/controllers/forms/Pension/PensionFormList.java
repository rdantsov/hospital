package org.dantsov.hospital.controllers.forms.pension;

import org.springframework.util.AutoPopulatingList;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 14.03.13
 * Time: 11:28
 * To change this template use File | Settings | File Templates.
 */
public class PensionFormList {
    private List<PensionForm> pensionForms = new AutoPopulatingList<PensionForm>(PensionForm.class);

    public List<PensionForm> getPensionForms() {
        return pensionForms;
    }

    public void setPensionForms(List<PensionForm> pensionForms) {
        this.pensionForms = pensionForms;
    }
}
