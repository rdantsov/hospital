package org.dantsov.hospital.controllers.forms.pacient;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 22.05.13
 * Time: 12:57
 * To change this template use File | Settings | File Templates.
 */
public class PacientListParametrsForm {
    private String searchText;
    private boolean pacientContractClose;
    private Integer page;
    private String sort;


    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public boolean isPacientContractClose() {
        return pacientContractClose;
    }

    public void setPacientContractClose(boolean pacientContractClose) {
        this.pacientContractClose = pacientContractClose;
    }
}
