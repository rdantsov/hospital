package org.dantsov.hospital.controllers;

import java.math.BigDecimal;
import java.util.*;

import javax.validation.Valid;

import org.dantsov.hospital.controllers.forms.pension.PensionForm;
import org.dantsov.hospital.controllers.forms.pension.PensionFormList;
import org.dantsov.hospital.controllers.forms.pension.PensionSearchForm;
import org.dantsov.hospital.controllers.validators.PensionValidator;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Pension;
import org.dantsov.hospital.service.ConfigService;
import org.dantsov.hospital.service.PacientService;
import org.dantsov.hospital.service.PensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class PensionController {
    @Autowired
    private PacientService pacientService;
    @Autowired
    private PensionService pensionService;
    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/pacient/{id}/pension", method = RequestMethod.GET)
    public String show(@RequestParam(value = "date", required = false) Integer year,
                       @PathVariable Long id,
                       @RequestParam(value = "page", required = false) Integer page,
                       @RequestParam(value = "searchText", required = false) String searchText,
                       @RequestParam(value = "isPacientContractClose", required = false) boolean isPacientContractClose,
                       Model model) {
        Date currentDate = configService.getCurrentDate();
        Pacient pacient = pacientService.getById(id);
        if  (pacient != null) {
            model.addAttribute("pacient", pacient);
        } else {
            return "/error/allError";
        }
        PensionSearchForm pensionSearchForm = new PensionSearchForm();
        if (year != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, 0, 1, 0, 0, 0);
            pensionSearchForm.setDate(calendar.getTime());
        } else {
            pensionSearchForm.setDate(currentDate);
        }
        List<PensionForm> pensionForms = getPensionForms(pacient, pensionSearchForm.getDate());
        PensionFormList pensionFormList = new PensionFormList();
        pensionFormList.setPensionForms(pensionForms);
        //параметры необходимы, для возврата на страницу со списком пациентов с прежними параметрами
        if (page == null) {
            page = 1;
        }
        if (searchText == null) {
            searchText = new String("");
        }

        model.addAttribute("page", page);
        model.addAttribute("searchText", searchText);
        model.addAttribute("isPacientContractClose", isPacientContractClose);
        model.addAttribute("pensionSearchForm", pensionSearchForm);
        model.addAttribute("pensionFormList", pensionFormList);
        return "/payment/pension";
    }

    @RequestMapping(value = "/pacient/{id}/pension", method = RequestMethod.POST)
    public String edit(@Valid @ModelAttribute("pensionFormList") PensionFormList pensionFormList, BindingResult pensionsResult,
                       @ModelAttribute("pensionSearchForm") PensionSearchForm pensionSearchForm, BindingResult searchResult,
                       @PathVariable Long id,
                       Model model) {
        Pacient pacient = pacientService.getById(id);
        if (pacient == null) {
            return "/error/allError";
        }
        PensionValidator pensionValidator = new PensionValidator();
        for(PensionForm pensionForm: pensionFormList.getPensionForms()) {
            pensionValidator.validate(pensionForm, pensionsResult);
        }
        if (pensionsResult.hasErrors()) {
            model.addAttribute("pacient", pacient);
            return "/payment/pension";
        }
        Calendar calendar = Calendar.getInstance();
        Calendar calendarYear = Calendar.getInstance();
        calendarYear.setTime(pensionSearchForm.getDate());
        Map<Integer, BigDecimal> amountPensions = new HashMap<Integer, BigDecimal>();
        List<PensionForm> pensionForms = pensionFormList.getPensionForms();
        if  ((pensionForms != null) && (pensionForms.size()>0) ) {
            for (PensionForm pensionForm: pensionForms) {
                calendar.setTime(pensionForm.getMonth());
                amountPensions.put(calendar.get(Calendar.MONTH), pensionForm.getAmount());
            }
            pensionService.edit(pacient, calendarYear.get(Calendar.YEAR), amountPensions);
        }
        return "redirect:/pacient/" + id + "/pension?date=" + calendarYear.get(Calendar.YEAR) ;
    }

    private List<PensionForm> getPensionForms(Pacient pacient, Date date) {
        //заполняем объект pensionForm
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        List<Pension> pensions = pensionService.getPensionsByPacientAndYear(pacient, calendar.get(Calendar.YEAR));
        List<PensionForm> pensionForms = new ArrayList<PensionForm>();
        for (Pension pension: pensions) {
            PensionForm pensionForm = new PensionForm();
            pensionForm.setAmount(pension.getAmount());
            pensionForm.setMonth(pension.getMonth());
            pensionForms.add(pensionForm);
        }
        return  pensionForms;
    }

}
