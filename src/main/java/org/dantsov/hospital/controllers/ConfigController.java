package org.dantsov.hospital.controllers;

import org.dantsov.hospital.controllers.forms.config.ConfigForm;
import org.dantsov.hospital.models.Config;
import org.dantsov.hospital.service.ConfigService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 30.04.13
 * Time: 14:18
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ConfigController  {
    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/configuration/edit", method = RequestMethod.GET)
    public String edit(Model model) {
        ModelMapper modelMapper = new ModelMapper();
        Config config = configService.getActualConfig();
        ConfigForm configForm = modelMapper.map(config, ConfigForm.class);
        model.addAttribute("configForm", configForm);
        return "/config/configEdit";
    }

    @RequestMapping(value = "/configuration/edit", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("configForm")
                        ConfigForm configForm, BindingResult configBindingResult,
                        Model model) {
        if (configBindingResult.hasErrors()) {
            return "/config/configEdit";
        }
        ModelMapper modelMapper = new ModelMapper();
        Config config = configService.getActualConfig();
        modelMapper.map(configForm, config);
        Config newConfig = new Config();
        newConfig.setCountPacientOnPage(config.getCountPacientOnPage());
        newConfig.setFullNameChief(config.getFullNameChief());
        newConfig.setShortNameChief(config.getShortNameChief());
        newConfig.setVicariousAuthorty(config.getVicariousAuthorty());
        newConfig.setId(null);
        newConfig.setDateCreated(new Date());
        newConfig.setActual(1);
        newConfig.setCurrentDate(config.getCurrentDate());
        newConfig.setRoundingAmount(config.getRoundingAmount());
        newConfig.setLogin(config.getLogin());
        newConfig.setPassword(config.getPassword());
        configService.update(newConfig);
        return "/security/login";
    }

    @RequestMapping(value = "/configuration/newMonth", method = RequestMethod.GET)
    public String newMonth(Model model) {
        Config config = configService.getActualConfig();
        Calendar calendarNextMonth = Calendar.getInstance();
        calendarNextMonth.setTime(config.getCurrentDate());
        calendarNextMonth.add(Calendar.MONTH, 1);
        model.addAttribute("currentDate", config.getCurrentDate());
        model.addAttribute("nextDate", calendarNextMonth.getTime());
        return "/config/newMonth";
    }

    @RequestMapping(value = "/configuration/newMonth", method = RequestMethod.POST)
    public String updateMonth(Model model) {
        configService.changeMonth();
        return "redirect:/pacients";
    }


}
