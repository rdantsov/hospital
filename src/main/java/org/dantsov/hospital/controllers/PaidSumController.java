package org.dantsov.hospital.controllers;

import org.dantsov.hospital.controllers.forms.paidSum.BankStatmentForm;
import org.dantsov.hospital.controllers.forms.paidSum.PaidSumForm;
import org.dantsov.hospital.controllers.forms.paidSum.PaidSumListForm;
import org.dantsov.hospital.controllers.forms.pension.PensionFormList;
import org.dantsov.hospital.controllers.forms.pension.PensionSearchForm;
import org.dantsov.hospital.controllers.validators.BankStatmentValidator;
import org.dantsov.hospital.controllers.validators.PaidSumValidator;
import org.dantsov.hospital.models.BankStatment;
import org.dantsov.hospital.models.BankStatmentType;
import org.dantsov.hospital.models.PaidSum;
import org.dantsov.hospital.service.BankStatmentService;
import org.dantsov.hospital.service.ConfigService;
import org.dantsov.hospital.service.PaidSumService;
import org.dantsov.hospital.service.impl.BankStatmentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 28.03.13
 * Time: 10:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class PaidSumController {
    @Autowired
    private PaidSumService paidSumService;
    @Autowired
    private BankStatmentService bankStatmentService;
    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/payment/paidsum/list", method = RequestMethod.GET)
    public String list(@RequestParam(value = "dateSearch", required = false) @DateTimeFormat(pattern="MM.yyyy")Date date,
            Model model) {
        if (date == null) {
            Date currentDate = configService.getCurrentDate();
            date = currentDate;
        }
        List<BankStatment> bankStatments = bankStatmentService.getByDate(date);
        Map<String, Boolean> bankStatmentsIsHaveContractClose =
                bankStatmentService.getBankStatmentsIsHaveContractClose(bankStatments);
        model.addAttribute("bankStatmentsIsHaveContractClose", bankStatmentsIsHaveContractClose);
        model.addAttribute("currentDate", date);
        model.addAttribute("dateSearch", date);
        model.addAttribute("bankStatments", bankStatments);
        return "/payment/paidSumsList";
    }

    @RequestMapping(value = "/payment/paidsum/{id}/delete", method = RequestMethod.GET)
      public String delete( @PathVariable Long id, Model model) {
        BankStatment bankStatment = bankStatmentService.getById(id);
        if (bankStatment == null) {
            return "/error/allError";
        }
        paidSumService.delete(bankStatment);
        return "redirect:/payment/paidsum/list";
    }

    @RequestMapping(value = "/payment/paidsum/{id}/show", method = RequestMethod.GET)
    public String onlyView(@PathVariable Long id, Model model) {
        BankStatment bankStatment = bankStatmentService.getById(id);
        if (bankStatment == null) {
            return "/error/allError";
        }

        BankStatmentForm bankStatmentForm = new BankStatmentForm();
        bankStatmentForm.setDate(bankStatment.getDate());
        bankStatmentForm.setNumberBankStatment(bankStatment.getNumber());
        if (bankStatment.getType() == BankStatmentType.PACIENT_PAID) {
            bankStatmentForm.setType(BankStatmentType.PACIENT_PAID);
            model.addAttribute("bankStatmentType", "pacientPaid");
        } else {
            bankStatmentForm.setType(BankStatmentType.HOSPITAL_RETURNED);
        }

        model.addAttribute("view", "view");
        model.addAttribute("currentDate", bankStatment.getDate());
        model.addAttribute("bankStatmentForm", bankStatmentForm);
        model.addAttribute("paidSumListForm", paidSumService.getPaidSumListFormByBankStatment(bankStatment));
        return "/payment/paidSums";
    }

    @RequestMapping(value = "/payment/paidsum/{id}/edit", method = RequestMethod.GET)
    public String edit( @PathVariable Long id, Model model) {
        BankStatment bankStatment = bankStatmentService.getById(id);
        if (bankStatment == null) {
            return "/error/allError";
        }

        BankStatmentForm bankStatmentForm = new BankStatmentForm();
        bankStatmentForm.setDate(bankStatment.getDate());
        bankStatmentForm.setNumberBankStatment(bankStatment.getNumber());
        if (bankStatment.getType() == BankStatmentType.PACIENT_PAID) {
            bankStatmentForm.setType(BankStatmentType.PACIENT_PAID);
            model.addAttribute("bankStatmentType", "pacientPaid");
        } else {
            bankStatmentForm.setType(BankStatmentType.HOSPITAL_RETURNED);
        }

        model.addAttribute("currentDate", bankStatment.getDate());
        model.addAttribute("bankStatmentForm", bankStatmentForm);
        model.addAttribute("paidSumListForm", paidSumService.getPaidSumListFormByBankStatment(bankStatment));
        return "/payment/paidSums";
    }

    @RequestMapping(value = "/payment/paidsum/{id}/edit", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("paidSumListForm") PaidSumListForm paidSumListForm, BindingResult paidSumResult,
                       @Valid @ModelAttribute("bankStatmentForm") BankStatmentForm bankStatmentForm, BindingResult bankStatmentResult,
                       @PathVariable Long id, Model model) {
        BankStatment bankStatment = bankStatmentService.getById(id);
        if (bankStatment == null) {
            return "/error/allError";
        }
        PaidSumValidator paidSumValidator = new PaidSumValidator();
        int countPacientHavingAmountGreaterZero = 0;
        for(PaidSumForm paidSumForm: paidSumListForm.getPaidSumForms()) {
            paidSumValidator.validate(paidSumForm, paidSumResult);
            if (paidSumForm.getAmount() != null && paidSumForm.getAmount().compareTo(BigDecimal.valueOf(0)) == 1) {
                countPacientHavingAmountGreaterZero++;
            }
        }
        //проверка чтобы в выписке был хотя бы один человек с суммой больше 0
        if (countPacientHavingAmountGreaterZero == 0) {
            paidSumResult.rejectValue("paidSumForms", "paidSumForms.wrong",
                    "В выписке должен быть указан хотя бы один пациент с суммой больше 0");
        }

        if (paidSumResult.hasErrors() || bankStatmentResult.hasErrors()) {
            Date currentDate = configService.getCurrentDate();
            model.addAttribute("currentDate", currentDate);
            if (bankStatmentForm.getType() == BankStatmentType.PACIENT_PAID) {
                model.addAttribute("bankStatmentType", "pacientPaid");
            }
            return "/payment/paidSums";
        }

        bankStatmentForm.setId(id);
        paidSumService.saveOrUpdate(paidSumListForm, bankStatmentForm);
        return "redirect:/payment/paidsum/list";
    }

    @RequestMapping(value = "/payment/paidsum/pacient/new", method = RequestMethod.GET)
    public String addPaidSumPacient(Model model) {
        Date currentDate = configService.getCurrentDate();
        model.addAttribute("currentDate", currentDate);
        BankStatmentForm bankStatmentForm = new BankStatmentForm();
        bankStatmentForm.setDate(currentDate);
        bankStatmentForm.setType(BankStatmentType.PACIENT_PAID);
        model.addAttribute("bankStatmentForm", bankStatmentForm);
        model.addAttribute("new", "new");
        model.addAttribute("bankStatmentType", "pacientPaid");
        model.addAttribute("paidSumListForm", paidSumService.getNewPaidSumListForm());
        return "/payment/paidSums";
    }

    @RequestMapping(value = "/payment/paidsum/pacient/new", method = RequestMethod.POST)
    public String createPaidSumPacient(@Valid @ModelAttribute("paidSumListForm") PaidSumListForm paidSumListForm,
                                       BindingResult paidSumResult,
                                       @Valid @ModelAttribute("bankStatmentForm") BankStatmentForm bankStatmentForm,
                                       BindingResult bankStatmentResult,
                                       Model model) {
        PaidSumValidator paidSumValidator = new PaidSumValidator();
        int countPacientHavingAmountGreaterZero = 0;
        for(PaidSumForm paidSumForm: paidSumListForm.getPaidSumForms()) {
            paidSumValidator.validate(paidSumForm, paidSumResult);
            if (paidSumForm.getAmount() != null && paidSumForm.getAmount().compareTo(BigDecimal.valueOf(0)) == 1) {
                countPacientHavingAmountGreaterZero++;
            }
        }

        if (bankStatmentForm.getDate() != null && bankStatmentForm.getNumberBankStatment() != null) {
            if (bankStatmentService.getByDateAndNumber(bankStatmentForm.getDate(),
                    bankStatmentForm.getNumberBankStatment()) != null) {
                bankStatmentResult.rejectValue("numberBankStatment", "numberBankStatment.wrong",
                        "Выписка с таким номером и за эту дату уже существует");
            }
        }

        //проверка чтобы в выписке был хотя бы один человек с суммой больше 0
        if (countPacientHavingAmountGreaterZero == 0) {
            paidSumResult.rejectValue("paidSumForms", "paidSumForms.wrong",
                    "В выписке должен быть указан хотя бы один пациент с суммой больше 0");
        }

        if (paidSumResult.hasErrors() || bankStatmentResult.hasErrors()) {
            Date currentDate = configService.getCurrentDate();
            model.addAttribute("currentDate", currentDate);
            model.addAttribute("new", "new");
            model.addAttribute("bankStatmentType", "pacientPaid");
            model.addAttribute("bankStatmentForm", bankStatmentForm);
            model.addAttribute("paidSumListForm", paidSumListForm);
            return "/payment/paidSums";
        }
        bankStatmentForm.setType(BankStatmentType.PACIENT_PAID);
        paidSumService.saveOrUpdate(paidSumListForm, bankStatmentForm);
        return "redirect:/payment/paidsum/list";
    }

    //реестр возврата излишней оплаты, отличие от рееестра оплаты:
    // выставляем соотвестсвующий тип в банковской выписке
    // при сохранении введенные суммы делаем со знаком минус, что бы расчеты были правильными
    @RequestMapping(value = "/payment/paidsum/hospital/new", method = RequestMethod.GET)
    public String addPaidSumHospital(Model model) {
        Date currentDate = configService.getCurrentDate();
        model.addAttribute("currentDate", currentDate);
        BankStatmentForm bankStatmentForm = new BankStatmentForm();
        bankStatmentForm.setDate(currentDate);
        bankStatmentForm.setType(BankStatmentType.HOSPITAL_RETURNED);
        model.addAttribute("bankStatmentForm", bankStatmentForm);
        model.addAttribute("new", "new");
        model.addAttribute("paidSumListForm", paidSumService.getNewPaidSumListForm());
        return "/payment/paidSums";
    }

    @RequestMapping(value = "/payment/paidsum/hospital/new", method = RequestMethod.POST)
    public String createPaidSumHospital(@Valid @ModelAttribute("paidSumListForm") PaidSumListForm paidSumListForm, BindingResult paidSumResult,
                         @Valid @ModelAttribute("bankStatmentForm") BankStatmentForm bankStatmentForm, BindingResult bankStatmentResult,
                         Model model) {
        PaidSumValidator paidSumValidator = new PaidSumValidator();
        int countPacientHavingAmountGreaterZero = 0;
        //проверка на ошибки
        for(PaidSumForm paidSumForm: paidSumListForm.getPaidSumForms()) {
            paidSumValidator.validate(paidSumForm, paidSumResult);
            if (paidSumForm.getAmount() != null && paidSumForm.getAmount().compareTo(BigDecimal.valueOf(0)) == 1) {
                countPacientHavingAmountGreaterZero++;
            }
        }

        //проверка чтобы в выписке был хотя бы один человек с суммой больше 0
        if (countPacientHavingAmountGreaterZero == 0) {
            paidSumResult.rejectValue("paidSumForms", "paidSumForms.wrong",
                    "В выписке должен быть указан хотя бы один пациент с суммой больше 0");
        }

        if (bankStatmentForm.getDate() != null && bankStatmentForm.getNumberBankStatment() != null) {
            if (bankStatmentService.getByDateAndNumber(bankStatmentForm.getDate(),
                    bankStatmentForm.getNumberBankStatment()) != null) {
                bankStatmentResult.rejectValue("numberBankStatment", "numberBankStatment.wrong",
                        "Выписка с таким номером и за эту дату уже существует");
            }
        }

        if (paidSumResult.hasErrors() || bankStatmentResult.hasErrors()) {
            Date currentDate = configService.getCurrentDate();
            model.addAttribute("currentDate", currentDate);
            model.addAttribute("new", "new");
            model.addAttribute("pacient", "pacient");
            model.addAttribute("bankStatmentForm", bankStatmentForm);
            model.addAttribute("paidSumListForm", paidSumListForm);
            return "/payment/paidSums";
        }
        bankStatmentForm.setType(BankStatmentType.HOSPITAL_RETURNED);
        paidSumService.saveOrUpdate(paidSumListForm, bankStatmentForm);
        return "redirect:/payment/paidsum/list";
    }

}
