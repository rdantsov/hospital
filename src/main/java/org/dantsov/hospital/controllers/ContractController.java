package org.dantsov.hospital.controllers;

import org.dantsov.hospital.controllers.forms.period.PeriodForm;
import org.dantsov.hospital.controllers.validators.ContractPeriodValidator;
import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Period;
import org.dantsov.hospital.service.*;
import org.dantsov.hospital.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 26.03.13
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ContractController {
    @Autowired
    private ContractService contractService;
    @Autowired
    private PacientService pacientService;
    @Autowired
    private PeriodService periodService;
    @Autowired
    private CalculatorPaymentService calculatorPaymentService;
    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/pacient/contract/{id}/period/new", method = RequestMethod.GET)
    public String add(@PathVariable Long id, Model model) {
        Contract contract = contractService.getById(id);
        if (contract == null) {
            return "/error/allError";
        }
        Pacient pacient = pacientService.getByContract(contract);
        PeriodForm periodForm = new PeriodForm();
        Date startPeriod = periodService.getLastEndPeriod(contract);

        //день начала нового периода делаем на день больше дня окончания предыдщего периода
        Calendar calStartPeriod = Calendar.getInstance();
        calStartPeriod.setTime(startPeriod);
        calStartPeriod.add(Calendar.DATE, 1);
        startPeriod = calStartPeriod.getTime();
        periodForm.setStartPeriod(startPeriod);

        //значение период по делаем на месяц больше
        Calendar calNextPeriod = DateUtils.getEndPeriodByStartPeriod(calStartPeriod);
        periodForm.setEndPeriod(calNextPeriod.getTime());
        model.addAttribute("calNextPeriod", calNextPeriod.getTime());
        model.addAttribute("pacient", pacient);
        model.addAttribute("periodForm", periodForm);
        model.addAttribute("new", "new");
        return "/contract/editPeriod";
    }

    @RequestMapping(value = "/pacient/contract/{id}/period/new", method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute("periodForm")
                         final PeriodForm periodForm, final BindingResult result,
                         @PathVariable Long id, Model model) {
        Contract contract = contractService.getById(id);
        Pacient pacient = pacientService.getByContract(contract);
        ContractPeriodValidator contractPeriodValidator = new ContractPeriodValidator();
        contractPeriodValidator.validate(periodForm, result);
        if (result.hasErrors()) {
            model.addAttribute("calNextPeriod", periodForm.getEndPeriod());
            model.addAttribute("pacient", pacient);
            model.addAttribute("periodForm", periodForm);
            model.addAttribute("new", "new");
            return "/contract/editPeriod";
        }
        if (contract == null) {
            return "/error/allError";
        }

        Period period = new Period();
        period.setContract(contract);
        period.setStartPeriod(periodForm.getStartPeriod());
        period.setEndPeriod(periodForm.getEndPeriod());
        periodService.save(period);
        return "redirect:/pacient/" + pacient.getId() + "/contract/list";
    }

    @RequestMapping(value = "/pacient/contract/{contractId}/period/{periodId}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable Long periodId,
                       @PathVariable Long contractId,
                       Model model) {
        Contract contract = contractService.getById(contractId);
        Pacient pacient = pacientService.getByContract(contract);
        if (contract == null || pacient == null) {
            return "/error/allError";
        }
        Period period = periodService.getById(periodId);

        PeriodForm periodForm = new PeriodForm();
        periodForm.setStartPeriod(period.getStartPeriod());
        periodForm.setEndPeriod(period.getEndPeriod());

        //данные дла возможности редактирования поле пириод с если он единственный в списке
        List<Period> periods = periodService.getByContract(pacient.getContract());
        if (periods.size() == 1) {
            model.addAttribute("onlyOnePeriod", "yes");
        }
        model.addAttribute("calNextPeriod", period.getEndPeriod());
        model.addAttribute("pacient", pacient);
        model.addAttribute("periodForm", periodForm);
        return "/contract/editPeriod";
    }

    @RequestMapping(value = "/pacient/contract/{contractId}/period/{periodId}/edit", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("periodForm")
                         PeriodForm periodForm, BindingResult result,
                         @PathVariable Long periodId,
                         @PathVariable Long contractId,
                         Model model) {
        Contract contract = contractService.getById(contractId);
        Pacient pacient = pacientService.getByContract(contract);
        ContractPeriodValidator contractPeriodValidator = new ContractPeriodValidator();
        contractPeriodValidator.validate(periodForm, result);
        if (result.hasErrors()) {
            Period period = periodService.getById(periodId);
            model.addAttribute("calNextPeriod", period.getEndPeriod());
            model.addAttribute("pacient", pacient);
            model.addAttribute("periodForm", periodForm);
            return "/contract/editPeriod";
        }
        if (contract == null || pacient == null) {
            return "/error/allError";
        }
        Period period = periodService.getById(periodId);
        period.setStartPeriod(periodForm.getStartPeriod());
        period.setEndPeriod(periodForm.getEndPeriod());
        periodService.save(period);
        return "redirect:/pacient/" + pacient.getId() + "/contract/list";
    }

    @RequestMapping(value = "/pacient/{pacientId}/contract/period/{periodId}/delete", method = RequestMethod.GET)
    public String delete(@PathVariable Long periodId,
                         @PathVariable Long pacientId,
                         Model model) {
        Period period = periodService.getById(periodId);
        if (period == null) {
            return "/error/allError";
        }
        periodService.delete(periodId);
        return "redirect:/pacient/" + pacientId + "/contract/list";
    }

    @RequestMapping(value = "/pacient/{id}/contract/list", method = RequestMethod.GET)
    public String list(@PathVariable Long id, Model model) {
        Pacient pacient = pacientService.getById(id);
        if (pacient == null) {
            return "/error/allError";
        }
        List<Period> periods = periodService.getByContract(pacient.getContract());
        model.addAttribute("pacient", pacient);
        model.addAttribute("periods", periods);
        return "/contract/list";
    }

    @RequestMapping(value = "/pacient/{id}/contract/reopen", method = RequestMethod.GET)
    public String reopen(@PathVariable Long id, Model model) {
        Pacient pacient = pacientService.getById(id);
        if (pacient == null) {
            return "/error/allError";
        }
        Contract contract = pacient.getContract();
        contract.setCompleted(0);
        contractService.saveOrUpdate(contract);
        return "redirect:/pacients";
    }
    @RequestMapping(value = "/pacient/{id}/contract/close", method = RequestMethod.GET)
    public String close(@PathVariable Long id, Model model) {
        /*
         ** при закрытии договора, делаем полный расчет и проверяем
         * если окончательная сумма равна 0, значит даем возможность его закрыть
         * иначе выдаем сообщение о том что необходимо произвести взаиморасчеты
         */
        Date currentDate = configService.getCurrentDate();
        Pacient pacient = pacientService.getById(id);
        if (pacient == null) {
            return "/error/allError";
        }
        Date endDogovor = periodService.getLastEndPeriod(pacient.getContract());
        Calendar startPeriod = Calendar.getInstance();
        Calendar endPeriod = Calendar.getInstance();
        startPeriod.setTime(endDogovor);
        endPeriod.setTime(currentDate);
        calculatorPaymentService.calculateAmountByMonth(currentDate, pacient);
        BigDecimal amount = calculatorPaymentService.
                getAmountForPaymentByPacientAndDate(pacient.id, currentDate).getAmount();
        StringBuilder error = null;
        StringBuilder success = null;

        if (amount.compareTo(BigDecimal.ZERO) == 0) {
            success = new StringBuilder("Взаиморасчеты между пациентом и больницей выполнены, договор можно закрыть.");
        }

        if (amount.compareTo(BigDecimal.ZERO) == -1) {
            if (DateUtils.daysBetween(startPeriod, endPeriod) > 63) {
                success = new StringBuilder("Задолженность больницы перед пациентом составляет ")
                        .append(amount.abs().toString())
                        .append(" руб. Срок исковой даности превысил 63 дня, поэтому возможно закрытие договора!");
            } else {
                error = new StringBuilder("Задолженность больницы перед пациентом составляет ")
                        .append(amount.abs().toString())
                        .append(" руб. Договор закрыть не возможно!");
            }
        }

        if (amount.compareTo(BigDecimal.ZERO) == 1) {
            error = new StringBuilder("Задолженность пациента перед больницей составляет ")
                    .append(amount.toString())
                    .append(" руб. Договор закрыть не возможно!");
        }

        model.addAttribute("error", error);
        model.addAttribute("success", success);
        return "contract/close";
    }

    @RequestMapping(value = "/pacient/{id}/contract/close", method = RequestMethod.POST)
    public String updateAfterClosing(@PathVariable Long id, Model model) {
        Pacient pacient = pacientService.getById(id);
        if (pacient == null) {
            return "/error/allError";
        }
        Contract contract = pacient.getContract();
        contract.setCompleted(1);
        contractService.saveOrUpdate(contract);
        return "redirect:/pacients";
    }

}
