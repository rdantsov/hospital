package org.dantsov.hospital.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 20.05.13
 * Time: 11:52
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ErrorController {

    @RequestMapping("/error/allError")
    public String show(Exception ex, HttpServletRequest request) {
        return "/error/allError";
    }
}
