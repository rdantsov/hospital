package org.dantsov.hospital.service;

import org.dantsov.hospital.controllers.forms.paidSum.BankStatmentForm;
import org.dantsov.hospital.controllers.forms.paidSum.PaidSumListForm;
import org.dantsov.hospital.models.BankStatment;
import org.dantsov.hospital.models.PaidSum;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 04.04.13
 * Time: 13:06
 * To change this template use File | Settings | File Templates.
 */
public interface PaidSumService {

    PaidSumListForm getNewPaidSumListForm();

    @Transactional
    PaidSumListForm getPaidSumListFormByBankStatment(BankStatment bankStatment);

    @Transactional
    void saveOrUpdate(PaidSumListForm paidSumListForm, BankStatmentForm bankStatmentForm);

    @Transactional
    PaidSum getById(Long id);

    @Transactional
    void delete(BankStatment bankStatment);

    @Transactional
    BigDecimal getPaidSumForPeriod(Date startDate, Date endDate);

    @Transactional
    BigDecimal getPaidSumByCreditCardForPeriod(Date startDate, Date endDate);
}
