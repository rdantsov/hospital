package org.dantsov.hospital.service;

import org.dantsov.hospital.controllers.forms.pacient.PacientForm;
import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Period;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rus
 * Date: 07.02.13
 * Time: 18:00
 * To change this template use File | Settings | File Templates.
 */
public interface PacientService {
    Pacient getById(Long id);

    void save(Pacient pacient, Contract contract, Period period);

    List<Pacient> getAll();

    Pacient getByContract(Contract contract);

    PacientForm getPacientFormByPacient(Pacient pacient);

    Map<String, String> getMapPacientsWithContracrtStatus(List<Pacient> pacients);

    @Transactional
    List<org.dantsov.hospital.controllers.forms.report.PacientForm> getPacientListForReport(Date dateCreation);

    @Transactional
    List<Pacient> getPacientListForDebtRegistrReport(Date startPeriod, Date endPeriod);

    @Transactional
    List<Pacient> getAllBySearchText(String searchText);

    @Transactional
    List<Pacient> getAllByPageAndFilter(int numberOfPage, int countOnPage,
                                        String searchText, boolean isPacientContractClose, String sort);

    @Transactional
    List<Pacient> getAllByPage(int numberOfPage, int countOnPage, String sort);
}
