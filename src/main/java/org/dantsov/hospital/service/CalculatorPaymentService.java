package org.dantsov.hospital.service;

import org.dantsov.hospital.models.AmountForPayment;
import org.dantsov.hospital.models.Pacient;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 14.03.13
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
public interface CalculatorPaymentService {

    Map<String, BigDecimal> getPacientsAndAmountByDate(Date monthForPayment);

    AmountForPayment getAmountForPaymentByPacientAndDate(Long pacientId, Date date);

    void calculateAmountByMonth(Date monthToCalculate, Pacient pacientToCalculate);

}
