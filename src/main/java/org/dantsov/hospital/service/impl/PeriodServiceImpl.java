package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Period;
import org.dantsov.hospital.repository.PeriodRepository;
import org.dantsov.hospital.service.PeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 27.03.13
 * Time: 12:16
 * To change this template use File | Settings | File Templates.
 */
@Component
public class PeriodServiceImpl implements PeriodService {
    @Autowired
    private PeriodRepository periodRepository;

    @Override
    @Transactional
    public List<Period> getByContract(Contract contract) {
        return periodRepository.getByContract(contract);
    }

    @Override
    @Transactional
    public Date getLastEndPeriod(Contract contract){
        return periodRepository.getLastEndPeriod(contract);
    }

    @Override
    @Transactional
    public void save(Period period) {
        periodRepository.save(period);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Period period = periodRepository.getById(id);
        periodRepository.delete(period);
    }

    @Override
    @Transactional
    public Period getById(Long id) {
      return periodRepository.getById(id);
    }

    @Override
    @Transactional
    public Period getFirstPeriodByContract(Contract contract) {
        return periodRepository.getFirstPeriodByContract(contract);
    }
}
