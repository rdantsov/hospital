package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.repository.ContractRepository;
import org.dantsov.hospital.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.ListResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 26.03.13
 * Time: 16:01
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ContractServiceImpl implements ContractService {
    @Autowired
    private ContractRepository contractRepository;

    @Override
    @Transactional
    public Contract getById(Long id) {
        return contractRepository.getById(id);
    }

    @Override
    @Transactional
    public Contract getByPacient(Pacient pacient) {
        return contractRepository.getByPacient(pacient);
    }

    @Override
    @Transactional
    public void saveOrUpdate(Contract contract) {
        contractRepository.saveOrUpdate(contract);
    }

    @Override
    @Transactional
    public Integer getMaxNumberOfContractByYear(Date date) {
        return contractRepository.getMaxNumberOfContractByYear(date);
    }
}
