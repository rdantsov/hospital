package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.models.Config;
import org.dantsov.hospital.models.Pension;
import org.dantsov.hospital.models.Users;
import org.dantsov.hospital.repository.ConfigRepository;
import org.dantsov.hospital.repository.PensionRepository;
import org.dantsov.hospital.repository.UsersRepository;
import org.dantsov.hospital.service.ConfigService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 30.04.13
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ConfigSrviceImpl implements ConfigService {
    @Autowired
    private ConfigRepository configRepository;
    @Autowired
    private PensionRepository pensionRepository;
    @Autowired
    private UsersRepository usersRepository;

    @Override
    @Transactional
    public Date getCurrentDate() {
        return configRepository.getCurrentDate();
    }

    @Override
    @Transactional
    public Config getActualConfig() {
        return configRepository.getActualConfig();
    }

    @Override
    @Transactional
    public void update(Config newConfig) {
        Config oldConfig = configRepository.getActualConfig();
        oldConfig.setActual(0);
        //получаем имя залогиневшегося пользователя
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = null;
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        Users user = usersRepository.getUserByName(userName);
        user.setUsername(newConfig.getLogin());
        user.setPassword(newConfig.getPassword());
        configRepository.update(oldConfig);
        configRepository.update(newConfig);
        usersRepository.saveOrUpdate(user);
    }

    @Override
    @Transactional
    public void changeMonth() {
        ModelMapper modelMapper = new ModelMapper();
        Config oldConfig = configRepository.getActualConfig();
        oldConfig.setActual(0);
        Calendar calendarNextMonth = Calendar.getInstance();
        calendarNextMonth.setTime(oldConfig.getCurrentDate());
        calendarNextMonth.add(Calendar.MONTH, 1);
        List<Pension> pensions = pensionRepository.getByDate(oldConfig.getCurrentDate());
        List<Pension> pensionsNextMonth = pensionRepository.getByDate(calendarNextMonth.getTime());
        /*
            * переносим пенссии из текущего месяца в следующий, по следующему алгоритму
            * если пенсия в следующем месяце уже есть и она не ровна 0, то ее не перезатераем
            * иначе либо достаем существующую пенсию но с размером ноль и задаем нужный размер,
            * либо создаем новую пенсии с нужным размером
         */

        for (Pension pension: pensions) {
            Pension newPension = new Pension();
            for (Pension pensionNextMonth: pensionsNextMonth) {
                if (pension.getPacient() == pensionNextMonth.getPacient()) {
                    newPension = pensionNextMonth;
                    if (newPension.getAmount().compareTo(BigDecimal.valueOf(0)) == 0) {
                        newPension.setAmount(pension.getAmount());
                    }
                }
            }
            if (newPension.getAmount() == null) {
                newPension.setAmount(pension.getAmount());
            }

            newPension.setPacient(pension.getPacient());
            newPension.setMonth(calendarNextMonth.getTime());
            pensionRepository.saveOrUpdate(newPension);
        }
        configRepository.update(oldConfig);
        Config newConfig = new Config();
        newConfig.setCountPacientOnPage(oldConfig.getCountPacientOnPage());
        newConfig.setFullNameChief(oldConfig.getFullNameChief());
        newConfig.setShortNameChief(oldConfig.getShortNameChief());
        newConfig.setVicariousAuthorty(oldConfig.getVicariousAuthorty());
        newConfig.setId(null);
        newConfig.setDateCreated(new Date());
        newConfig.setActual(1);
        newConfig.setCurrentDate(calendarNextMonth.getTime());
        newConfig.setRoundingAmount(oldConfig.getRoundingAmount());
        configRepository.update(newConfig);
    }
}
