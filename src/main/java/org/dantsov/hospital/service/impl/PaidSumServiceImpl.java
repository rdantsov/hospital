package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.controllers.forms.paidSum.BankStatmentForm;
import org.dantsov.hospital.controllers.forms.paidSum.PaidSumForm;
import org.dantsov.hospital.controllers.forms.paidSum.PaidSumListForm;
import org.dantsov.hospital.models.BankStatment;
import org.dantsov.hospital.models.BankStatmentType;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.PaidSum;
import org.dantsov.hospital.repository.BankStatmentRepository;
import org.dantsov.hospital.repository.PacientRepository;
import org.dantsov.hospital.repository.PaidSumRepository;
import org.dantsov.hospital.service.PaidSumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 04.04.13
 * Time: 13:07
 * To change this template use File | Settings | File Templates.
 */
@Component
public class PaidSumServiceImpl implements PaidSumService {
    @Autowired
    private PacientRepository pacientRepository;
    @Autowired
    private PaidSumRepository paidSumRepository;
    @Autowired
    private BankStatmentRepository bankStatmentRepository;

    @Override
    @Transactional
    public PaidSum getById(Long id) {
        return paidSumRepository.getById(id);
    }

    @Override
    @Transactional
    public PaidSumListForm getNewPaidSumListForm() {
        PaidSumListForm paidSumListForm = new PaidSumListForm();
        List<PaidSumForm> paidSumForms = new ArrayList<PaidSumForm>();
        List<Pacient> pacients = pacientRepository.getAllWithContractOrderByContractNumber();
        for(Pacient pacient: pacients) {
            PaidSumForm paidSumForm = new PaidSumForm();
            paidSumForm.setPacientId(pacient.getId());
            paidSumForm.setShortName(pacient.getSurname() + " " + pacient.getName());
            paidSumForm.setContractNumber(pacient.getContract().getNomer());
            paidSumForm.setAmount(new BigDecimal(0));
            paidSumForm.setPaymentCreditCard(false);
            paidSumForms.add(paidSumForm);
        }
        paidSumListForm.setPaidSumForms(paidSumForms);
        return paidSumListForm;
    }

    @Override
    @Transactional
    public PaidSumListForm getPaidSumListFormByBankStatment(BankStatment bankStatment) {
        PaidSumListForm paidSumListForm = new PaidSumListForm();
        List<PaidSumForm> paidSumForms = new ArrayList<PaidSumForm>();
        List<PaidSum> paidSums = paidSumRepository.getByBankStatment(bankStatment);
        for(PaidSum paidSum: paidSums) {
            PaidSumForm paidSumForm = new PaidSumForm();
            paidSumForm.setPacientId(paidSum.getPacient().getId());
            paidSumForm.setShortName(paidSum.getPacient().getSurname() + " " + paidSum.getPacient().getName());
            paidSumForm.setContractNumber(paidSum.getPacient().getContract().getNomer());
            paidSumForm.setAmount(paidSum.getAmount().abs());
            if (paidSum.getPaymentCreditCard() == 1) {
                paidSumForm.setPaymentCreditCard(true);
            } else {
                paidSumForm.setPaymentCreditCard(false);
            }

            paidSumForms.add(paidSumForm);
        }
        paidSumListForm.setPaidSumForms(paidSumForms);
        return paidSumListForm;
    }

    @Override
    @Transactional
    public void saveOrUpdate(PaidSumListForm paidSumListForm, BankStatmentForm bankStatmentForm) {
        BankStatment bankStatment = bankStatmentRepository.getById(bankStatmentForm.getId());
        if (bankStatment == null) {
            bankStatment = new BankStatment();
            if (bankStatmentForm.getType() == BankStatmentType.PACIENT_PAID) {
                bankStatment.setType(BankStatmentType.PACIENT_PAID);
            } else {
                bankStatment.setType(BankStatmentType.HOSPITAL_RETURNED);
            }
        }
        bankStatment.setDate(bankStatmentForm.getDate());
        bankStatment.setNumber(bankStatmentForm.getNumberBankStatment());

        bankStatmentRepository.save(bankStatment);
        List<PaidSum> paidSums = new ArrayList<PaidSum>();
        List<PaidSumForm> paidSumForms = paidSumListForm.getPaidSumForms();
        for (PaidSumForm paidSumForm: paidSumForms) {
            //не сохраняем людей у которых стоит 0 в поле сумма
            if (paidSumForm.getAmount().compareTo(BigDecimal.valueOf(0)) == 1) {
                PaidSum paidSum = paidSumRepository.getByPacientIdAndBankStatmentId(paidSumForm.getPacientId(),
                        bankStatment.getId());
                if (paidSum == null) {
                    paidSum = new PaidSum();
                }
                if (bankStatment.getType() == BankStatmentType.PACIENT_PAID) {
                    paidSum.setAmount(paidSumForm.getAmount());
                } else {
                    paidSum.setAmount(paidSumForm.getAmount().multiply(new BigDecimal(-1)));
                }
                paidSum.setPacient(pacientRepository.getById(paidSumForm.getPacientId()));
                paidSum.setBankStatment(bankStatment);
                if (paidSumForm.getPaymentCreditCard()) {
                    paidSum.setPaymentCreditCard(1);
                } else {
                    paidSum.setPaymentCreditCard(0);
                }
                paidSums.add(paidSum);
            }
        }
        for (PaidSum paidSum: paidSums) {
            paidSumRepository.save(paidSum);
        }
    }

    @Override
    @Transactional
    public void delete (BankStatment bankStatment) {
        List<PaidSum> paidSums = paidSumRepository.getByBankStatment(bankStatment);
        for (PaidSum paidSum: paidSums) {
            paidSumRepository.delete(paidSum);
        }
        bankStatmentRepository.delete(bankStatment);
    }

    @Override
    @Transactional
    public BigDecimal getPaidSumForPeriod(Date startDate, Date endDate) {
        return paidSumRepository.getPaidSumForPeriod(startDate, endDate);

    }

    @Override
    @Transactional
    public BigDecimal getPaidSumByCreditCardForPeriod(Date startDate, Date endDate) {
        return paidSumRepository.getPaidSumByCreditCardForPeriod(startDate, endDate);

    }
}
