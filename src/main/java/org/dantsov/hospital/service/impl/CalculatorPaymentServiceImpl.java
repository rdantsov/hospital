package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.models.AmountForPayment;
import org.dantsov.hospital.models.BankStatment;
import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.repository.*;
import org.dantsov.hospital.service.CalculatorPaymentService;
import org.dantsov.hospital.service.ConfigService;
import org.dantsov.hospital.service.ServiceUtils;
import org.dantsov.hospital.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 14.03.13
 * Time: 13:01
 * To change this template use File | Settings | File Templates.
 */

/*
ИНСТРУКЦИЯ ДЛЯ РАСЧЕТОВ
1. ищем актуальный договор для пациента (у договора поле completed должно быть 0)
разобраться с завершением договра, когда приосходит, предлагаю выдавать список с истекшим сроком и руками закрывать
2. по договору находим минимальную дату начала и максимальную дату окончания
3. проверяем чтобы текущий месяц расчета не совпадал с месяцем окончания договора, если не совпадает проводим расчет
    и текущего периода +1 месяц, иначе только за прошлый период.

*/
@Component
public class CalculatorPaymentServiceImpl implements CalculatorPaymentService {
    @Autowired
    private ContractRepository contractRepository;
    @Autowired
    private PeriodRepository periodRepository;
    @Autowired
    private PensionRepository pensionRepository;
    @Autowired
    private PacientRepository pacientRepository;
    @Autowired
    private AmountForPaymentRepository amountForPaymentRepository;
    @Autowired
    private PaidSumRepository paidSumRepository;
    @Autowired
    private ConfigService configService;

    @Override
    @Transactional
    public AmountForPayment getAmountForPaymentByPacientAndDate(Long pacientId, Date date) {
        Pacient pacient = pacientRepository.getById(pacientId);
        return amountForPaymentRepository.getByPacientAndDate(pacient, date);
    }

    @Override
    @Transactional
    public Map<String, BigDecimal> getPacientsAndAmountByDate(Date monthForPayment) {
        //todo: разрулить все через один запрос, что бы он возвращал мар пациент, сумма
        Map<String, BigDecimal> mapAmountsByPacient = new HashMap<String, BigDecimal>();
        List<Pacient> pacients = pacientRepository.getAllWithoutClose();
        AmountForPayment amountForPayment;
        for(Pacient pacient: pacients) {
            amountForPayment = amountForPaymentRepository.getByPacientAndDate(pacient, monthForPayment);
            if (amountForPayment == null) {
                mapAmountsByPacient.put(pacient.getId().toString(), new BigDecimal(0));
            } else {
                mapAmountsByPacient.put(pacient.getId().toString(), amountForPayment.getAmount());
            }
        }
        return mapAmountsByPacient;
    }

    @Override
    @Transactional
    //если в качестве пациента будет передан null, расчет будет произведен для всех пациентов
    //у которых договор не закончился до последнего числа месяца переданного в качестве параметра даты
    public void calculateAmountByMonth(Date monthToCalculate, Pacient pacientToCalculate) {
        SimpleDateFormat dateFormatShort = new SimpleDateFormat("dd.MM.yyyy");
        List<Pacient> pacients;
        if (pacientToCalculate == null) {
            pacients = pacientRepository.getAllWithoutClose();
        } else {
            pacients = new ArrayList<Pacient>();
            pacients.add(pacientToCalculate);
        }

        for(Pacient pacient: pacients) {
            StringBuilder detailCalculation = new StringBuilder("");
            addMessageForDetailCalculation(detailCalculation,
                    pacient.getSurname(),
                    " ",
                    pacient.getName(),
                    " Договор №",
                    pacient.getContract().getNomer().toString(),
                    " от ",
                    pacient.getContract().getDateCreated().toString(),
                    "\n");
            //считаем общуюю сумму для оплаты
            BigDecimal roundingAmount = BigDecimal.valueOf(configService.getActualConfig().getRoundingAmount());


            // для проверки расчетов в режиме отладки, брек поинт ставить на нужном пациенте
            String surname;
            if (pacient.getSurname().equals(new String("Пивченко"))) {
                surname = pacient.getSurname();
            }
            ///////////////////////////////////////////////////////////

            BigDecimal amount = getPayment(pacient, monthToCalculate, roundingAmount, detailCalculation);
            //получаем общуюю сумму "оплаченных сумм"
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(monthToCalculate);
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DATE, 1);
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            Date dateForCalculatePaidSum = calendar.getTime();
            BigDecimal totalPaidSum = paidSumRepository.getTotalPaidSumByPacient(pacient, dateForCalculatePaidSum);
            //вычитаем от общей суммы для оплаты, оплаченные суммы.
            amount = amount.subtract(totalPaidSum);

            //вытягиваем список сумм по реестрам для конкретного пациента, используется для вывода в детализацию расчета
            addMessageForDetailCalculation(detailCalculation,
                    "Суммы по реестрам :",
                    "\n");
            Map<BankStatment, BigDecimal> bankStatmentsWithPaidSumtByPacient =
                    paidSumRepository.getBankStatmentsWithPaidSumtByPacient(pacient);
            //выводим №выписки, дату и сумму по этой выписки для пациента
            for (BankStatment key: bankStatmentsWithPaidSumtByPacient.keySet()) {
                addMessageForDetailCalculation(detailCalculation,
                        "№ ",
                        key.getNumber().toString(),
                        " от ",
                        dateFormatShort.format(key.getDate()),
                        " сумма: ",
                        bankStatmentsWithPaidSumtByPacient.get(key).toString(),
                        " руб.",
                        "\n"
                );
            }
            //выводим итог расчетов
            addMessageForDetailCalculation(detailCalculation,
                    "Итого: ",
                    amount.toString(),
                    " руб.");

            AmountForPayment amountForPayment = amountForPaymentRepository.getByPacientAndDate(pacient, monthToCalculate);
            if (amountForPayment == null) {
                amountForPayment = new AmountForPayment();
            }
            amount = ServiceUtils.round(amount, roundingAmount);
            amountForPayment.setAmount(amount);
            amountForPayment.setMonth(monthToCalculate);
            amountForPayment.setPacient(pacient);
            amountForPayment.setDetailCalculation(detailCalculation.toString());
            amountForPaymentRepository.save(amountForPayment);
        }
    }

    //расчет суммы за конкретный период
    private BigDecimal getPaymentByPeriod(BigDecimal pensionForCurMonth, BigDecimal dayForCurMonth,
                                          BigDecimal pensionForNextMonth, BigDecimal dayForNextMonth,
                                          StringBuilder detailCalculation) {
        // на вход принимает размер пенсии в текущем месяце, кол дней в текущем месяце
        //размер пенсии в след месяце, кол дней в след месяце, значение КД
        final BigDecimal kPension = BigDecimal.valueOf(0.8);
        final BigDecimal averageDayInMonth = BigDecimal.valueOf(30.4);
        //сделать что бы функция принимала к.пенсии для каждого месяц, т.к. он может изменятся, добавить
        //в программу таблицу констант для каждого месяца где будет хранится данный коэффициент по месяцам

        ///детализация расчетов
        addMessageForDetailCalculation(detailCalculation,
                "**** (",
                pensionForNextMonth.toString(),
                " * 0.8) / 30.4 * ",
                dayForNextMonth.toString(),
                " + (",
                pensionForCurMonth.toString(),
                " * 0.8) / 30.4 * ",
                "(30.4 - ",
                dayForCurMonth.toString(),
                ") = ");
        /////////////////////////
        //делаем размер пенсии 80%
        pensionForNextMonth = pensionForNextMonth.multiply(kPension);
        pensionForCurMonth = pensionForCurMonth.multiply(kPension);
        //ПС / 30,4 * ДС +
        // П /30.4 * (30.4 - Д)

        BigDecimal paymentNextMonth = (pensionForNextMonth)
                .divide(averageDayInMonth, 10, BigDecimal.ROUND_HALF_UP)
                .multiply(dayForNextMonth);

        BigDecimal paymentCurMonth = (pensionForCurMonth)
                .divide(averageDayInMonth, 10, BigDecimal.ROUND_HALF_UP)
                .multiply(averageDayInMonth.subtract(dayForCurMonth).abs());

        BigDecimal totalPayment = paymentCurMonth.add(paymentNextMonth);
        addMessageForDetailCalculation(detailCalculation,
                totalPayment.toString(),
                " ****",
                "\n");
        ///
        return totalPayment;
    }


    private void addMessageForDetailCalculation(StringBuilder detailCalculation, String ... args ) {
        for (String item: args) {
            detailCalculation.append(item);
        }
    }


    private BigDecimal getPayment(Pacient pacient, Date monthToCalculate, BigDecimal roundingAmount,
                                  StringBuilder detailCalculation) {
        SimpleDateFormat dateFormatShort = new SimpleDateFormat("dd.MM.yyyy");
        final BigDecimal kPension = BigDecimal.valueOf(0.8);
        BigDecimal paymentForPeriod = new BigDecimal(0);
        BigDecimal totalPayment = new BigDecimal(0);
        BigDecimal pensionCurMonth = new BigDecimal(0);
        BigDecimal pensionNextMonth = new BigDecimal(0);
        Calendar temp = Calendar.getInstance();
        Calendar dateCalculation = Calendar.getInstance();
        Calendar dateOfStartPeriod = Calendar.getInstance();
        Calendar dateOfEndPeriod = Calendar.getInstance();
        Calendar dateOfEndDogovor = Calendar.getInstance();
        Calendar dateOfEndCalculation = Calendar.getInstance();
        dateCalculation.setTime(monthToCalculate);
        Contract contract = contractRepository.getByPacient(pacient);
        dateOfStartPeriod.setTime(periodRepository.getFirstStartPeriod(contract));
        dateOfEndDogovor.setTime(periodRepository.getLastEndPeriod(contract));
        dateOfEndCalculation = DateUtils.getDateOfEndCalculation(
                dateOfStartPeriod, dateOfEndDogovor, dateCalculation);
        //если месяц не полный, то период будет не величиной в месяц,
        //такой случай происходит, когда период попадает на месяц расчета, либо дата начала равна дате окончания периода
        dateOfEndPeriod = DateUtils.getEndPeriodByStartPeriod(dateOfStartPeriod);
        if (dateOfStartPeriod.before(dateOfEndCalculation)  //условие меньше либо равно
                || dateFormatShort.format(dateOfStartPeriod.getTime())
                .equals(dateFormatShort.format(dateOfEndCalculation.getTime()))) {
            if (dateOfEndPeriod.after(dateOfEndCalculation) //условие больше либо равно
                    || dateFormatShort.format(dateOfEndPeriod.getTime())
                    .equals(dateFormatShort.format(dateOfEndCalculation.getTime()))) {
                dateOfEndPeriod.setTime(dateOfEndCalculation.getTime());
            } else {
                dateOfEndPeriod = DateUtils.getEndPeriodByStartPeriod(dateOfStartPeriod);
            }
        }
        //пока дата начала периода не превышает даты расчета  (месяца расчета),
        // либо не равна(случай когда в периоде один день (дата начала и конца периода совпадают)
        // производим расчет по периодам
        while (dateOfStartPeriod.before(dateOfEndCalculation) 
                || dateFormatShort.format(dateOfStartPeriod.getTime())
                        .equals(dateFormatShort.format(dateOfEndCalculation.getTime()))) {
            pensionCurMonth = pensionRepository.getByPacientAndDate(pacient, dateOfStartPeriod.getTime());
            pensionNextMonth = pensionRepository.getByPacientAndDate(pacient, dateOfEndPeriod.getTime());
            int firstDayOfMonth = dateOfStartPeriod.getActualMinimum(Calendar.DAY_OF_MONTH);
            int lastDayOfMonth = dateOfStartPeriod.getActualMaximum(Calendar.DAY_OF_MONTH);
            //если период выпадает на полный месяц с 1 по последнее число месяца,
            // то берем 80% от пенсии за тек. месяц,
            //      если период попадает на один месяц и он не полный, то
            //      расчет делаем по формуле (пенсия*80%)/30.4*кол-во не полных дней в месяце
            //      иначе расчет выполняем по формуле
            if ( dateOfStartPeriod.get(Calendar.MONTH) == dateOfEndPeriod.get(Calendar.MONTH) &&
                    dateOfStartPeriod.get(Calendar.DATE) == firstDayOfMonth &&
                    dateOfEndPeriod.get(Calendar.DATE) == lastDayOfMonth) { //если месяц полный с 1 по послед число
                //получаем за период округленное значение 80% размера пенсии за месяц расчетного периода
                paymentForPeriod = pensionCurMonth.multiply(kPension);
                //детализация
                addMessageForDetailCalculation(detailCalculation,
                        "**** ",
                        pensionCurMonth.toString(),
                        " * 0.8 = ",
                        paymentForPeriod.toString(),
                        " ****",
                        "\n");
                //
            } else {
                if (dateOfStartPeriod.get(Calendar.MONTH) == dateOfEndPeriod.get(Calendar.MONTH)) {
                    //если период не полный и попадает в один месяц
                    //long daysInCurMonth = dateOfStartPeriod.get(Calendar.DATE);
                    //long daysInNextMonth = dateOfEndPeriod.get(Calendar.DATE);
                    long daysForCalculation = DateUtils.daysBetween(dateOfStartPeriod, dateOfEndPeriod);
                    paymentForPeriod =
                            getPaymentByPeriod(BigDecimal.valueOf(0),
                                    BigDecimal.valueOf(0),
                                    pensionCurMonth,
                                    BigDecimal.valueOf(daysForCalculation),
                                    detailCalculation
                            );
                } else {
                    //определяем кол-во дней пребывания в тек месяце
                    long daysInCurMonth = 0;
                    long maxDayInNextMonth = dateOfEndPeriod.getActualMaximum(Calendar.DAY_OF_MONTH);
                    //если конец периода у нас попадает на конец месяца,
                    // то кол-во дней в тек месяце равно дата окончания периода (пример 30.01.13 по 28.02.13)
                    // иначе кол-во дней в тек месяце равно дата начала период - 1 день (пример 25.01.13 по 10.02.13)
                    if (maxDayInNextMonth == dateOfEndPeriod.get(Calendar.DATE)) {
                        daysInCurMonth = dateOfEndPeriod.get(Calendar.DATE);
                    } else {
                        daysInCurMonth = dateOfStartPeriod.get(Calendar.DATE)-1;
                    }

                    //определяем кол-во дней пребывания в след месяце
                    temp.setTime(dateOfEndPeriod.getTime());
                    temp.set(Calendar.DATE, dateOfEndPeriod.getActualMinimum(Calendar.DAY_OF_MONTH));
                    long daysInNextMonth = DateUtils.daysBetween(temp, dateOfEndPeriod);

                    paymentForPeriod =
                            getPaymentByPeriod(pensionCurMonth,
                                    BigDecimal.valueOf(daysInCurMonth),
                                    pensionNextMonth,
                                    BigDecimal.valueOf(daysInNextMonth),
                                    detailCalculation
                            );
                }
            }
            //в общую сумму добавляем уже округленные значения расчитанные за период
            totalPayment = totalPayment.add(ServiceUtils.round(paymentForPeriod, roundingAmount));
            addMessageForDetailCalculation(detailCalculation,
                    "период ",
                    dateFormatShort.format(dateOfStartPeriod.getTime()),
                    " - ",
                    dateFormatShort.format(dateOfEndPeriod.getTime()),
                    " : ",
                    ServiceUtils.round(paymentForPeriod, roundingAmount).toString(),
                    " руб.",
                    "\n");

            //переходим к следующему периоду
            dateOfStartPeriod = DateUtils.getStartPeriodByPreviousEndPeriod(dateOfEndPeriod);
            //если месяц не полный, то период будет не величиной в месяц, проверяем это
            //такой случай происходит, когда период попадает на месяц расчета
            dateOfEndPeriod = DateUtils.getEndPeriodByStartPeriod(dateOfStartPeriod);
            if (dateOfStartPeriod.before(dateOfEndCalculation)  //условие меньше либо равно
                    || dateFormatShort.format(dateOfStartPeriod.getTime())
                    .equals(dateFormatShort.format(dateOfEndCalculation.getTime()))) {
                if (dateOfEndPeriod.after(dateOfEndCalculation) //условие больше либо равно
                        || dateFormatShort.format(dateOfEndPeriod.getTime())
                        .equals(dateFormatShort.format(dateOfEndCalculation.getTime()))) {
                    dateOfEndPeriod.setTime(dateOfEndCalculation.getTime());
                } else {
                    dateOfEndPeriod = DateUtils.getEndPeriodByStartPeriod(dateOfStartPeriod);
                }
            }
        }
        return totalPayment;
    }
}



