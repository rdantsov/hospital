package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.controllers.forms.report.DebtRegisterForm;
import org.dantsov.hospital.models.AmountForPayment;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.repository.AmountForPaymentRepository;
import org.dantsov.hospital.repository.PacientRepository;
import org.dantsov.hospital.repository.hibernate.AmountForPaymentRepositoryHibernate;
import org.dantsov.hospital.service.ConfigService;
import org.dantsov.hospital.service.PacientService;
import org.dantsov.hospital.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 25/07/13
 * Time: 13:01
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ReportServiceImpl implements ReportService {
    @Autowired
    private PacientRepository pacientRepository;
    @Autowired
    private AmountForPaymentRepository amountForPaymentRepository;
    @Autowired
    private PacientService pacientService;
    @Autowired
    private ConfigService configSrvice;

    @Override
    @Transactional
    public List<DebtRegisterForm> getDebtRegisterFormsByDate(Date startPeriod, Date endPeriod, Pacient pacientForReport) {
        List<Pacient> pacients = new LinkedList<Pacient>();
        if (pacientForReport == null) {
             pacients = pacientService.getPacientListForDebtRegistrReport(startPeriod, endPeriod);
        } else {
            pacients.add(pacientForReport);
        }

        List<DebtRegisterForm> debtRegisterForms = new LinkedList<DebtRegisterForm>();
        Date currentDate = configSrvice.getCurrentDate();
        for (Pacient pacient : pacients) {
            //парсим детализацию расчетов и вытягиваем от туда оплату по периодам, суммы по реестрам, итого
            StringBuilder periodOfStayAndAmount = new StringBuilder("");
            StringBuilder paidSum = new StringBuilder("");
            StringBuilder totalAmount = new StringBuilder("");

            AmountForPayment amountForPayment = amountForPaymentRepository.getLastByPacient(pacient);
            DebtRegisterForm debtRegisterForm = new DebtRegisterForm();
            if (amountForPayment != null) {
                String amountForPaymentString = amountForPayment.getDetailCalculation();
                String detailCalculationStrings[] = amountForPaymentString.split("\\n");
                ///////
                for (String str : detailCalculationStrings) {
                    if (str.indexOf("период") != -1) {
                        periodOfStayAndAmount.append(str.substring(7, str.length()).replace(':', '='));
                        periodOfStayAndAmount.append("\n");
                    }
                    if ( (str.indexOf("№ ") != -1) && (str.indexOf(".2012") == -1) ) {
                        paidSum.append(str);
                        paidSum.append("\n");
                    }
                    if (str.indexOf("Итого") != -1) {
                        totalAmount.append(str.substring(6, str.length()));
                    }
                }
            }

                debtRegisterForm.setPeriodOfStayAndAmount(periodOfStayAndAmount.toString());

                debtRegisterForm.setDateAndPaidSum(paidSum.toString());

                debtRegisterForm.setTotalAmount(totalAmount.toString());


            debtRegisterForm.setFullName(pacient.getSurname() + " "
                    + pacient.getName() + " "
                    + pacient.getMidleName());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
            debtRegisterForm.setContract("№" + pacient.getContract().getNomer()
                    + " от " + dateFormat.format(pacient.getContract().getDateCreated()));

            debtRegisterForms.add(debtRegisterForm);

        }
        return debtRegisterForms;
    }
}
