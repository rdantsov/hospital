package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.controllers.forms.report.PacientForm;
import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.ContractStatus;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Period;
import org.dantsov.hospital.repository.ContractRepository;
import org.dantsov.hospital.repository.PacientRepository;
import org.dantsov.hospital.repository.PeriodRepository;
import org.dantsov.hospital.service.ConfigService;
import org.dantsov.hospital.service.PacientService;
import org.dantsov.hospital.utils.DateUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: rus
 * Date: 07.02.13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */
@Component
public class PacientServiceImpl implements PacientService {
    @Autowired
    private PacientRepository pacientRepository;
    @Autowired
    private ContractRepository contractRepository;
    @Autowired
    private PeriodRepository periodRepository;
    @Autowired
    private ConfigService configService;

    @Override
    @Transactional
    public void save(Pacient pacient, Contract contract, Period period) {
        contractRepository.saveOrUpdate(contract);
        pacientRepository.save(pacient);
        periodRepository.save(period);
    }

    @Override
    @Transactional
    public Map<String, String> getMapPacientsWithContracrtStatus(List<Pacient> pacients) {
        Map<String, String> pacientWithContractStatus = new HashMap<String, String>();
        String status = null;
        Date currentDate = configService.getCurrentDate();
        Calendar calendarCurrentDate = Calendar.getInstance();
        calendarCurrentDate.setTime(currentDate);
        Calendar calendarEndPeriod = Calendar.getInstance();
        for (Pacient pacient: pacients) {
            status = ContractStatus.ACTIVE.toString();
            calendarEndPeriod.setTime(periodRepository.getLastEndPeriod(pacient.getContract()));
           //проверка когда и закрыт и истекала дата периода
            if (pacient.getContract().getCompleted() == 1) {
                status = ContractStatus.CLOSE.toString();
            } else if (calendarEndPeriod.before(calendarCurrentDate)) {
                status = ContractStatus.COMPLETED.toString();
            }
            pacientWithContractStatus.put(pacient.getId().toString(), status);
        }
        return pacientWithContractStatus;
    }

    @Override
    @Transactional
    public List<Pacient> getAllByPage(int numberOfPage, int countOnPage, String sort) {
        return pacientRepository.getAllByPage(numberOfPage, countOnPage, sort);
    }

    @Override
    @Transactional
    public List<Pacient> getAllByPageAndFilter(int numberOfPage, int countOnPage,
                                               String searchText, boolean isPacientContractClose, String sort) {
        return pacientRepository.getAllByPageAndFilter(numberOfPage, countOnPage, searchText,
                isPacientContractClose, sort);
    }

    @Override
    @Transactional
    public List<Pacient> getAllBySearchText(String searchText) {
        return pacientRepository.getAllBySearchText(searchText);
    }

    @Override
    @Transactional
    public Pacient getById(Long id) {
        return pacientRepository.getById(id);
    }

    @Override
    @Transactional
    public List<Pacient> getAll() {
        return pacientRepository.getAllWithoutClose();
    }

    @Override
    @Transactional
    public Pacient getByContract(Contract contract) {
        return pacientRepository.getByContract(contract);
    }

    @Override
    @Transactional
    public org.dantsov.hospital.controllers.forms.pacient.PacientForm getPacientFormByPacient(Pacient pacient) {
        ModelMapper modelMapper = new ModelMapper();
        ModelMapper mp = new ModelMapper();
        org.dantsov.hospital.controllers.forms.pacient.PacientForm pacientForm = new org.dantsov.hospital.controllers.forms.pacient.PacientForm();
        Contract contract = contractRepository.getByPacient(pacient);
        Period period = periodRepository.getFirstPeriodByContract(contract);
        pacientForm = modelMapper.map(pacient, org.dantsov.hospital.controllers.forms.pacient.PacientForm.class);
        modelMapper.map(contract, pacientForm);
        modelMapper.map(period, pacientForm);

        return pacientForm;
    }

    @Override
    @Transactional
    public List<PacientForm> getPacientListForReport(Date dateCreation) {
        Calendar calendarEndContract = Calendar.getInstance();
        Calendar calendarStartContract = Calendar.getInstance();
        Calendar calendarDateCreation = Calendar.getInstance();
        calendarDateCreation.setTime(dateCreation);
        List<PacientForm> pacientForms = new LinkedList<PacientForm>();
        List<Pacient> pacients = pacientRepository.getAll();
        SimpleDateFormat shortFormat = new SimpleDateFormat("dd.MM.yy");
        for (Pacient pacient: pacients) {
            Date endContract = periodRepository.getLastEndPeriod(pacient.getContract());
            Date startContract = periodRepository.getFirstStartPeriod(pacient.getContract());
            calendarStartContract.setTime(startContract);
            calendarEndContract.setTime(endContract);
            //делаем проверку что бы дата нач договора <= дата отчета <= дата окон дог
            if ( ( calendarStartContract.before(calendarDateCreation)
                    || shortFormat.format(calendarStartContract.getTime()).equals(shortFormat.format(calendarDateCreation.getTime()))
                 ) &&
                 (calendarEndContract.after(calendarDateCreation)
                    || shortFormat.format(calendarEndContract.getTime()).equals(shortFormat.format(calendarDateCreation.getTime()))
                 )
               ) {
                PacientForm pacientForm = new PacientForm();
                pacientForm.setFullName(pacient.getSurname()
                        + " " + pacient.getName()
                        + " " + pacient.getMidleName());

                pacientForm.setContract("№"
                        + pacient.getContract().getNomer().toString()
                        + " от "
                        + shortFormat.format(pacient.getContract().getDateCreated()));

                pacientForm.setPeriodOfStay(shortFormat.format(startContract)
                        + " - "
                        + shortFormat.format(endContract));
                pacientForms.add(pacientForm);
            }
        }
        return pacientForms;
    }


    @Override
    @Transactional
    public List<Pacient> getPacientListForDebtRegistrReport(Date startPeriod, Date endPeriod) {
        List<Pacient> pacientsForReport = new LinkedList<Pacient>();
        List<Pacient> pacients = pacientRepository.getAll();
        for (Pacient pacient: pacients) {
            Date endContract = periodRepository.getLastEndPeriod(pacient.getContract());
            Date startContract = periodRepository.getFirstStartPeriod(pacient.getContract());
            if (DateUtils.isCrossingPeriodsOfDates(startContract, endContract, startPeriod, endPeriod)) {
                pacientsForReport.add(pacient);
            }

        }
        return pacientsForReport;
    }


}
