package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Pension;
import org.dantsov.hospital.repository.PensionRepository;
import org.dantsov.hospital.service.PensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Component
public class PensionServiceImpl implements PensionService {
    @Autowired
    private PensionRepository pensionRepository;

    @Override
    @Transactional
    public List<Pension> getPensionsByPacientAndYear(Pacient pacient, int year) {
        List<Pension> pensions = pensionRepository.getPensionsByPacientAndYear(pacient, year);
        if (pensions.size() < 1) {
            //формируем список из названий месяцев (дат) и сумм пенсий
            Pension pension;
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            for (int i=0; i<12; i++) {
                pension = new Pension();
                calendar.set(Calendar.MONTH, i);
                pension.setAmount(BigDecimal.valueOf(0));
                pension.setMonth(calendar.getTime());
                pensions.add(pension);
            }
        }
        return pensions;
    }

    @Override
    @Transactional
    public void  edit(Pacient pacient, Integer year, Map<Integer, BigDecimal> amountPensions) {
        List<Pension> pensions = pensionRepository.getPensionsByPacientAndYear(pacient, year);

        if (pensions.size() != 0) {  //если записи о пенсиях за указанный год уже имеются
            Calendar calendar = Calendar.getInstance();
            BigDecimal amount;
            for (Pension pension: pensions) {
                calendar.setTime(pension.getMonth());
                amount = amountPensions.get(calendar.get(Calendar.MONTH));
                pension.setAmount(amount);
                pensionRepository.saveOrUpdate(pension);
            }

        } else { //если записей о пенсиях еще не было за указанный год
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            for (int month = 0; month < 12; month++) {
                Pension pension = new Pension();
                pension.setAmount(amountPensions.get(month));
                pension.setPacient(pacient);
                calendar.set(Calendar.MONTH, month);
                pension.setMonth(calendar.getTime());
                pensionRepository.saveOrUpdate(pension);
            }
        }


    }
}
