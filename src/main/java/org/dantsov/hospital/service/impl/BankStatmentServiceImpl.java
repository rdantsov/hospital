package org.dantsov.hospital.service.impl;

import org.dantsov.hospital.models.BankStatment;
import org.dantsov.hospital.repository.BankStatmentRepository;
import org.dantsov.hospital.service.BankStatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 09.04.13
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
@Component
public class BankStatmentServiceImpl implements BankStatmentService {
    @Autowired
    private BankStatmentRepository bankStatmentRepository;

    @Override
    @Transactional
    public List<BankStatment> getByDate(Date date) {
        return bankStatmentRepository.getByDate(date);
    }

    @Override
    @Transactional
    public BankStatment getById(Long id){
        return bankStatmentRepository.getById(id);
    }

    @Override
    @Transactional
    public BankStatment getByDateAndNumber(Date date, Integer number) {
        return bankStatmentRepository.getByDateAndNumber(date, number);
    }

    @Override
    @Transactional
    public Map<String, Boolean> getBankStatmentsIsHaveContractClose(List<BankStatment> bankStatments) {
        List<Object[]> bankStatmentsWithCountContractClose =
                bankStatmentRepository.getBankStatmentsIsHaveContractClose(bankStatments);

        Map<String, Boolean> bankStatmentsIsHaveContractClose = new HashMap<String, Boolean>();
        for (BankStatment bankStatment: bankStatments) {
            bankStatmentsIsHaveContractClose.put(bankStatment.getId().toString(), false);
        }

        String id;
        Boolean isHaveContractClose = null;
        for (Object[] object: bankStatmentsWithCountContractClose) {
            id = String.valueOf( (Long) object[0]);
            if ( ( (Long) object[1]) > 0 ) {
                isHaveContractClose = true;
            } else {
                isHaveContractClose = false;
            }
            bankStatmentsIsHaveContractClose.put(id, isHaveContractClose);
        }
    return bankStatmentsIsHaveContractClose;
    }
}
