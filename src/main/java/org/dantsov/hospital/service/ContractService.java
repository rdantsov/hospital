package org.dantsov.hospital.service;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 26.03.13
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */
public interface ContractService {
    @Transactional
    Contract getById(Long id);

    @Transactional
    Contract getByPacient(Pacient pacient);

    @Transactional
    void saveOrUpdate(Contract contract);

    @Transactional
    Integer getMaxNumberOfContractByYear(Date date);
}
