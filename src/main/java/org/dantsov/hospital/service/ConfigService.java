package org.dantsov.hospital.service;

import org.dantsov.hospital.models.Config;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 30.04.13
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
public interface ConfigService {
    @Transactional
    Date getCurrentDate();

    @Transactional
    Config getActualConfig();

    @Transactional
    void update(Config newConfig);

    @Transactional
    void changeMonth();
}
