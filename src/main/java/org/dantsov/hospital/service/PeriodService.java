package org.dantsov.hospital.service;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Period;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 27.03.13
 * Time: 12:16
 * To change this template use File | Settings | File Templates.
 */
public interface PeriodService {
    List<Period> getByContract(Contract contract);

    @Transactional
    Date getLastEndPeriod(Contract contract);

    @Transactional
    void save(Period period);

    @Transactional
    Period getFirstPeriodByContract(Contract contract);

    @Transactional
    void delete(Long id);

    @Transactional
    Period getById(Long id);
}
