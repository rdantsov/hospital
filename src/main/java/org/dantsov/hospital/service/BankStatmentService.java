package org.dantsov.hospital.service;

import org.dantsov.hospital.models.BankStatment;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 09.04.13
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
public interface BankStatmentService {
    @Transactional
    List<BankStatment> getByDate(Date date);

    @Transactional
    BankStatment getById(Long id);

    @Transactional
    BankStatment getByDateAndNumber(Date date, Integer number);

    @Transactional
    Map<String, Boolean> getBankStatmentsIsHaveContractClose(List<BankStatment> bankStatments);
}
