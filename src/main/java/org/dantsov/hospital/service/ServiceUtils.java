package org.dantsov.hospital.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 07.05.13
 * Time: 11:34
 * To change this template use File | Settings | File Templates.
 */
public abstract class ServiceUtils {

    public static BigDecimal round(BigDecimal value, BigDecimal roundTo){
        if ((value != null) && (roundTo != null)
                && (roundTo.compareTo(BigDecimal.ZERO) == 1)) {
            return value.divide(roundTo, 0, RoundingMode.HALF_UP).multiply(roundTo);
        } else {
            return value;
        }
    }
}
