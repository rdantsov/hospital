package org.dantsov.hospital.service;

import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Pension;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface PensionService {
    void  edit(Pacient pacient, Integer year, Map<Integer, BigDecimal> amountPensions);

    List<Pension> getPensionsByPacientAndYear(Pacient pacient, int year);
}
