package org.dantsov.hospital.service;

import org.dantsov.hospital.controllers.forms.report.DebtRegisterForm;
import org.dantsov.hospital.models.Pacient;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 25/07/13
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
public interface ReportService {
    List<DebtRegisterForm> getDebtRegisterFormsByDate(Date startPeriod, Date endPeriod, Pacient pacientForReport);
}
