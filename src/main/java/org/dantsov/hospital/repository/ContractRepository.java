package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 26.03.13
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
public interface ContractRepository {
    Contract getById(Long id);

    Contract getByPacient(Pacient pacient);

    void saveOrUpdate(Contract contract);

    Integer getMaxNumberOfContractByYear(Date date);
}
