package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.BankStatment;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 09.04.13
 * Time: 11:26
 * To change this template use File | Settings | File Templates.
 */
public interface BankStatmentRepository {
    void save(BankStatment bankStatment);

    List<BankStatment> getByDate(Date date);

    BankStatment getById(Long id);

    void delete(BankStatment bankStatment);

    BankStatment getByDateAndNumber(Date date, Integer number);

    List<Object[]> getBankStatmentsIsHaveContractClose(
            List<BankStatment> bankStatments);
}
