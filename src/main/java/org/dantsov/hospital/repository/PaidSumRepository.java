package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.BankStatment;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.PaidSum;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 08.04.13
 * Time: 11:41
 * To change this template use File | Settings | File Templates.
 */
public interface PaidSumRepository {
    void save(PaidSum paidSum);

    BigDecimal getTotalPaidSumByPacient(Pacient pacient, Date monthToCalculate);

    @SuppressWarnings("unchecked")
    List<PaidSum> getByBankStatment(BankStatment bankStatment);

    void delete(PaidSum paidSum);

    PaidSum getByPacientIdAndBankStatmentId(Long pacientId, Long bankStatmentId);

    PaidSum getById(Long id);

    @SuppressWarnings("unchecked")
    Map<BankStatment, BigDecimal> getBankStatmentsWithPaidSumtByPacient(
            Pacient pacient);

    BigDecimal getPaidSumForPeriod(Date startDate, Date endDate);

    BigDecimal getPaidSumByCreditCardForPeriod(Date startDate, Date endDate);
}
