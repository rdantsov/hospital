package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.Config;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 30.04.13
 * Time: 16:19
 * To change this template use File | Settings | File Templates.
 */
public interface ConfigRepository {
    Date getCurrentDate();

    Config getActualConfig();

    void update(Config config);
}
