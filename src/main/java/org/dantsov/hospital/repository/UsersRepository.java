package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.Users;

/**
 * autor Dantsov Ruslan
 */

public interface UsersRepository {
    Users getUserByName(String name);
    void saveOrUpdate(Users user);
}
