package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Pension;
import org.dantsov.hospital.repository.PensionRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * autor Dantsov Ruslan
 */

@Repository
public class PensionRepositoryHibernate extends RepositoryHibernate
        implements PensionRepository {

    @Override
    @SuppressWarnings("unchecked")
    public List<Pension> getPensionsByPacientAndYear(Pacient pacient, int year) {
        Query query = session().createQuery(
                "FROM Pension as p " +
                "WHERE year(p.month) = :year " +
                "AND p.pacient = :pacient  " +
                "ORDER BY p.month")
                .setParameter("year", year)
                .setParameter("pacient", pacient);
        return query.list();
    }

    @Override
    public BigDecimal getByPacientAndDate(Pacient pacient, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        Query query = session().createQuery(
                "FROM Pension as p " +
                        "WHERE p.pacient = :pacient " +
                        "AND YEAR(p.month) = :year " +
                        "AND MONTH(p.month) = :month")
                .setParameter("pacient", pacient)
                .setParameter("year", year)
                .setParameter("month", month);
        Pension pension = (Pension) query.uniqueResult();
        if (pension != null) {
            BigDecimal amount = pension.getAmount();
            return amount;
        } else return BigDecimal.valueOf(0);
    }

   /* @Override
    public BigDecimal getByPacientAndDate(Pacient pacient, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        Query query = session().createQuery(
                "FROM Pension as p " +
                        "WHERE p.pacient = :pacient " +
                        "AND YEAR(p.month) = :year " +
                        "AND MONTH(p.month) = :month")
                .setParameter("pacient", pacient)
                .setParameter("year", year)
                .setParameter("month", month);
        Pension pension = (Pension) query.uniqueResult();
        if (pension != null) {
            BigDecimal amount = pension.getAmount();
            return amount;
        } else return BigDecimal.valueOf(0);
    }   */

    @Override
    @SuppressWarnings("uncheked")
    public List<Pension> getByDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        Query query = session().createQuery(
                "FROM Pension as p " +
                "WHERE YEAR(p.month) = :year " +
                "AND MONTH(p.month) = :month")
                .setParameter("year", year)
                .setParameter("month", month);
        return query.list();
    }

    @Override
    public void saveOrUpdate(Pension pension) {
        session().saveOrUpdate(pension);
    }
}
