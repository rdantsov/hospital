package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class RepositoryHibernate {
    @Autowired
    private SessionFactory sessionFactory;

    public final Session session() {
        return this.sessionFactory.getCurrentSession();
    }
}
