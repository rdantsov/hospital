package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.BankStatment;
import org.dantsov.hospital.repository.BankStatmentRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 09.04.13
 * Time: 11:26
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class BankStatmentRepositoryHibernate extends RepositoryHibernate
                implements BankStatmentRepository {

    @Override
    public void save(BankStatment bankStatment) {
        session().saveOrUpdate(bankStatment);
    }

    @Override
    public void delete(BankStatment bankStatment) {
        session().delete(bankStatment);
    }

    @Override
    public BankStatment getById(Long id) {
        Query query = session().createQuery(
                "FROM BankStatment as bs " +
                "WHERE bs.id = :id")
                .setParameter("id", id);
        return (BankStatment) query.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<BankStatment> getByDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        Query query = session().createQuery(
                "FROM BankStatment as bs " +
                "WHERE YEAR(bs.date) = :year " +
                "AND MONTH(bs.date) = :month " +
                "ORDER BY bs.date")
                .setParameter("year", year)
                .setParameter("month", month);
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> getBankStatmentsIsHaveContractClose(
            List<BankStatment> bankStatments) {
        if (bankStatments != null && bankStatments.size()>0) {
            Query query = session().createQuery(
                    "SELECT bs.id, COUNT(p) " +
                    "FROM PaidSum as ps " +
                    "INNER JOIN ps.bankStatment as bs " +
                    "INNER JOIN ps.pacient as p " +
                    "WHERE EXISTS (FROM Contract as c WHERE c.id = p.contract and c.completed = 1) and bs IN (:bankStatments) " +
                    "GROUP BY bs.id")
                    .setParameterList("bankStatments", bankStatments);
            return query.list();
        } else return new ArrayList<Object[]>();
    }

    @Override
    public BankStatment getByDateAndNumber(Date date, Integer number) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        Query query = session().createQuery(
                "FROM BankStatment as bs " +
                "WHERE YEAR(bs.date) = :year " +
                "AND MONTH(bs.date) = :month " +
                "AND bs.number = :number")
                .setParameter("year", year)
                .setParameter("month", month)
                .setParameter("number", number);
        return (BankStatment) query.uniqueResult();
    }



}
