package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.BankStatment;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.PaidSum;
import org.dantsov.hospital.repository.PaidSumRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 08.04.13
 * Time: 11:42
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class PaidSumRepositoryHibernate extends RepositoryHibernate
        implements PaidSumRepository {

    @Override
    public void save(PaidSum paidSum) {
        session().saveOrUpdate(paidSum);
    }

    @Override
    public void delete(PaidSum paidSum) {
        session().delete(paidSum);
    }

    @Override
    public PaidSum getById(Long id) {
        Query query = session().createQuery(
                "FROM PaidSum as p " +
                "WHERE p.id = :id")
                .setParameter("id", id);
        return (PaidSum) query.uniqueResult();
    }

    @Override
      @SuppressWarnings("unchecked")
      public List<PaidSum> getByBankStatment(BankStatment bankStatment) {
        Query query = session().createQuery(
                "FROM PaidSum as ps " +
                "INNER JOIN FETCH ps.pacient as p " +
                "INNER JOIN FETCH p.contract " +
                "WHERE ps.bankStatment = :bankStatment " +
                "ORDER BY p.contract.nomer")
                .setParameter("bankStatment", bankStatment);
        return query.list();
    }

    @Override
    public BigDecimal getTotalPaidSumByPacient(Pacient pacient, Date monthToCalculate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(monthToCalculate);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Query query = session().createQuery(
                "SELECT SUM(p.amount) " +
                "FROM PaidSum as p " +
                "WHERE p.pacient = :pacient " +
                "AND p.bankStatment.date <= :monthToCalculate")
                .setParameter("pacient", pacient)
                .setParameter("monthToCalculate", calendar.getTime());
        BigDecimal totalAmount = (BigDecimal) query.uniqueResult();
        if (totalAmount != null) {
            return totalAmount;
        } else {
            return new BigDecimal(0);
        }
    }

    @Override
    public PaidSum getByPacientIdAndBankStatmentId(Long pacientId, Long bankStatmentId) {
        Query query = session().createQuery(
               "FROM PaidSum as p " +
               "WHERE p.pacient.id = :pacientId " +
               "AND p.bankStatment.id = :bankStatmentId")
               .setParameter("pacientId", pacientId)
               .setParameter("bankStatmentId", bankStatmentId);

            return (PaidSum) query.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<BankStatment, BigDecimal> getBankStatmentsWithPaidSumtByPacient(
            Pacient pacient) {
        Query query = session().createQuery(
                "SELECT bs, sum(ps.amount) " +
                "FROM PaidSum as ps " +
                "INNER JOIN ps.bankStatment as bs " +
                "WHERE ps.pacient = :pacient " +
                "GROUP BY bs " +
                "ORDER BY bs.date")
                .setParameter("pacient", pacient);
        List<Object[]> objects = query.list();
        Map<BankStatment, BigDecimal> bankStatmentsWithPaidSumtByPacient = new LinkedHashMap<BankStatment, BigDecimal>();
        for(Object[] object: objects) {
            bankStatmentsWithPaidSumtByPacient.put((BankStatment) object[0], (BigDecimal) object[1]);
        }
        return bankStatmentsWithPaidSumtByPacient;
    }

    @Override
    public BigDecimal getPaidSumForPeriod(Date startDate, Date endDate) {
        Query query = session().createQuery(
                "SELECT SUM(p.amount) " +
                "FROM PaidSum as p " +
                "WHERE p.bankStatment.date >= :startDate " +
                "AND p.bankStatment.date <= :endDate")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate);
        BigDecimal totalAmount = (BigDecimal) query.uniqueResult();
        if (totalAmount != null) {
            return totalAmount;
        } else {
            return new BigDecimal(0);
        }
    }

    @Override
    public BigDecimal getPaidSumByCreditCardForPeriod(Date startDate, Date endDate) {
        Query query = session().createQuery(
                "SELECT SUM(p.amount) " +
                "FROM PaidSum as p " +
                "WHERE p.paymentCreditCard = :paymentCreditCard " +
                "AND p.bankStatment.date >= :startDate " +
                "AND p.bankStatment.date <= :endDate")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .setParameter("paymentCreditCard", 1);
        BigDecimal totalAmount = (BigDecimal) query.uniqueResult();
        if (totalAmount != null) {
            return totalAmount;
        } else {
            return new BigDecimal(0);
        }
    }

}
