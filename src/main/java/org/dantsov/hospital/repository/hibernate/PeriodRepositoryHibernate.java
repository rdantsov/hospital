package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Period;
import org.dantsov.hospital.repository.PeriodRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 26.03.13
 * Time: 17:55
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class PeriodRepositoryHibernate extends RepositoryHibernate
        implements PeriodRepository {

    @Override
    public void save(Period period) {
        session().saveOrUpdate(period);
    }

    @Override
    public Period getById(Long id) {
        Query query = session().createQuery(
                "FROM Period as p " +
                "WHERE p.id = :id")
                .setParameter("id", id);
        return (Period) query.uniqueResult();
    }

    @Override
    public void delete(Period period) {
        session().delete(period);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Period> getByContract(Contract contract) {
        Query query = session().createQuery(
                "FROM Period as p " +
                "WHERE p.contract = :contract")
                .setParameter("contract", contract);
        return query.list();
    }

    @Override
    public Date getLastEndPeriod(Contract contract) {
        Query query = session().createQuery(
                "SELECT MAX(p.endPeriod) " +
                "FROM Period as p " +
                "WHERE p.contract = :contract ")
                .setParameter("contract", contract);
        return (Date) query.uniqueResult();
    }

    @Override
    public Period getFirstPeriodByContract(Contract contract) {
        Query query = session().createQuery(
                "FROM Period as p " +
                "INNER JOIN FETCH p.contract " +
                "WHERE p.contract = :contract " +
                "ORDER BY p.startPeriod")
                .setParameter("contract", contract);
        query.setMaxResults(1);
        return (Period) query.uniqueResult();
    }

    @Override
    public Date getFirstStartPeriod(Contract contract) {
        Query query = session().createQuery(
                "SELECT MIN(p.startPeriod) " +
                "FROM Period as p " +
                "WHERE p.contract = :contract ")
                .setParameter("contract", contract);
        return (Date) query.uniqueResult();
    }
}
