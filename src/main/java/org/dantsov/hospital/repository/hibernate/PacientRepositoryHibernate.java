package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.repository.PacientRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class PacientRepositoryHibernate extends RepositoryHibernate
        implements PacientRepository {

    @Override
    public void save(Pacient pacient) {
        session().saveOrUpdate(pacient);
    }

    @Override
    public Pacient getById(Long id) {
        Query query = session().createQuery(
                "FROM Pacient as p " +
                "INNER JOIN FETCH p.contract " +
                "WHERE p.id = :id")
                .setParameter("id", id);
        return (Pacient) query.uniqueResult();
    }

    @Override
    public Pacient getByContract(Contract contract) {
        Query query = session().createQuery(
                "FROM Pacient as p " +
                "WHERE p.contract = :contract")
                .setParameter("contract", contract);
        return (Pacient) query.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Pacient> getAllByPage(int numberOfPage, int countOnPage, String sort) {
        int from = (numberOfPage - 1) * countOnPage;
        int count = countOnPage + 1;
        Query query;
        if (sort.equals("fullName")) {
            query = session().createQuery(
                    "FROM Pacient as p " +
                    "INNER JOIN FETCH p.contract " +
                    "WHERE p.contract.completed = :notCompleted " +
                    "ORDER BY p.surname")
                    .setParameter("notCompleted", 0)
                    .setMaxResults(count)
                    .setFirstResult(from);
        } else {
            query = session().createQuery(
                    "FROM Pacient as p " +
                    "INNER JOIN FETCH p.contract " +
                    "WHERE p.contract.completed = :notCompleted " +
                    "ORDER BY p.contract.nomer")
                    .setParameter("notCompleted", 0)
                    .setMaxResults(count)
                    .setFirstResult(from);
        }
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Pacient> getAllByPageAndFilter(int numberOfPage, int countOnPage,
                                               String searchText, boolean isPacientContractClose, String sort) {
        int from = (numberOfPage - 1) * countOnPage;
        int count = countOnPage + 1;
        int contractCompleted;
        if (isPacientContractClose) {
            contractCompleted = 1;
        } else {
            contractCompleted = 0;
        }
        Query query;
        if (sort.equals("fullName")) {
            query = session().createQuery(
                "FROM Pacient as p " +
                "INNER JOIN FETCH p.contract " +
                "WHERE p.contract.completed = :contractCompleted " +
                "AND (UPPER(p.surname) LIKE UPPER(:searchText) " +
                "OR CAST(p.contract.nomer AS string) LIKE :searchText) " +
                "ORDER BY p.surname")
                .setParameter("contractCompleted", contractCompleted)
                .setParameter("searchText", '%' + searchText + '%')
                .setMaxResults(count)
                .setFirstResult(from);
        } else {
            query = session().createQuery(
                    "FROM Pacient as p " +
                    "INNER JOIN FETCH p.contract " +
                    "WHERE p.contract.completed = :contractCompleted " +
                    "AND (UPPER(p.surname) LIKE UPPER(:searchText) " +
                    "OR CAST(p.contract.nomer AS string) LIKE :searchText) " +
                    "ORDER BY p.contract.nomer")
                    .setParameter("contractCompleted", contractCompleted)
                    .setParameter("searchText", '%' + searchText + '%')
                    .setMaxResults(count)
                    .setFirstResult(from);
        }
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Pacient> getAllBySearchText(String searchText) {
        Query query = session().createQuery(
            "FROM Pacient as p " +
            "INNER JOIN FETCH p.contract " +
            "WHERE (UPPER(p.surname) LIKE UPPER(:searchText) " +
            "OR CAST(p.contract.nomer AS string) LIKE :searchText) " +
            "ORDER BY p.surname")
            .setParameter("searchText", '%' + searchText + '%');
        return query.list();
    }

    @Override
      @SuppressWarnings("unchecked")
        public List<Pacient> getAllWithoutClose() {
            Query query = session().createQuery(
                     "FROM Pacient as p " +
                     "INNER JOIN FETCH p.contract " +
                     "WHERE p.contract.completed = :completed " +
                     "ORDER BY p.surname")
                     .setParameter("completed", 0);
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Pacient> getAll() {
        Query query = session().createQuery(
                "FROM Pacient as p " +
                "INNER JOIN FETCH p.contract " +
                "ORDER BY p.surname");
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Pacient> getAllWithContract() {
        Query query = session().createQuery(
                "FROM Pacient as p " +
                "INNER JOIN FETCH p.contract " +
                "WHERE p.contract.completed = :completed " +
                "ORDER BY p.surname")
                .setParameter("completed", 0);
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Pacient> getAllWithContractOrderByContractNumber() {
        Query query = session().createQuery(
                "FROM Pacient as p " +
                "INNER JOIN FETCH p.contract " +
                "WHERE p.contract.completed = :completed " +
                "ORDER BY p.contract.nomer")
                .setParameter("completed", 0);
        return query.list();
    }

}
