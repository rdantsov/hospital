package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.Config;
import org.dantsov.hospital.repository.ConfigRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 30.04.13
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class ConfigRepositoryHibernate extends RepositoryHibernate
        implements ConfigRepository {

    @Override
    public Date getCurrentDate() {
        Query query = session().createQuery(
                        "SELECT c.currentDate " +
                        "FROM Config as c " +
                        "WHERE c.actual = :actual")
                        .setParameter("actual", 1);
        return (Date) query.uniqueResult();
    }

    @Override
    public Config getActualConfig() {
        Query query = session().createQuery(
                "FROM Config as c " +
                "WHERE c.actual = :actual")
                .setParameter("actual", 1);
        return (Config) query.uniqueResult();
    }

    @Override
    public void update(Config config) {
        session().saveOrUpdate(config);
    }

}
