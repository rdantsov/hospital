package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.Users;
import org.dantsov.hospital.repository.UsersRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * autor Dantsov Ruslan
 */

@Repository
public class UsersRepositoryHibernate extends RepositoryHibernate
        implements UsersRepository {

    @Override
    public void saveOrUpdate(Users user) {
        session().saveOrUpdate(user);
    }

    @Override
    public Users getUserByName(String name) {
        Query query = session().createQuery(
                "FROM Users as u " +
                "WHERE u.username = :name")
                .setParameter("name", name);
        return (Users) query.uniqueResult();
    }

}
