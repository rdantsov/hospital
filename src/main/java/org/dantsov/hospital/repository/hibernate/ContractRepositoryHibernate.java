package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.repository.ContractRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 26.03.13
 * Time: 16:49
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ContractRepositoryHibernate extends RepositoryHibernate
        implements ContractRepository {

    @Override
    public Contract getById(Long id) {
        Query query = session().createQuery(
                "FROM Contract as c " +
                "WHERE c.id = :id")
                .setParameter("id", id);
        return (Contract) query.uniqueResult();
    }

    @Override
    public void saveOrUpdate (Contract contract) {
        session().saveOrUpdate(contract);
    }

    @Override
    public Contract getByPacient(Pacient pacient) {
        Query query = session().createQuery(
                "FROM Contract as c " +
                "WHERE c.id = :pacientContract")
                .setParameter("pacientContract", pacient.getContract().getId());
        return (Contract) query.uniqueResult();
    }

    @Override
    public Integer getMaxNumberOfContractByYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        Query query = session().createQuery(
                "SELECT MAX(c.nomer) " +
                "FROM Contract as c " +
                "WHERE YEAR(dateCreated) = :year")
                .setParameter("year", year);

        Integer maxNumber = (Integer) query.uniqueResult();
        if (maxNumber == null) {
            maxNumber = 0;
        }

        return maxNumber;
    }
}
