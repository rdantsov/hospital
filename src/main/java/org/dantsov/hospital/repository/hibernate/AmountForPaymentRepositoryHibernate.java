package org.dantsov.hospital.repository.hibernate;

import org.dantsov.hospital.models.AmountForPayment;
import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.repository.AmountForPaymentRepository;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 03.04.13
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class AmountForPaymentRepositoryHibernate extends RepositoryHibernate
        implements AmountForPaymentRepository {

    @Override
    public void save(AmountForPayment amountForPayment) {
        session().saveOrUpdate(amountForPayment);
    }

    @Override
    public AmountForPayment getByPacientAndDate(Pacient pacient, Date monthForPayment) {
        Query query = session().createQuery(
                "FROM AmountForPayment as afp " +
                "WHERE afp.pacient = :pacient " +
                "AND afp.month = :monthForPayment")
                .setParameter("pacient", pacient)
                .setParameter("monthForPayment", monthForPayment);
        return (AmountForPayment) query.uniqueResult();
    }

    @Override
    public AmountForPayment getLastByPacient(Pacient pacient) {
        Query query = session().createQuery(
                "FROM AmountForPayment as afp " +
                "WHERE afp.pacient = :pacient " +
                "ORDER BY afp.month DESC")
                .setParameter("pacient", pacient);
        query.setMaxResults(1);
        return (AmountForPayment) query.uniqueResult();
    }

}
