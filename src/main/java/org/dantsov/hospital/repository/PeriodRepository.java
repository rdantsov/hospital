package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Period;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 26.03.13
 * Time: 17:55
 * To change this template use File | Settings | File Templates.
 */
public interface PeriodRepository {
    void save(Period period);

    @SuppressWarnings("unchecked")
    List<Period> getByContract(Contract contract);

    Date getLastEndPeriod(Contract contract);

    Date getFirstStartPeriod(Contract contract);

    Period getFirstPeriodByContract(Contract contract);

    Period getById(Long id);

    void delete(Period period);
}
