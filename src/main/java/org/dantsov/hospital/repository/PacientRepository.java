package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.Contract;
import org.dantsov.hospital.models.Pacient;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: rus
 * Date: 07.02.13
 * Time: 17:37
 * To change this template use File | Settings | File Templates.
 */
public interface PacientRepository {
    void save(Pacient pacient);

    Pacient getById(Long id);

    @SuppressWarnings("unchecked")
    List<Pacient> getAllWithoutClose();

    @SuppressWarnings("unchecked")
    List<Pacient> getAllWithContract();

    Pacient getByContract(Contract contract);

    @SuppressWarnings("unchecked")
    List<Pacient> getAllWithContractOrderByContractNumber();


    @SuppressWarnings("unchecked")
    List<Pacient> getAll();

    @SuppressWarnings("unchecked")
    List<Pacient> getAllBySearchText(String searchText);

    @SuppressWarnings("unchecked")
    List<Pacient> getAllByPage(int numberOfPage, int countOnPage, String sort);

    @SuppressWarnings("unchecked")
    List<Pacient> getAllByPageAndFilter(int numberOfPage, int countOnPage,
                                        String searchText, boolean isPacientContractClose, String sort);
}
