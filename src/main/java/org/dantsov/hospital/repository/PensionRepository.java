package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.Pacient;
import org.dantsov.hospital.models.Pension;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * autor Dantsov Ruslan
 */

public interface PensionRepository {

    void saveOrUpdate(Pension pension);

    BigDecimal getByPacientAndDate(Pacient pacient, Date date);

    @SuppressWarnings("unchecked")
    List<Pension> getPensionsByPacientAndYear(Pacient pacient, int year);

    @SuppressWarnings("uncheked")
    List<Pension> getByDate(Date date);
}
