package org.dantsov.hospital.repository;

import org.dantsov.hospital.models.AmountForPayment;
import org.dantsov.hospital.models.Pacient;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 03.04.13
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
public interface AmountForPaymentRepository {
    void save(AmountForPayment amountForPayment);

    AmountForPayment getByPacientAndDate(Pacient pacient, Date monthForPayment);

    AmountForPayment getLastByPacient(Pacient pacient);
}
