package org.dantsov.hospital.views;


import java.awt.Color;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import org.dantsov.hospital.controllers.forms.report.PacientForm;
import org.springframework.ui.Model;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 11/07/13
 * Time: 16:29
 * To change this template use File | Settings | File Templates.
 */
public class ListOfPacientsPDF extends AbstractPdfView {

    @Override
    protected void buildPdfDocument(Map<String, Object> model,
                                    Document document,
                                    PdfWriter writer,
                                    HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        int countOnFirstPage = 39;
        int countOnOtherPage = 43;
        boolean firstPage = true;
        Integer numberPage = 1;
        //открывам файл с шаблоном
        PdfReader pdfReader = new PdfReader(getServletContext().
                getResourceAsStream("/WEB-INF/templates/pdf/pacientList.pdf"));
        //указываем выходной поток
        ServletOutputStream out = response.getOutputStream();
        List<PacientForm> pacientFormList = (List<PacientForm>) model.get("pacientFormList");
        //OutputStream out = new FileOutputStream(new File("C:\\Anuj.pdf"));
        //если нужно записывать в файл
        //задаем путь к русским шрифтам
        String pathForNormalFont = request.getSession().getServletContext().getRealPath("/font/times.ttf");
        String pathForBoldFont = request.getSession().getServletContext().getRealPath("/font/timesbd.ttf");
        BaseFont baseNormalFont = BaseFont.createFont(pathForNormalFont, "Cp1251", BaseFont.EMBEDDED);
        BaseFont baseBoldFont = BaseFont.createFont(pathForBoldFont, "Cp1251", BaseFont.EMBEDDED);
        Font font12 = new Font(baseNormalFont, 12);
        Font font14 = new Font(baseNormalFont, 14);
        Color color;

        PdfStamper stamper = new PdfStamper(pdfReader,  out);
        PdfWriter.getInstance(document,out);
        //получаем именнованный список полей из шаблона
        AcroFields form = stamper.getAcroFields();
        //пробегаем по всем полям из модели и пытаемся их заменить в шаблоне
        for (String field: model.keySet()) {
            if (stamper.getAcroFields().getFields().containsKey(field) == true) {
                //задаем полю нужные свойства
                stamper.getAcroFields().setFieldProperty(field, "textfont", baseBoldFont, null);

                //производим замену
                stamper.getAcroFields().setField(field, (String) model.get(field));
            }
        }
        stamper.setFormFlattening(true);
        //y - это расстояние от низа страницы
        float tableX = Utilities.millimetersToPoints(25);
        float tableY = Utilities.millimetersToPoints(297-50);
        float tableY2Page = Utilities.millimetersToPoints(297-20);


        PdfPTable table = new PdfPTable(4);
        table.setTotalWidth(Utilities.millimetersToPoints(175));
        float[] columnWidths = new float[] {
                Utilities.millimetersToPoints(10),
                Utilities.millimetersToPoints(85),
                Utilities.millimetersToPoints(35),
                Utilities.millimetersToPoints(45),
                };
        table.setWidths(columnWidths);

        Integer i = 1;
        Integer count = 0;
        PdfContentByte canvas = null;
        for (PacientForm pacientForm : pacientFormList ) {
            count++; //общее количество записей в таблице
            if (i == 1) {  // при первой записи формируем шапку таблицы
                font14.setColor(Color.WHITE);
                insertCell(table, "№ п/п", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font14, new Color(11, 148, 214), 0.01f, 0.01f, 0f, 0f);
                insertCell(table, "ФИО", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font14, new Color(11, 148, 214), 0.01f, 0.01f, 0f, 0f);
                insertCell(table, "Договор, дата", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font14, new Color(11, 148, 214), 0.01f, 0.01f, 0f, 0f);
                insertCell(table, "Срок пребывания", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font14, new Color(11, 148, 214), 0.01f, 0.01f, 0f, 0f);
            }
            if (i % 2 == 0)
                color = new Color(228, 226, 226);
            else {
                color = Color.WHITE;
            }
            insertCell(table, count.toString(), Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            insertCell(table, pacientForm.getFullName(), Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            insertCell(table, pacientForm.getContract(), Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            insertCell(table, pacientForm.getPeriodOfStay(), Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            i++; //кол-во записей на странице
            ///если колечество записей для первой странице сравнелось с количеством строк
            if ((i >= countOnOtherPage && !firstPage) || (pacientFormList.size() == count && !firstPage)) {
                stamper.insertPage(numberPage, pdfReader.getPageSize(1));
                canvas = stamper.getOverContent(numberPage);
                //вставка номера страницы
                ColumnText ct = new ColumnText(canvas);
                ct.setSimpleColumn(Utilities.millimetersToPoints(195),
                        Utilities.millimetersToPoints(297-10),
                        Utilities.millimetersToPoints(200),
                        Utilities.millimetersToPoints(297-20));
                ct.setText(new Phrase(numberPage.toString(), font12));
                ct.go();
                //номер страницы
                table.writeSelectedRows(0, -1 , tableX, tableY2Page, canvas); //canvas
                table.flushContent();
                numberPage++;
                i = 1;
            }
            ///если колечество записей для второй странице сравнелось с количеством строк
            if ((i >= countOnFirstPage && firstPage) || (pacientFormList.size() == count && firstPage)) {

                canvas = stamper.getOverContent(numberPage);
                //вставка номера страницы
                ColumnText ct = new ColumnText(canvas);
                ct.setSimpleColumn(Utilities.millimetersToPoints(195),
                        Utilities.millimetersToPoints(297-10),
                        Utilities.millimetersToPoints(200),
                        Utilities.millimetersToPoints(297-20));
                ct.setText(new Phrase(numberPage.toString(), font12));
                ct.go();

                table.writeSelectedRows(0, -1 , tableX, tableY, canvas); //canvas
                table.flushContent();
                numberPage++;
                firstPage = false;
                i = 1;
            }
        }
        pdfReader.close();
        stamper.close();
    }

    private void insertCell(PdfPTable table, String text, int alignHorizontal,
                            int alignVertical, int colspan, Font font, Color color,
                            float leftBorderWidth, float rightBorderWidth,
                            float topBorderWidth, float bottomBorderWidth){

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(alignHorizontal);
        cell.setVerticalAlignment(alignVertical);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //set color of cell
        cell.setBackgroundColor(color);
        //border Width
        cell.setBorderWidthTop(topBorderWidth);
        cell.setBorderWidthBottom(bottomBorderWidth);
        cell.setBorderWidthLeft(leftBorderWidth);
        cell.setBorderWidthRight(rightBorderWidth);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")){
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }
}
