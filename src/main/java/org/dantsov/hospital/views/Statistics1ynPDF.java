package org.dantsov.hospital.views;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.*;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 22/07/13
 * Time: 17:43
 * To change this template use File | Settings | File Templates.
 */
public class Statistics1ynPDF extends AbstractPdfView {

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document,
                                    PdfWriter writer, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        //открывам файл с шаблоном
        PdfReader pdfReader = new PdfReader(getServletContext().
                getResourceAsStream("/WEB-INF/templates/pdf/statistics1yn.pdf"));
        //указываем выходной поток
        ServletOutputStream out = response.getOutputStream();

        //OutputStream out = new FileOutputStream(new File("C:\\Anuj.pdf"));
        //если нужно записывать в файл
        //задаем путь к русским шрифтам
        String path = request.getSession().getServletContext().getRealPath("/font/times.ttf");
        BaseFont baseFont = BaseFont.createFont(path, "Cp1251", BaseFont.EMBEDDED);
        PdfStamper stamper = new PdfStamper(pdfReader,  out);
        PdfWriter.getInstance(document,out);
        //получаем именнованный список полей из шаблона
        AcroFields form = stamper.getAcroFields();
        //пробегаем по всем полям из модели и пытаемся их заменить в шаблоне
        for (String field: model.keySet()) {
            if (stamper.getAcroFields().getFields().containsKey(field) == true) {
                //задаем полю нужные свойства
                stamper.getAcroFields().setFieldProperty(field, "textfont", baseFont, null);

                //производим замену
                stamper.getAcroFields().setField(field, (String) model.get(field));
            }
        }
        stamper.setFormFlattening(true);
        pdfReader.close();
        stamper.close();



    }
}

