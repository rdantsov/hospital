package org.dantsov.hospital.views;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.*;
import org.dantsov.hospital.controllers.forms.report.DebtRegisterForm;
import org.dantsov.hospital.controllers.forms.report.PacientForm;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rdantsov
 * Date: 24/07/13
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
public class DebtRegisterPDF extends AbstractPdfView {

    @Override
    @SuppressWarnings("")
    protected void buildPdfDocument(Map<String, Object> model, Document document,
                                    PdfWriter writer, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        document.setPageSize(PageSize.A4.rotate());
        document.open();
        document.newPage();
        List<DebtRegisterForm> debtRegisterForms = (List<DebtRegisterForm>) model.get("debtRegisterForm");
        //задаем путь к русским шрифтам
        String path = request.getSession().getServletContext().getRealPath("/font/times.ttf");
        String pathForBoldFont = request.getSession().getServletContext().getRealPath("/font/timesbd.ttf");
        BaseFont baseFont = BaseFont.createFont(path, "Cp1251", BaseFont.EMBEDDED);
        BaseFont baseBoldFont = BaseFont.createFont(pathForBoldFont, "Cp1251", BaseFont.EMBEDDED);
        Font font16 = new Font(baseBoldFont, 16);
        Font font12 = new Font(baseFont, 12);
        //шапка
        Paragraph paragraph1 = new Paragraph();
        Paragraph paragraph2 = new Paragraph();
        Phrase phrase = new Phrase();
        Chunk chunk = new Chunk("Реестр задолженности ", font16);
        phrase.add(chunk);
        paragraph1.add(phrase);
        paragraph1.setAlignment(Element.ALIGN_CENTER);
        paragraph2.add(new Phrase(new Chunk(" за период с " + model.get("startPeriod") + " по " + model.get("endPeriod"), font16)));
        paragraph2.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph1);
        document.add(paragraph2);


        PdfPTable table = new PdfPTable(6);
        table.setSpacingBefore(Utilities.millimetersToPoints(10));
        table.setWidthPercentage(100);
        //table.setTotalWidth(Utilities.millimetersToPoints(185));
        float[] columnWidths = new float[] {
                Utilities.millimetersToPoints(10),
                Utilities.millimetersToPoints(25),
                Utilities.millimetersToPoints(45),
                Utilities.millimetersToPoints(40),
                Utilities.millimetersToPoints(35),
                Utilities.millimetersToPoints(30),
        };
        //шапка таблицы и пустая строка внизу таблицы
        table.setWidths(columnWidths);
        insertCell(table, "№ п/п", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, new Color(226, 239, 248), 0.01f, 0.01f, 0.05f, 0.05f);
        insertCell(table, "Договор, дата", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, new Color(226, 239, 248), 0.01f, 0.01f, 0.05f, 0.05f);
        insertCell(table, "ФИО", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, new Color(226, 239, 248), 0.01f, 0.01f, 0.05f, 0.05f);
        insertCell(table, "Срок пребывания, сумма к оплате", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, new Color(226, 239, 248), 0.01f, 0.01f, 0.05f, 0.05f);
        insertCell(table, "Дата, сумма оплаты", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, new Color(226, 239, 248), 0.01f, 0.01f, 0.05f, 0.05f);
        insertCell(table, "Задолженность (+), излишне оплачено (-)", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, new Color(226, 239, 248), 0.01f, 0.01f, 0.05f, 0.05f);


        table.setHeaderRows(2);
        table.setFooterRows(1);
        insertCell(table, "", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, Color.WHITE, 0.01f, 0.01f, 0f, 0.05f);
        insertCell(table, "", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, Color.WHITE, 0.01f, 0.01f, 0f, 0.05f);
        insertCell(table, "", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, Color.WHITE, 0.01f, 0.01f, 0f, 0.05f);
        insertCell(table, "", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, Color.WHITE, 0.01f, 0.01f, 0f, 0.05f);
        insertCell(table, "", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, Color.WHITE, 0.01f, 0.01f, 0f, 0.05f);
        insertCell(table, "", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, Color.WHITE, 0.01f, 0.01f, 0f, 0.05f);

        //основные данные
        Integer count = 0;
        Color color = Color.WHITE;
        for (DebtRegisterForm debtRegisterForm : debtRegisterForms) {
            count++;
            if (count % 2 == 0)
                color = new Color(228, 226, 226);
            else {
                color = Color.WHITE;
            }
            insertCell(table, count.toString(), Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            insertCell(table, debtRegisterForm.getContract(), Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            insertCell(table, debtRegisterForm.getFullName(), Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            insertCell(table, debtRegisterForm.getPeriodOfStayAndAmount(), Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            insertCell(table, debtRegisterForm.getDateAndPaidSum(), Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
            insertCell(table, debtRegisterForm.getTotalAmount(), Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, font12, color, 0.01f, 0.01f, 0f, 0f);
        }

        document.add(table);
        document.close();
    }

    private void insertCell(PdfPTable table, String text, int alignHorizontal,
                            int alignVertical, int colspan, Font font, Color color,
                            float leftBorderWidth, float rightBorderWidth,
                            float topBorderWidth, float bottomBorderWidth){

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(alignHorizontal);
        cell.setVerticalAlignment(alignVertical);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //set color of cell
        cell.setBackgroundColor(color);
        //border Width
        cell.setBorderWidthTop(topBorderWidth);
        cell.setBorderWidthBottom(bottomBorderWidth);
        cell.setBorderWidthLeft(leftBorderWidth);
        cell.setBorderWidthRight(rightBorderWidth);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")){
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }
}
