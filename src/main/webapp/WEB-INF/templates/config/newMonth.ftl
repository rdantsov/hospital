<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Переход на новый месяц</h3>
    <div class="hero-unit">
        <form class="form-inline" method="POST" action="newMonth">
            <@l.infoMessage 'info', 'При переходе на следующуий месяц, будут скопированы пенсии пациентов, значениями текущего месяца!'/>
            <p><h3>Текущий месяц ${currentDate?string("MMMM yyyy")} года</h3></p>
            <p><h3> Вы собираетесь перейти на ${nextDate?string("MMMM yyyy")} года</h3></p>
            <div class="form-actions">
                <button id="createButton" name="save" class="btn btn-primary">
                    Перейти
                </button>
                <a class="btn" href="<@s.url'/pacients'/>" >Отмена</a>
            </div>
        </form>
    </div>
</div>
<@l.footer/>
