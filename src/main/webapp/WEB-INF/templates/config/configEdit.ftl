<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Изменение констант</h3>
    <div class="hero-unit">
        <@l.errorValidMessage 'configForm.*'/>
        <form class="form-inline" method="POST" action="edit">
            <div class="accordion" id="accordion2">
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                            Основные константы
                        </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in">
                        <div class="accordion-inner">
                            <div class="control-group">
                                <label class="control-label">Полное имя заведующей</label>
                                <@s.formInput 'configForm.fullNameChief' 'class="input-xxlarge"'/>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Сокращенная запись ФИО заведующей</label>
                                <@s.formInput 'configForm.shortNameChief' 'class="input-xxlarge"'/>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Данные о доверенности (заведующая)</label>
                                <@s.formInput 'configForm.vicariousAuthorty' 'class="input-xxlarge"'/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                            Системные константы
                        </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <div class="control-group">
                                <label class="control-label">Кол-во пациентов отображаемых на странице</label>
                                <@s.formInput 'configForm.countPacientOnPage' 'class="input-xxlarge"'/>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Правило округлений (руб.)</label>
                                <@s.formInput 'configForm.roundingAmount' 'class="input-xxlarge"'/>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Имя пользователя</label>
                            <@s.formInput 'configForm.login' 'class="input-xxlarge"'/>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Пароль</label>
                            <@s.formInput 'configForm.password' 'class="input-xxlarge"'/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button id="createButton" name="save" class="btn btn-primary">
                    Обновить
                </button>
                <a class="btn" href="<@s.url'/pacients'/>" >Отмена</a>
            </div>
        </form>

    </div>
</div>
<@l.footer/>
