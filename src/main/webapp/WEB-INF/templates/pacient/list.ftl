<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<#import "/myComponent.ftl" as mc/>
<@l.header/>
<@l.menu "Список пациентов", userName/>
    <!-- Main hero unit for a primary marketing message or call to action -->
    <div class="container">
        <h3 class="breadcrumb">Список пациентов</h3>
        <div class="hero-unit">
            <form class="form-inline" method="GET" action="pacients">
                <#if page??>
                    <input type="hidden" name="page" value="${page}"/>
                </#if>
                <input type="text" class="span2 search-query" name="searchText" value="${pacientListParametrsForm.searchText}">
                <label class="control-label" for="checkOutPacient">Выбывшие пациенты</label>
                <input type="checkbox" name="pacientContractClose"
                <#if pacientListParametrsForm.pacientContractClose>checked="true"</#if>/>
                <button type="submit" class="btn">Поиск</button>
            </form>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th><a href='<@s.url"/pacients?sort=contract"/>'>Номер договора</a></th>
                        <th><a href='<@s.url"/pacients?sort=fullName"/>'>Фамилия, Имя, Отчество</a></th>
                        <th class="span3">Паспорт</th>
                        <th class="span2">Статус договора</th>
                        <th class="span3">Действия</th>
                    </tr>
                </thead>
                <tbody>
                    <#list pacients as pacient>
                        <tr>
                            <td>
                                ${pacient.contract.nomer}
                            </td>
                            <td>
                                <a href="#">${pacient.surname}&nbsp${pacient.name}&nbsp${pacient.midleName} </a>
                            </td>
                            <td>
                                ${pacient.passport}
                            </td>
                            <td>
                                <#switch  pacientWithContractStatus[pacient.id?c?string]>
                                    <#case 'ACTIVE'>
                                        <span class="label label-info">Активный</span>
                                        <#break>
                                    <#case 'COMPLETED'>
                                        <span class="label label-important">Завершен</span>
                                        <#break>
                                    <#case 'CLOSE'>
                                        <span class="label">Закрыт</span>
                                        <#break>
                                </#switch>
                            </td>
                            <td>
                                <#if pacientWithContractStatus[pacient.id?c?string] != 'CLOSE'>
                                    <div class="btn-group">
                                        <a class="btn btn-mini" href="#">Операции</a>
                                        <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href='<@s.url"/pacient/${pacient.id?c}/pension?page=${pacientListParametrsForm.page}"/>'>Добавить пенсию</a>
                                                <a href='<@s.url"/pacient/${pacient.id?c}/contract/list"/>'>Продлить/изменить срок пребывания</a>
                                                <a href='<@s.url"/pacient/${pacient.id?c}/edit"/>'>Изменить данные о пациенте</a>
                                                <#if pacientWithContractStatus[pacient.id?c?string] == 'COMPLETED'>
                                                    <a href='<@s.url"/pacient/${pacient.id?c}/contract/close"/>'>Закрыть договор</a>
                                                </#if>
                                                <a target="_blank"
                                                   href='<@s.url"/reports/pacient/${pacient.id?c}/contract/pdf"/>'>
                                                    Печать договора
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                <#else>
                                    <div class="btn-group">
                                        <a class="btn btn-mini" href='<@s.url"/pacient/${pacient.id?c}/contract/reopen"/>'
                                           onclick="return confirm('Внимание договор будет заново открыт, для него вновь будут доступны все операции.')">
                                            Открыть заново
                                        </a>
                                    </div>
                                </#if>
                            </td>
                        </tr>
                    </#list>
                </tbody>
            </table>
            <ul class="pager pagination-right">
                <#if pacientListParametrsForm.page == 1>
                    <li class="disabled"><a href="#">Предыдушая</a></li>
                <#else>
                    <li>
                        <a href='<@s.url"/pacients?page=${prevPage}"/>'>
                            Предыдушая
                        </a>
                    </li>
                </#if>
                <#if availableNextPage>
                    <li>
                        <a href='<@s.url"/pacients?page=${nextPage}"/>'>
                            Следующая
                        </a>
                    </li>
                <#else>
                    <li class="disabled"><a href='#'>Следующая</a></li>
                </#if>
            </ul>
        </div>
    </div>
<@l.footer/>

