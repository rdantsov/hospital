<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Добавить пациента", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Добавление пациента</h3>
    <@s.bind 'pacientForm.*'/>
    <#if new??>
        <form class="form-inline" method="POST" action="new">
    <#else>
        <form class="form-inline" method="POST" action="edit">
    </#if>
        <div class="hero-unit">
            <@l.errorValidMessage 'pacientForm.*'/>
                <div class="well">
                    Данные о заказчике:
                    <div class="control-group">
                        <label class="control-label">Фамилия</label>
                        <@s.formInput 'pacientForm.customerSurname' 'class="input-medium"'/>
                        <!-- < @s.showErrors "pacientForm.customerSurname","error" /> -->

                        <label class="control-label">Имя</label>
                         <@s.formInput 'pacientForm.customerName' 'class="input-medium"'/>

                        <label class="control-label">Отчество</label>
                        <@s.formInput 'pacientForm.customerMidleName' 'class="input-medium" '/>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Адрес</label>
                        <@s.formInput 'pacientForm.customerAddress' 'class="input-xxlarge" '/>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Паспорт (серия)</label>
                        <@s.formInput 'pacientForm.customerPassport' 'class="input-small" '/>

                        <label class="control-label">Выдавший орган, дата</label>
                        <@s.formInput 'pacientForm.customerAuthorityIssuingPassport' 'class="input-xlarge" '/>
                    </div>
                </div>

                <div class="well">
                    Данные о больном:
                    <div class="control-group">
                    <label class="control-label">Фамилия</label>
                    <@s.formInput 'pacientForm.surname' 'class="input-medium" id="surname"'/>

                    <label class="control-label">Имя</label>
                    <@s.formInput 'pacientForm.name' 'class="input-medium" id="name"'/>

                    <label class="control-label">Отчество</label>
                    <@s.formInput 'pacientForm.midleName' 'class="input-medium " id="midleName"'/>
                </div>

                <div class="control-group">
                    <label class="control-label">Адрес</label>
                    <@s.formInput 'pacientForm.address' 'class="input-xxlarge" id="address"'/>

                    <label class="control-label">Дата рождения</label>
                    <div class="input-append date" id="dateBirthday" data-date=${currentDate?date}
                            data-date-format="dd.mm.yyyy">
                    <input type="text" class="input-small" name="birthday"
                           value="${pacientForm.birthday?string("dd.MM.yyyy")}" readonly/>
                        <span class="add-on">
                            <i class="icon-calendar"></i>
                        </span>
                </div>
                    <script type="text/javascript">
                        $("#dateBirthday").datepicker({
                            autoclose: "true",
                            language: "ru"
                        });
                    </script>
                </div>

                <div class="control-group">
                    <label class="control-label">Паспорт (серия)</label>
                    <@s.formInput 'pacientForm.passport' 'class="input-small" id="passport"'/>

                    <label class="control-label">Выдавший орган, дата</label>
                    <@s.formInput 'pacientForm.authorityIssuingPassport' 'class="input-xlarge" id="authorityIssuingPassport"'/>
                </div>
            </div>
            <div class="well">
                Данные о договоре:
                <div class="control-group">
                    <label class="control-label">Номер договора</label>
                    <@s.formInput 'pacientForm.contractNomer' 'class="input-small" id="contractNomer"'/>

                    <label class="control-label">Дата заключения</label>
                    <div class="input-append date" id="dateCreate" data-date=${currentDate?date}
                            data-date-format="dd.mm.yyyy">
                    <input type="text" class="input-small" name="dateCreated"
                           value="${pacientForm.dateCreated?string("dd.MM.yyyy")}" readonly/>
                        <span class="add-on">
                            <i class="icon-calendar"></i>
                        </span>
                    </div>
                    <script type="text/javascript">
                        $("#dateCreate").datepicker({
                            autoclose: "true",
                            language: "ru"
                        });
                    </script>

                    <label class="control-label">Период с</label>
                    <#if new??>
                        <div class="input-append date" id="dateStart" data-date=${currentDate?date}
                                data-date-format="dd.mm.yyyy">
                            <input type="text" class="input-small" name="startPeriod"
                               value="${pacientForm.startPeriod?string("dd.MM.yyyy")}" readonly/>
                            <span class="add-on">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <script type="text/javascript">
                            $("#dateStart").datepicker({
                                autoclose: "true",
                                language: "ru"
                            });
                        </script>
                    <#else>
                        <input type="text" class="input-small" name="startPeriod"
                               value="${pacientForm.startPeriod?string("dd.MM.yyyy")}" readonly/>
                    </#if>

                    <label class="control-label">Период по</label>
                    <#if new??>
                        <div class="input-append date" id="dateEnd" data-date=${endDate?date}
                                data-date-format="dd.mm.yyyy">
                            <input type="text" class="input-small" name="endPeriod"
                                value="${pacientForm.endPeriod?string("dd.MM.yyyy")}" readonly/>
                            <span class="add-on">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <script type="text/javascript">
                            $("#dateEnd").datepicker({
                                autoclose: "true",
                                language: "ru"
                            });
                        </script>
                    <#else>
                        <input type="text" class="input-small" name="endPeriod"
                            value="${pacientForm.endPeriod?string("dd.MM.yyyy")}" readonly/>
                    </#if>
                </div>
            </div>
            <div class="form-actions">
                <#if new??>
                    <button id="createButton" name="save" class="btn btn-primary">
                        Добавить
                    </button>
                <#else>
                    <button id="createButton" name="save" class="btn btn-primary">
                        Обновить
                    </button>
                </#if>
                <a class="btn" href="<@s.url'/pacients'/>" >Отмена</a>
            </div>
        </div>
    </form>
</div>
<@l.footer/>

