<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Продление договора</h3>
    <div class="hero-unit">
        <@l.errorValidMessage 'periodForm.*'/>
        <h4>
            Пациент: ${pacient.surname} ${pacient.name} ${pacient.midleName}
        </h4>
        <div>Новый период:</div>
        <#if new??>
            <form class="form-inline" method="POST" action="new">
        <#else>
            <form class="form-inline" method="POST" action="edit">
        </#if>
            <@s.bind 'periodForm.*'/>
            <div class="control-group">
                <label class="control-label" for="startPeriod">C</label>
                <#if onlyOnePeriod??>
                    <div class="input-append date" id="dateStart" data-date=${periodForm.startPeriod?date}
                            data-date-format="dd.mm.yyyy">
                        <input type="text" class="input-small" id="startPeriod"
                               name="startPeriod" value=${periodForm.startPeriod?string("dd.MM.yyyy")} readonly />
                        <span class="add-on">
                            <i class="icon-calendar"></i>
                        </span>
                     </div>
                    <script type="text/javascript">
                        $("#dateStart").datepicker({
                            autoclose: "true",
                            language: "ru"
                        });
                    </script>
                <#else>
                    <@s.formInput 'periodForm.startPeriod' 'class="input-medium uneditable-input" id="startPeriod"
                        readonly'/>
                </#if>

                <label class="control-label" for="endPeriod">По</label>
                <div class="input-append date" id="dateEnd" data-date=${calNextPeriod?date}
                        data-date-format="dd.mm.yyyy">
                    <input type="text" class="input-small" id="endPeriod"
                           name="endPeriod" value=${periodForm.endPeriod?string("dd.MM.yyyy")} readonly />

                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                 </div>
                <script type="text/javascript">
                    $("#dateEnd").datepicker({
                        autoclose: "true",
                        language: "ru"
                    });
                </script>

            </div>
            <div class="form-actions">
                <#if new??>
                    <button id="createButton" name="save" class="btn btn-primary">
                        Продлить
                    </button>
                <#else>
                    <button id="createButton" name="save" class="btn btn-primary">
                        Обновить
                    </button>
                </#if>
                <a class="btn" herf="#" onclick="window.history.back()">К списку периодов</a>
            </div>
        </form>

    </div>
</div>
<@l.footer/>
