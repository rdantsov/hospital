<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Закрытие договора</h3>
    <div class="hero-unit">
        <#if error??>
            <@l.errorMessage error, error/>
        </#if>
        <#if success??>
            <@l.successMessage success, success/>
            <form class="form-inline" method="POST" action="close">
                <div class="form-actions">
                    <button id="createButton" name="closeContract" class="btn btn-primary">
                        Закрыть договор
                    </button>
                    <a class="btn" href="<@s.url'/pacients'/>" >Отмена</a>
                </div>
            </form>
        <#else>
            <div class="form-actions">
                <a class="btn" href="<@s.url'/pacients'/>" >Отмена</a>
            </div>
        </#if>
    </div>
</div>
<@l.footer/>
