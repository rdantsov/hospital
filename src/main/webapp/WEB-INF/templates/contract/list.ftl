<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Договор", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Договор: список периодов</h3>
    <div class="hero-unit">
        <h4>
            Пациент: ${pacient.surname} ${pacient.name} ${pacient.midleName}
        </h4>
        <h4>
            Договор: ${pacient.contract.nomer}
        </h4>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>№</th>
                <th class="span3">Период С:</th>
                <th class="span3">Период По:</th>
                <th class="span4">Операции</th>
            </tr>
            </thead>
            <tbody>
                <#list periods as period>
                    <tr>
                        <td>
                            ${period_index + 1}
                        </td>
                        <td>
                            ${period.startPeriod}
                        </td>
                        <td>
                            ${period.endPeriod}
                        </td>
                        <td>
                            <#if period_has_next>
                            <#else>
                                <#assign periodsSize=periods?size-1>
                                <#if periodsSize != 0>
                                    <div class="btn-group">
                                        <a class="btn btn-mini" href='<@s.url"/pacient/contract/${pacient.contract.id?c}/period/${period.id?c}/edit"/>'>Изменить</a>
                                        <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href='<@s.url"/pacient/${pacient.id?c}/contract/period/${period.id?c}/delete"/>'>Удалить</a>
                                            </li>
                                        </ul>
                                    </div>
                                <#else>
                                    <div class="btn-group">
                                        <a class="btn btn-mini" href='<@s.url"/pacient/contract/${pacient.contract.id?c}/period/${period.id?c}/edit"/>'>Изменить</a>
                                    </div>
                                </#if>
                            </#if>
                        </td>
                    </tr>
                </#list>
            </tbody>
        </table>
        <div class="form-actions">
            <a class="btn" href="<@s.url'/pacient/contract/${pacient.contract.id?c}/period/new'/>" >Продлить договор</a>
            <a class="btn" href="<@s.url'/pacients'/>" >Отмена</a>
        </div>
    </div>
</div>
<@l.footer/>
