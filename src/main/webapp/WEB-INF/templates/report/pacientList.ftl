<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Отчет "Список пациентов"</h3>
    <div class="hero-unit">
        <form class="form-inline" method="POST" action="pacientList" target="_blank">
            <div class="control-group">
                <label class="control-label" for="year">Кол-во пациентов на:&nbsp;</label>
                <div class="input-append date" id="dateReport" data-date-format="dd.mm.yyyy"
                     data-date=${configDate?string("dd.MM.yyyy")} >
                    <input type="text" class="input-small" id="year" name="dateForReport"
                       value=${dateForReport?string("dd.MM.yyyy")} readonly />

                    <span class="add-on">
                                <i class="icon-calendar"></i>
                    </span>
                </div>

            <script type="text/javascript">
                $("#dateReport").datepicker({
                    autoclose: "true",
                    language: "ru"
                });
            </script>
            <button id="searchButton" class="btn btn-primary">
                Cформировать отчет
            </button>
            </div>
        </form>
    </div>
</div>
<@l.footer/>
