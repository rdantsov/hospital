<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Отчет "1-ун"</h3>
    <div class="hero-unit">
        <form class="form-inline" method="POST" action="statistics1yn" target="_blank">
            <div class="control-group">
                <label class="control-label" for="year">Год:&nbsp;</label>
                <div class="input-append date" id="dateTo" data-date-format="yyyy"
                     data-date=${configDate?string("dd.MM.yyyy")} >
                    <input type="text" class="input-small" name="yearForReport"
                           value=${yearForReport?string("yyyy")} readonly />
                    <span class="add-on">
                                <i class="icon-calendar"></i>
                    </span>
                </div>
                <script type="text/javascript">
                    $("#dateTo").datepicker({
                        format: " yyyy",
                        viewMode: "years",
                        minViewMode: "years",
                        autoclose: "true",
                        language: "ru"
                    });
                </script>

                <button id="searchButton" class="btn btn-primary">
                    Cформировать отчет
                </button>
            </div>
        </form>
    </div>
</div>
<@l.footer/>
