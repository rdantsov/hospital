<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<script src="<@s.url '/js/my/statistics6yn.js'/>"></script>
<div class="container">
    <h3 class="breadcrumb">Отчет "6-ун"</h3>
    <div class="hero-unit">
        <@l.errorValidMessage 'periodForm.*'/>
        <form class="form-inline" method="POST" action="statistics6yn" target="_blank">
            <div id="divReportForAllPacient" class="control-group">
                <label class="control-label" for="dateFrom">Дата c:&nbsp;</label>
                <div class="input-append date" id="dateFrom" data-date-format="yyyy"
                     data-date=${periodForm.startPeriod?string("dd.MM.yyyy")} >
                    <input id="startPeriod" type="text" class="input-small" name="startPeriod"
                           value=${periodForm.startPeriod?string("yyyy")} readonly />
                    <span class="add-on">
                           <i class="icon-calendar"></i>
                    </span>
                </div>
                <script type="text/javascript">
                    $("#dateFrom").datepicker({
                        format: " yyyy",
                        viewMode: "years",
                        minViewMode: "years",
                        autoclose: "true",
                        language: "ru"
                    });
                </script>

                <label class="control-label" for="dateto">Дата по:&nbsp;</label>
                <div class="input-append date" id="dateTo" data-date-format="mm.yyyy"
                     data-date=${periodForm.endPeriod?string("dd.MM.yyyy")} >
                    <input id="endPeriod" type="text" class="input-small" name="endPeriod"
                           value=${periodForm.endPeriod?string("MM.yyyy")} readonly />
                    <span class="add-on">
                                <i class="icon-calendar"></i>
                    </span>
                </div>
                <script type="text/javascript">
                    $("#dateTo").datepicker({
                        format: " mm.yyyy",
                        viewMode: "months",
                        minViewMode: "months",
                        autoclose: "true",
                        language: "ru"
                    });
                </script>

                <button id="searchButton" class="btn btn-primary">
                    Cформировать отчет
                </button>
            </div>
        </form>
    </div>
</div>
<@l.footer/>
