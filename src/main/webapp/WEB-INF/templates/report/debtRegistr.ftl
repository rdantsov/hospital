<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<script src="<@s.url '/js/my/myCollapse.js'/>"></script>
<script src="<@s.url '/js/my/debtRegistr.js'/>"></script>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Отчет "Реестр задолжности"</h3>
    <div class="hero-unit">
            <@l.errorValidMessage 'periodForm.*'/>
            <div class="accordion" id="accordion2">
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                            Отчет по всем пациентам
                        </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in">
                        <div class="accordion-inner">
                            <form class="form-inline" method="POST" action="debtRegistr" target="_blank">
                                <div id="divReportForAllPacient" class="control-group">
                                    <label class="control-label" for="dateFrom">Дата c:&nbsp;</label>
                                    <div class="input-append date" id="dateFrom" data-date-format="dd.mm.yyyy"
                                         data-date=${periodForm.startPeriod?string("dd.MM.yyyy")} >
                                        <input id="startPeriod" type="text" class="input-small" name="startPeriod"
                                               value=${periodForm.startPeriod?string("dd.MM.yyyy")} readonly />
                                        <span class="add-on">
                                               <i class="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <script type="text/javascript">
                                        $("#dateFrom").datepicker({
                                            autoclose: "true",
                                            language: "ru"
                                        });
                                    </script>

                                    <label class="control-label" for="dateto">Дата по:&nbsp;</label>
                                    <div class="input-append date" id="dateTo" data-date-format="dd.mm.yyyy"
                                         data-date=${periodForm.endPeriod?string("dd.MM.yyyy")} >
                                        <input id="endPeriod" type="text" class="input-small" name="endPeriod"
                                               value=${periodForm.endPeriod?string("dd.MM.yyyy")} readonly />
                                        <span class="add-on">
                                                    <i class="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <script type="text/javascript">
                                        $("#dateTo").datepicker({
                                            autoclose: "true",
                                            language: "ru"
                                        });
                                    </script>

                                    <button id="searchButton" class="btn btn-primary">
                                        Cформировать
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                            Отчет по пациенту
                        </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <form class="form-inline" method="GET" action="debtRegistr">
                                <input id="searchText" type="text" class="span2 search-query" name="searchText" value="${searchText}">
                                <button type="submit" class="btn">Поиск</button>
                            </form>

                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Номер договора</th>
                                        <th>Фамилия, Имя, Отчество</th>
                                        <th class="span3">Паспорт</th>
                                        <th class="span3">Действия</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <#list pacients as pacient>
                                        <tr>
                                            <td>
                                                ${pacient.contract.nomer}
                                            </td>
                                            <td>
                                                ${pacient.surname}&nbsp${pacient.name}&nbsp${pacient.midleName}
                                            </td>
                                            <td>
                                                ${pacient.passport}
                                            </td>
                                            <td>
                                                <a target="_blank" class="btn btn-mini" href='<@s.url"/reports/debtRegistr/${pacient.id?c}/pdf"/>'>
                                                    Сформировать
                                                </a>
                                            </td>
                                        </tr>
                                    </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<@l.footer/>
