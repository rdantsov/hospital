<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html lang="ru">
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <link rel="stylesheet" type="text/css" href='<@s.url "/css/bootstrap.css"/>'/>
    <head>
        <meta charset="utf-8"/>
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin {
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

        </style>
    </head>
    <body>

        <div class="container">
            <form class="form-signin" method="POST" action="<@s.url'/j_spring_security_check'/>"  >
               <h2 class="form-signin-heading">Авторизация</h2>
               <#if RequestParameters.error??>
                    <@l.errorMessage RequestParameters.error, 'Неизвестный пользователь или неверный пароль.'/>
               </#if>
                <fieldset>
                    <div class="control-group">
                        <label class="control-label">Имя пользователя</label>
                        <input class="input-block-level" type="text" name='j_username' class="span3" id="loginField"/>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Пароль</label>
                        <input class="input-block-level" type="password" name='j_password' class="span3" id="passwordField"/>
                    </div>
                    <button id="loginButton" class="btn btn-large btn-primary">Войти</button>
                </fieldset>
            </form>
        </div>
        <script src="<@s.url '/js/jquery.min.js'/>"></script>
        <script src="<@s.url '/js/bootstrap.js'/>"></script>
    </body>
</html>
