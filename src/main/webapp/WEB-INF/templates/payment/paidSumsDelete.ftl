<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Удаление банковской выписки</h3>
    <div class="hero-unit">
        <form class="form-horizontal" method="POST" action="delete">
            <fieldset>
                <div class="control-group">
                    <div class="span2">№ выписки:</div>
                    <div class="span4" >${bankStatment.number}</div>
                </div>
                <div class="control-group">
                    <div class="span2">Дата:</div>
                    <div class="span4">${bankStatment.date?string("dd.MM.yyyy")}</div>
                </div>
            </fieldset>
            <div class="form-actions">
                <button id="deleteButton" class="btn btn-danger" type="submit">Удалить</button>
                <a class="btn" href="<@s.url'/pacients/payment/paidsum/list'/>">Отмена</a>
            </div>
        </form>
    </div>
</div>
<@l.footer/>
