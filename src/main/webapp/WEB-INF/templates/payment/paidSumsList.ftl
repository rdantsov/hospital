<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Выписки из банка</h3>
    <div class="hero-unit">
        <form class="form-inline" method="GET" action="list">
            <a class="btn" href="<@s.url'/payment/paidsum/pacient/new'/>" >Создать реестр оплаты</a>
            <a class="btn" href="<@s.url'/payment/paidsum/hospital/new'/>" >Создать реестр возврата излишней оплаты</a>
            <div class="search-box pull-right">
                <div class="input-append date" id="dateForSearch" data-date="${currentDate?date}"
                     data-date-format=" mm.yyyy">
                    <input type="text" class="input-small" id="date" name="dateSearch"
                           value="${dateSearch?string("MM.yyyy")}" readonly />
                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                </div>

                <script type="text/javascript">
                    $("#dateForSearch").datepicker({
                        format: " mm.yyyy",
                        viewMode: "months",
                        minViewMode: "months",
                        autoclose: "true",
                        language: "ru"
                    });
                </script>
                <button id="searchButton" class="btn btn-primary">
                    Выбрать
                </button>
            </div>
        </form>
        <h5>
            Выписки за ${dateSearch?string("MMMM  yyyy")} года
        </h5>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="span3">№ выписки</th>
                <th class="span3">Дата</th>
                <th class="span3">Тип выписки</th>
                <th class="span2">Операции</th>
            </tr>
            </thead>
            <tbody>
                <#list bankStatments as bankStatment>
                    <tr>
                        <td>${bankStatment.number?c}</td>
                        <td>${bankStatment.date?string("dd.MM.yyyy")}</td>
                        <#if bankStatment.type = 'PACIENT_PAID'>
                            <td>
                                <span class="label label-info">
                                    ${bankStatment.type.russianName}
                                </span>
                            </td>
                        <#else>
                            <td>
                                <span class="label label-success">
                                    ${bankStatment.type.russianName}
                                </span>
                            </td>
                        </#if>
                        <td>
                            <#if !bankStatmentsIsHaveContractClose[bankStatment.id?c?string]>
                                <div class="btn-group">
                                    <a class="btn btn-mini" href="<@s.url'/payment/paidsum/${bankStatment.id?c}/edit'/>">
                                        Изменить
                                    </a>
                                    <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<@s.url'/payment/paidsum/${bankStatment.id?c}/delete'/>"
                                               onclick="return confirm('Удалить выписку № ${bankStatment.number}')">
                                                Удалить
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            <#else>
                                <div class="btn-group">
                                    <a class="btn btn-mini" href="<@s.url'/payment/paidsum/${bankStatment.id?c}/show'/>">
                                        Отобразить
                                    </a>
                                </div>
                            </#if>
                        </td>
                    </tr>
                </#list>

            </tbody>
        </table>



    </div>
</div>
<@l.footer/>
