<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Пациенты", " userName"/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container" xmlns="http://www.w3.org/1999/html">
    <h3 class="breadcrumb">Ввод сумм пенсии</h3>
    <div class="hero-unit">
        <@l.errorValidMessage 'pensionFormList.*'/>
        <input type="hidden" name="page" value="${page}"/>
        <input type="hidden" name="searchText" value="${searchText}"/>
        <input type="hidden" name="isPacientContractClose" value="${isPacientContractClose?string}"/>
            <form class="form-inline" method="GET" action="pension">
                <div class="search-box pull-right">

                    <div class="input-append date" id="dateForSearch" data-date=${pensionSearchForm.date?date}
                            data-date-format=" yyyy">
                        <input type="text" class="input-small" id="year" name="date"
                               value=${pensionSearchForm.date?string("yyyy")} readonly />

                        <span class="add-on">
                                <i class="icon-calendar"></i>
                            </span>
                    </div>
                    <script type="text/javascript">
                        $("#dateForSearch").datepicker({
                            format: " yyyy",
                            viewMode: "years",
                            minViewMode: "years",
                            autoclose: "true",
                            language: "ru"
                        });
                    </script>
                    <button id="searchButton" class="btn btn-primary">
                        Выбрать
                    </button>
                </div>
            </form>

            <h4>
                Пациент: ${pacient.surname} ${pacient.name} ${pacient.midleName}
            </h4>

            <h5>
                Пенсия за ${pensionSearchForm.date?string("yyyy")} год
            </h5>
            <form class="form-horizontal" method="POST" action="pension">
                <@s.formHiddenInput 'pensionSearchForm.date'/>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="span3">Месяц</th>
                            <th class="span3">Сумма</th>
                        </tr>
                    </thead>
                    <tbody>

                        <#list pensionFormList.pensionForms as pensionForm>
                            <#assign index=pensionFormList.pensionForms?seq_index_of(pensionForm)>
                            <@s.bind 'pensionFormList.pensionForms[index].*'/>
                        <tr>
                            <td>
                                ${pensionForm.month?string("MMMM")}
                                <input type="hidden" class="input-small" name="pensionForms[${index}].month"
                                           value="${pensionForm.month?date}" />
                            </td>
                            <td>
                                <#if pensionForm.amount??>
                                    <input type="text" class="input-small" name="pensionForms[${index}].amount"
                                        value="${pensionForm.amount?c}" />
                                <#else> <!--в случае если оставили поле пустым, что бы после вывода ошибки оно и оставалось пустым -->
                                    <input type="text" class="input-small" name="pensionForms[${index}].amount"
                                           value="" />
                                </#if>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
                <div class="form-actions">
                    <button id="createButton" name="save" class="btn btn-primary">
                        Сохранить
                    </button>
                    <a class="btn" href="<@s.url'/pacients?page=${page}&searchText=${searchText}&isPacientContractClose=${isPacientContractClose?string}'/>" >Отмена</a>
                </div>
            </form>
    </div>
</div>
<@l.footer/>
