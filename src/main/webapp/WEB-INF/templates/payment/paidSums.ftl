<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<#import "/myComponent.ftl" as mc/>
<@l.header/>
<@l.menu "Пациенты", userName/>
<script src="<@s.url '/js/my/paidSums.js'/>"></script>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container" xmlns="http://www.w3.org/1999/html">
    <#if bankStatmentType??>
        <h3 class="breadcrumb">Реестр оплаченных сумм</h3>
    <#else>
        <h3 class="breadcrumb">Реестр возврата излишней оплаты</h3>
    </#if>

    <div class="hero-unit" ng-controller="MainCtrl">
            <#if new??>
                <form class="form-inline" method="POST" action="new">
            <#else>
                <form class="form-inline" method="POST" action="edit">
            </#if>
                <@s.bind 'bankStatmentForm.*'/>
                <@l.errorValidMessage 'bankStatmentForm.*'/>
                <@l.errorValidMessage 'paidSumListForm.*'/>
                <input type="hidden" name="type" value="${bankStatmentForm.type}"/>
                <label  for="numberBankStatment">№ бакновской выписки</label>
                    <#if new??>
                        <@s.formInput 'bankStatmentForm.numberBankStatment' 'class="input-small" id="numberBankStatment"'/>
                    <#else>
                        <@s.formInput 'bankStatmentForm.numberBankStatment' 'class="input-small" id="numberBankStatment" readonly'/>
                    </#if>

                <label for="dateDiv">Дата</label>
                <#if new??>
                    <div class="input-append date" id="dateDiv" data-date="${currentDate?date}"
                        data-date-format="dd.mm.yyyy">
                        <input type="text" class="input-small" name="date"
                            value="${bankStatmentForm.date?string("dd.MM.yyyy")?date}" readonly/>
                        <span class="add-on">
                            <i class="icon-calendar"></i>
                        </span>
                    </div>
                    <script type="text/javascript">
                    $("#dateDiv").datepicker({
                        autoclose: "true",
                        language: "ru"
                    });
                    </script>
                <#else>
                    <input type="text" class="input-small" name="date"
                           value="${bankStatmentForm.date?string("dd.MM.yyyy")}" readonly/>
                </#if>
                <br />
                <br>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="span3">Номер договора</th>
                        <th class="span3">ФИО</th>
                        <th class="span3">Сумма</th>
                        <#if bankStatmentType??>
                            <th class="span3">Оплата кредиткой</th>
                        </#if>
                    </tr>
                    </thead>
                    <tbody>
                        <#list paidSumListForm.paidSumForms as paidSumForm>
                            <#assign index=paidSumListForm.paidSumForms?seq_index_of(paidSumForm)>
                            <@s.bind 'paidSumListForm.paidSumForms[index].*'/>
                            <input type="hidden"
                                name="paidSumForms[${index}].pacientId"
                                value="${paidSumForm.pacientId?c}"/>
                            <tr>
                                <td>
                                    ${paidSumForm.contractNumber}
                                    <input type="hidden"
                                           name="paidSumForms[${index}].contractNumber"
                                           value="${paidSumForm.contractNumber}" />
                                </td>
                                <td>
                                ${paidSumForm.shortName}
                                    <input type="hidden"
                                           name="paidSumForms[${index}].shortName"
                                           value="${paidSumForm.shortName}" />
                                </td>
                                <td class="amount">
                                    <#if view??>
                                        ${paidSumForm.amount?c}
                                    <#else>
                                        <#if paidSumForm.amount??>
                                        <input type="text" id="mmm" class="input-small" name="paidSumForms[${index}].amount"
                                               value="${paidSumForm.amount?c}"/>
                                        <#else>
                                            <input type="text" id="mmm" class="input-small" name="paidSumForms[${index}].amount"
                                                   value=""/>
                                        </#if>
                                    </#if>
                                </td>
                                <#if bankStatmentType??>
                                    <td>
                                        <#if view??>
                                            <input type="checkbox" name="paidSumForms[${index}].paymentCreditCard"
                                                   disabled="true"
                                                   <#if paidSumForm.paymentCreditCard>checked="true"</#if>/>
                                        <#else>
                                            <input type="checkbox" name="paidSumForms[${index}].paymentCreditCard"
                                                <#if paidSumForm.paymentCreditCard>checked="true"</#if>/>
                                        </#if>
                                    </td>
                                </#if>
                            </tr>
                        </#list>
                    </tbody>
                </table>
                <div>Итого по реестру: <label id="labelTotal">0</label> </div>
                <div class="form-actions">
                    <#if view??>
                        <a class="btn" href="<@s.url'/payment/paidsum/list'/>" >Отмена</a>
                    <#else>
                        <button id="createButton" name="save" class="btn btn-primary">
                            Сохранить
                        </button>
                        <a class="btn" href="<@s.url'/payment/paidsum/list'/>" >Отмена</a>
                    </#if>

                </div>
            </form>
    </div>
</div>
<@l.footer/>
