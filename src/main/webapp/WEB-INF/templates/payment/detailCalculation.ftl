<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Детальная информация о расчете для пациента </h3>
    <div class="hero-unit">
        <div>
            <textarea rows="20" class="input-block-level">${detailCalculation}</textarea>
        </div>
        <div class="form-actions">
            <a class="btn" href="javascript:history.back();" >Вернуться назад</a>
        </div>

    </div>
</div>
<@l.footer/>
