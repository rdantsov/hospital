<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb">Расчет оплаты</h3>
    <div class="hero-unit">
        <form class="form-inline" method="GET" action="calculation">
            <div class="search-box pull-right">
                <div class="input-append date" id="dateForSearch" data-date="${currentDate?date}"
                     data-date-format=" mm.yyyy">
                    <input type="text" class="input-small" id="date" name="date"
                       value="${paymentCalculationSearchForm.date?string("MM.yyyy")}" readonly />
                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                </div>

                <script type="text/javascript">
                    $("#dateForSearch").datepicker({
                        format: " mm.yyyy",
                        viewMode: "months",
                        minViewMode: "months",
                        autoclose: "true",
                        language: "ru"
                    });
                </script>
                <button id="searchButton" class="btn btn-primary">
                    Выбрать
                </button>
            </div>
        </form>
        <h5>
            Расчет за ${paymentCalculationSearchForm.date?string("MMMM  yyyy")} года
        </h5>

        <form name="calc" class="form-horizontal" method="POST" action="calculation">
            <@s.formHiddenInput 'paymentCalculationSearchForm.date'/>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="span3">Пациент</th>
                        <th class="span3">Сумма</th>
                        <th class="span3">Операции</th>
                    </tr>
                </thead>
                <tbody>
                    <#list pacients as pacient>

                    <tr>
                            <td>${pacient.surname} ${pacient.name}</td>
                            <td>${amountsForPayment[pacient.id?c?string]}</td>
                            <td>
                                <input type="hidden" class="input-small"
                                       name="pacient" value="${pacient.id?c}" />
                                <div class="btn-group">
                                    <a class="btn btn-mini" target="_blank"
                                      href="<@s.url'/reports/receiptForPayment/pdf?pacient=${pacient.id?c}&date=${paymentCalculationSearchForm.date?string("MM.yyyy")}'/>" >
                                        Печать
                                    </a>
                                    <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<@s.url'/payment/detailcalculation?pacient=${pacient.id?c}&date=${paymentCalculationSearchForm.date?string("MM.yyyy")}'/>" >
                                                  Детализация расчета
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </#list>
                </tbody>
            </table>
        <div class="form-actions">
            <button id="createButton" name="save" class="btn btn-primary">
                Расчет
            </button>
            <a class="btn" href="<@s.url'/pacients'/>" >Отмена</a>
        </div>
        </form>
    </div>
</div>
<@l.footer/>
