<#import "/spring.ftl" as s/>
<#import "/layout.ftl" as l/>
<@l.header/>
<@l.menu "Оплата", userName/>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="container">
    <h3 class="breadcrumb label-warning"> Ошибка </h3>
    <div class="hero-unit">
        Произошла внутренняя ошибка, либо запрашиваемя вами страница не найдена!
        <#if exceptionStackTrace??>
            <div>
                Описание ошибки:
                <textarea rows="10" class="input-block-level">

                        ${exceptionStackTrace?string}
                </textarea>
            </div>
        </#if>
    </div>

    <div class="form-actions">
        <a class="btn" href="javascript:history.back();" >Вернуться назад</a>
    </div>
</div>
<@l.footer/>
