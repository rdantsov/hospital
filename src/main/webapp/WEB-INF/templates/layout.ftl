<#import "spring.ftl" as s/>
<!--Загаловок-->
<#macro header>
    <!DOCTYPE html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" type="text/css" href='<@s.url "/css/bootstrap.css"/>'/>
        <title>МБСУ</title>
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link rel="stylesheet" type="text/css" href='<@s.url "/css/bootstrap.css"/>'/>
        <link rel="stylesheet" type="text/css" href='<@s.url "/css/bootstrap-responsive.css"/>'/>
        <link rel="stylesheet" type="text/css" href='<@s.url "/css/datepicker.css"/>'/>
        <script src="<@s.url '/js/jquery.min.js'/>"></script>
        <script src="<@s.url '/js/bootstrap.js'/>"></script>
        <script src="<@s.url '/js/bootstrap-datepicker.js'/>"></script>
        <script src="<@s.url '/js/locales/bootstrap-datepicker.ru.js'/>"></script>
    </head>
    <body style="background-color:#6891ad">
</#macro>

<!--нижний колонтитул-->
<#macro footer>
    </body>
    </html>
</#macro>

<!--Меню -->
<#macro menu activeMenu, userName>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Пациенты<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href='<@s.url"/pacients"/>'>Список пациентов</a></li>
                            <li><a href='<@s.url"/pacient/new"/>'>Добавить пациента</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Оплата<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href='<@s.url"/payment/calculation"/>'>Расчет оплаты</a></li>
                            <li><a href='<@s.url"/payment/paidsum/list"/>'>Ввод оплаченных сумм</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Отчеты <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="nav-header">Отчеты по больнице</li>
                        <li><a href='<@s.url"/reports/pacientList"/>'>Список пациентов</a></li>
                        <li><a href='<@s.url"/reports/debtRegistr"/>'>Реестр задолжности</a></li>
                        <li class="nav-header">Отчеты в статистику</li>
                        <li><a href='<@s.url"/reports/statistics1yn"/>'>1-ун</a></li>
                        <li><a href='<@s.url"/reports/statistics6yn"/>'>6-ун</a></li>
                    </ul>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Сервис <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href='<@s.url"/configuration/edit"/>'>Изменение констант</a></li>
                            <li><a href='<@s.url"/configuration/newMonth"/>'>Переход на новый месяц</a></li>                        </ul>
                    </li>
                </li>
                </ul>
                <p class="pull-right">
                    <a class="navbar-text">Дата ${configCurrentDate} &nbsp</a>
                    <a class="navbar-text">Пользователь: ${userName}&nbsp</a>
                    <a class="navbar-links" href='<@s.url"/help"/>'>Помощь</a>
                    <a class="brand:hover" href='<@s.url"/logout"/>'>Выход</a>
                </p>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>
</#macro>

<!--блок сообщения об ошибках-->
<#macro errorMessage error, text>
    <#if error??>
        <#if text!="">
            <div class="alert alert-error">
                <strong>Ошибка</strong> </br>
                ${text}
            </div>
        </#if>
    </#if>
</#macro>

<!--Сообщения об успешной операции-->
<#macro successMessage success, text>
    <#if success??>
        <div class="alert alert-success">
            <strong>Сообщение</strong>
            <br />
            ${text}
        </div>
    </#if>
</#macro>

<!--Сообщения об успешной операции-->
<#macro infoMessage info, text>
    <#if info??>
        <div class="alert alert-info">
            <strong>Внимание</strong>
            <br />
            ${text}
        </div>
    </#if>
</#macro>

<!--Сообщения об ошибках валидации-->
<#macro errorValidMessage validForm>
    <@s.bind validForm/>
    <#if s.status.error>
    <div class="alert alert-error">
        <strong>Ошибка</strong>
        <ul>
            <#list s.status.errorMessages as error>
                <li> ${error} </li>
            </#list>
        </ul>
    </div>
    </#if>
</#macro>

<!-- Нижний колонтитул основной области-->
<#macro formFooter>
    <footer>
        <p style="color: #acadad">© Данцов Р.С. 2013</p>
    </footer>
</#macro>




