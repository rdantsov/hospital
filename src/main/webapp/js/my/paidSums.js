var calcTotal = function(){
    var summa = 0;
    $('td.amount input').each(function(i, elem) {
        summa += parseInt($(elem).val())||0;
    });
    $("#labelTotal").text(summa);
};

$(function(){
    calcTotal();
    $("td.amount input").keyup(function () {
        calcTotal();
    });
});