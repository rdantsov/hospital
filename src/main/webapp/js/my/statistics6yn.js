function compareTwoDate() {
    var sfromDate = $("#startPeriod").val();
    var myDateArray = sfromDate.split(".");
    fromDate = new Date(myDateArray[0], "00", "01"); // Получаем дату по строке
    var stoDate = $("#endPeriod").val();
    var myDateArray2 = stoDate.split(".");
    toDate = new Date(myDateArray2[1], myDateArray2[0]-1, "01"); // Получаем дату по строке
    if (fromDate.getTime() > toDate.getTime()) {
        $("#searchButton").attr("disabled", "disabled");
        if ($("#errorMessage").length==0) {
            $("#divReportForAllPacient").after('<div id="errorMessage" class="alert alert-error"><strong>Ошибка данных!<ul><li>Начальная дата должна быть меньше конечной!</li></ul></strong></div>');
        }
        return false;
    } else {
        $("#searchButton").removeAttr("disabled");
        if ($("#errorMessage").length>0) {
            $("#errorMessage").remove();
        }
    }
    if (Math.abs(parseInt(myDateArray[0]) - parseInt(myDateArray2[1])) != 0) {
        $("#searchButton").attr("disabled", "disabled");
        if ($("#errorMessage").length==0) {
            $("#divReportForAllPacient").after('<div id="errorMessage" class="alert alert-error"><strong>Ошибка данных!<ul><li>Период должен быть за один год</li></ul></strong></div>');
        }
        return false;
    } else {
        $("#searchButton").removeAttr("disabled");
        if ($("#errorMessage").length>0) {
            $("#errorMessage").remove();
        }
    }
}

$(document).ready(function(){
    compareTwoDate();
    $("#endPeriod").change(function() {
        compareTwoDate();
    });

    $("#startPeriod").change(function() {
        compareTwoDate();
    });
});

