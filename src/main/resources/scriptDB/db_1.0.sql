CREATE TABLE IF NOT EXISTS Contract (
  id NUMERIC(20, 0) UNIQUE  PRIMARY KEY,
  dateCreated Date NOT NULL,
  nomer NUMERIC(6, 0) NOT NULL,
  completed NUMERIC(1, 0) NOT NULL
);

CREATE TABLE IF NOT EXISTS Period (
  id NUMERIC(20, 0) UNIQUE  PRIMARY KEY,
  startPeriod DATE NOT NULL,
  endPeriod DATE NOT NULL,
  contract NUMERIC(20, 0) NOT NULL,
  FOREIGN KEY (contract) REFERENCES Contract (id)
);

CREATE TABLE IF NOT EXISTS Pacient (
  id NUMERIC(20, 0) UNIQUE PRIMARY KEY,
  surname VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  midleName VARCHAR(255) NOT NULL,
  passport VARCHAR(9) NOT NULL,
  authorityIssuingPassport VARCHAR(255) NOT NULL,
  address VARCHAR(255) NOT NULL,
  birthday DATE NOT NULL,
  customerName VARCHAR(255) NOT NULL,
  customerSurname VARCHAR(255) NOT NULL,
  customerMidleName VARCHAR(255) NOT NULL,
  customerAddress VARCHAR(255) NOT NULL,
  customerPassport VARCHAR(9) NOT NULL,
  customerAuthorityIssuingPassport VARCHAR(255) NOT NULL,
  contract NUMERIC(20,0) NOT NULL,
  FOREIGN KEY (contract) REFERENCES Contract (id)
);

CREATE TABLE IF NOT EXISTS Pension (
  id NUMERIC(20, 0) UNIQUE  PRIMARY KEY,
  month DATE NOT NULL,
  amount DECIMAL NOT NULL,
  pacient NUMERIC(20, 0) NOT NULL,
  FOREIGN KEY (pacient) REFERENCES Pacient (id)
);

CREATE TABLE IF NOT EXISTS BankStatment (
  id NUMERIC(20, 0) UNIQUE PRIMARY KEY,
  date DATE NOT NULL,
  number NUMERIC(6,0) NOT NULL,
  type NUMERIC (1, 0) NOT NULL
);

CREATE TABLE IF NOT EXISTS PaidSum (
  id NUMERIC(20, 0) UNIQUE  PRIMARY KEY,
  amount DECIMAL NOT NULL,
  pacient NUMERIC(20, 0) NOT NULL,
  bankStatment NUMERIC(20, 0) NOT NULL,
  paymentCreditCard NUMERIC(1, 0) NOT NULL,
  FOREIGN KEY (pacient) REFERENCES Pacient (id),
  FOREIGN KEY (bankStatment) REFERENCES BankStatment (id)
);

CREATE TABLE IF NOT EXISTS AmountForPayment (
  id NUMERIC(20, 0) UNIQUE  PRIMARY KEY,
  month DATE NOT NULL,
  amount DECIMAL NOT NULL,
  pacient NUMERIC(20, 0) NOT NULL,
  detailCalculation VARCHAR(5000) NULL,
  FOREIGN KEY (pacient) REFERENCES Pacient (id)
);

//таблица для id генерации
create table hibernate_id_generation(next_hi NUMERIC(20,0));
insert into hibernate_id_generation values (32768);

CREATE TABLE IF NOT EXISTS Config (
  id NUMERIC(20, 0) UNIQUE  PRIMARY KEY,
  currentDate DATE NOT NULL,
  fullNameChief VARCHAR(255) NOT NULL,
  shortNameChief VARCHAR(255) NOT NULL,
  vicariousAuthorty VARCHAR(255) NOT NULL,
  countPacientOnPage NUMERIC(3, 0) NOT NULL,
  dateCreated TIMESTAMP(6) NOT NULL,
  roundingAmount NUMERIC(3, 0) NOT NULL,
  actual NUMERIC(1, 0) NOT NULL,
  login VARCHAR(45) NULL,
  password VARCHAR(45) NULL
);

INSERT INTO config VALUES (
  1073938000,
  '2013-04-01',
  'Масальцева Светлана Викторовна',
  'Масальцева С.В.',
  '№9 от 04.01.2013',
  '10',
  '2013-04-02 16:37:43.163',
  '50',
  '1',
   'rus',
   '123'
);

CREATE TABLE IF NOT EXISTS users (
  id NUMERIC(20, 0) UNIQUE  PRIMARY KEY,
  username VARCHAR(45) NOT NULL,
  password VARCHAR(45) NOT NULL,
  enabled BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS user_roles (
  ID NUMERIC(20, 0) UNIQUE  PRIMARY KEY,
  users NUMERIC(20, 0) NOT NULL,
  authority VARCHAR(45) NOT NULL,
  FOREIGN KEY (users) REFERENCES users (id)
);

INSERT INTO users VALUES (
  1073938100,
  'admin',
  'admin',
  TRUE
);

INSERT INTO user_roles VALUES (
  1073938001,
  1073938100,
  'ROLE_USER'
);


//CREATE INDEX surname_index ON pacient (surname) USING BTREE;