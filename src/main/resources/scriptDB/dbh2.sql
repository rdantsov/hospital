CREATE TABLE IF NOT EXISTS Contract (
  id INT IDENTITY  PRIMARY KEY,
  nomer VARCHAR(255) NOT NULL,
  completed INT NOT NULL
);

CREATE TABLE IF NOT EXISTS Period (
  id INT IDENTITY  PRIMARY KEY,
  startPeriod DATE NOT NULL,
  endPeriod DATE NOT NULL,
  contract INT NOT NULL,
  FOREIGN KEY (contract) REFERENCES Contract (id)
);

CREATE TABLE IF NOT EXISTS Pacient (
  id INT IDENTITY PRIMARY KEY,
  surname VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  midleName VARCHAR(255),
  passport VARCHAR(9) NOT NULL,
  contract INT NOT NULL,
  FOREIGN KEY (contract) REFERENCES Contract (id)
);

CREATE TABLE IF NOT EXISTS Pension (
  id INT IDENTITY  PRIMARY KEY,
  month DATE NOT NULL,
  amount DECIMAL NOT NULL,
  pacient INT NOT NULL,
  FOREIGN KEY (pacient) REFERENCES Pacient (id)
);

CREATE TABLE IF NOT EXISTS BankStatment (
  id INT IDENTITY PRIMARY KEY,
  date DATE NOT NULL,
  number VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS PaidSum (
  id INT IDENTITY  PRIMARY KEY,
  amount DECIMAL NOT NULL,
  pacient INT NOT NULL,
  bankStatment INT NOT NULL,
  FOREIGN KEY (pacient) REFERENCES Pacient (id),
  FOREIGN KEY (bankStatment) REFERENCES BankStatment (id)
);

CREATE TABLE IF NOT EXISTS AmountForPayment (
  id INT IDENTITY  PRIMARY KEY,
  month DATE NOT NULL,
  amount DECIMAL NOT NULL,
  pacient INT NOT NULL,
  FOREIGN KEY (pacient) REFERENCES Pacient (id)
);

create table IF NOT EXISTS hibernate_id_generation(next_hi INT);
insert into hibernate_id_generation values (32768);


