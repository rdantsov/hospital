ALTER TABLE Contract
ADD COLUMN nomernew NUMERIC(6, 0) NULL;

UPDATE Contract
SET nomernew = cast(nomer as numeric);

ALTER TABLE Contract
DROP COLUMN nomer;

ALTER TABLE Contract
RENAME COLUMN nomernew TO nomer;

ALTER TABLE Contract
ALTER COLUMN nomer SET NOT NULL;

